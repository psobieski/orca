program RPT;

uses
  Forms,
  UnitMainForm in 'src\UnitMainForm.pas' {MainForm},
  uAbsConnConfig in 'src\uAbsConnConfig.pas',
  UnitClassDBSettings in 'src\UnitClassDBSettings.pas',
  uEncrypt in 'src\uEncrypt.pas',
  UnitProjectsEditor in 'src\UnitProjectsEditor.pas' {ProjectsForm},
  UnitDMDatabase in 'src\UnitDMDatabase.pas' {DMDatabase: TDataModule},
  UnitClassProjects in 'src\Classes\UnitClassProjects.pas',
  DataModuleStyles in 'src\DataModuleStyles.pas' {DMStyles: TDataModule},
  UnitProjectInterface in 'src\Interfaces\UnitProjectInterface.pas',
  UnitClassTestCase in 'src\Classes\UnitClassTestCase.pas',
  uReklama in 'src\uReklama.pas' {FOAutorze},
  DataModuleReport in 'src\DataModuleReport.pas' {DMReport: TDataModule},
  UnitWynikiInterface in 'src\Interfaces\UnitWynikiInterface.pas',
  UnitClassWyniki in 'src\Classes\UnitClassWyniki.pas',
  UnitFormWyniki in 'src\Form\UnitFormWyniki.pas' {FormWyniki};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Rejestr przypadków testowych';
  Application.CreateForm(TDMStyles, DMStyles);
  Application.CreateForm(TDMDatabase, DMDatabase);
  Application.CreateForm(TDMReport, DMReport);
  if DMDatabase.Connect then
  begin
    Application.CreateForm(TMainForm, MainForm);
    Application.Run;
  end
  else
    Application.Terminate;
end.
