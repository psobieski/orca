unit UnitClassProjects;

interface

uses
  Classes, UnitProjectInterface, DB, SDEngine;

type

  TProject = class( TInterfacedObject, IProject )
  private
    fId: integer;
    fName: String;
    fDesctiption: String;
    fModified: Boolean;
  protected
    function GetId: Integer; virtual;
    function GetName: String; virtual;
    function GetDescription: String; virtual;
    procedure SetName( value: String ); virtual;
    procedure SetDescription( value: String ); virtual;
    procedure SetId( value: Integer ); virtual;
  public
    procedure ReadFromDataset( aDataSet: TDataSet );
    procedure SaveToDatabase( DataBase: TSDDatabase );

    property Name: String read GetName write SetName;
    property Id: Integer read GetId write SetId;
    property Description: String read GetDescription write SetDescription;
  end;

implementation

uses sysUtils;

function TProject.GetId: Integer;
begin
  result := Fid;
end;

function TProject.GetName: String;
begin
  result := fName;
end;

function TProject.GetDescription: String;
begin
  result := fDesctiption;
end;

procedure TProject.SetName( value: String );
begin
  if fname <> Value then
  begin
    fName := Value;
    fModified  := true;
  end;
end;

procedure TProject.SetDescription( value: String );
begin
  if fDesctiption <> Value then
  begin
    fDesctiption := Value;
    fModified := True;
  end;
end;

procedure TProject.SetId( value: Integer );
begin
  if fid <> value then
  begin
    fId := value;
    fModified := True;
  end;
end;

procedure TProject.ReadFromDataset( aDataSet: TDataSet );
begin
  fid          := aDataSet.FieldValues['ID_PROJEKT'];
  fName        := aDataSet.FieldValues['NAZWA'];
  fDesctiption := aDataSet.FieldValues['OPIS'];
  fModified    := False;
end;

procedure TProject.SaveToDatabase( DataBase: TSDDatabase );
var Q:TSDQuery;
begin
  Q := TSDQuery.Create( nil );
  if fid = -1 then
  begin
    q.SQL.Add('select gen_id(GEN_RPT_PROJEKTY,1) as lastid from rdb$database');
    q.Open;
    fId := q.FieldByName('LASTID').AsInteger;
    q.close;

    q.SQL.Clear;
    Q.SQL.Add('insert into RPT_PROJEKTY (ID_PROJEKT,NAZWA,OPIS) values (:ID_PROJEKT,:NAZWA,:OPIS)');
  end
  else
  begin
    Q.SQL.Add('UPDATE RPT_PROJEKTY set NAZWA=:NAZWA,OPIS=:OPIS where ID_PROJEKT=:ID_PROJEKT');
  end;

  q.ParamByName('ID_PROJEKTY').AsInteger := fid;
  q.ParamByName('NAZWA').AsString := fName;
  q.ParamByName('OPIS').AsString := fDesctiption;

  try
    if not DataBase.InTransaction then
       DataBase.StartTransaction;
    q.ExecSQL;
    DataBase.Commit;
    fModified := False;
  except
    on E: Exception do begin
      Raise Exception.Create('Zapis danych nie powi�d� si�');
    end;
  end;

end;

end.
