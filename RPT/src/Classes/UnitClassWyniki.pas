unit UnitClassWyniki;

interface

uses
  Classes, UnitWynikiInterface, DB, SDEngine, SysUtils;

type

  TWynik = class(TInterfacedObject, IWynik)
  private
    fId: integer;
    fIdTest: integer;
    fIdStatusWynik: Integer;
    fDataWykonania: TDateTime;
    fOpis: string;
  protected
    function GetId: Integer; virtual;
    function GetIdTest: Integer;
    function GetDataWykonania: TDateTime; virtual;
    function GetOpis: string; virtual;
    function GetIdStatusWynik: Integer; virtual;

    procedure SetId(value: Integer);  virtual;
    procedure SetIdTest(value: Integer);
    procedure SetIdStatusWynik(value: integer);    virtual;
    procedure SetDataWykonania(value: TDateTime);  virtual;
    procedure SetOpis(value: String);  virtual;

  public
    procedure ReadFromDataset( aDataSet: TDataSet );
    procedure SaveToDatabase( DataBase: TSDDatabase );

    procedure DodajWynikTestu();

    property Id: Integer read GetId write SetId;
    property IdTest: Integer read GetIdTest write SetIdTest;
    property IdStatusWynik: Integer read GetIdStatusWynik write SetIdStatusWynik;
    property DataWykonania: TDateTime read GetDataWykonania write SetDataWykonania;
    property Opis: String read GetOpis write SetOpis;
  end;

implementation

{ TWynik }

procedure TWynik.DodajWynikTestu;
begin
//
end;

function TWynik.GetDataWykonania: TDateTime;
begin
  result := fDataWykonania;
end;

function TWynik.GetId: Integer;
begin
  result := fId;
end;

function TWynik.GetIdStatusWynik: Integer;
begin
  result := fIdStatusWynik;
end;

function TWynik.GetIdTest: Integer;
begin
  result := fIdTest;
end;

function TWynik.GetOpis: string;
begin
  result := fOpis;
end;

procedure TWynik.ReadFromDataset(aDataSet: TDataSet);
begin
  fId            := aDataSet.FieldValues['ID_WYNIK'];
  fIdTest        := aDataSet.FieldValues['ID_TEST'];
  fIdStatusWynik := aDataSet.FieldValues['ID_WYNIK_STATUS'];
  fDataWykonania := aDataSet.FieldValues['DATA_WYKONANIA'];
  fOpis          := aDataSet.FieldValues['OPIS'];
end;

procedure TWynik.SaveToDatabase(DataBase: TSDDatabase);
var
  Q:TSDQuery;
begin
  Q := TSDQuery.Create( nil );
  if fid = -1 then
  begin
    q.SQL.Add('select gen_id(GEN_RPT_TESTY_WYNIKI,1) as lastid from rdb$database');
    q.Open;
    fId := q.FieldByName('LASTID').AsInteger;
    q.close;

    q.SQL.Clear;
    Q.SQL.Add('insert into RPT_TESTY_WYNIKI (ID_WYNIK,ID_TEST,ID_WYNIK_STATUS,DATA_WYKONANIA,OPIS) values (:ID_WYNIK, :ID_TEST, :ID_WYNIK_STATUS, :DATA_WYKONANIA, :OPIS)');
  end
  else
  begin
    Q.SQL.Add('UPDATE RPT_TESTY_WYNIKI set ID_TEST=:ID_TEST, ID_WYNIK_STATUS=:ID_WYNIK_STATUS ,DATA_WYKONANIA=:DATA_WYKONANIA ,OPIS=:OPIS  where ID_WYNIK=:ID_WYNIK');
  end;

  q.ParamByName('ID_WYNIK').AsInteger := fid;
  q.ParamByName('ID_TEST').AsInteger := fIdTest;
  q.ParamByName('ID_WYNIK_STATUS').AsInteger := fIdStatusWynik;
  q.ParamByName('DATA_WYKONANIA').AsDateTime := fDataWykonania;
  q.ParamByName('OPIS').AsString := fOpis;

  try
    if not DataBase.InTransaction then
       DataBase.StartTransaction;
    q.ExecSQL;
    DataBase.Commit;
  except
    on E: Exception do begin
      Raise Exception.Create('Zapis danych nie powi�d� si�');
    end;
  end;
end;

procedure TWynik.SetDataWykonania(value: TDateTime);
begin
  fDataWykonania := value
end;

procedure TWynik.SetId(value: Integer);
begin
  fId := value;
end;

procedure TWynik.SetIdStatusWynik(value: integer);
begin
  fIdStatusWynik := value;
end;

procedure TWynik.SetIdTest(value: Integer);
begin
  fIdTest := value;
end;

procedure TWynik.SetOpis(value: String);
begin
  fOpis := value;
end;

end.
