unit UnitClassTestCase;

interface

uses
  Classes;

Type
  TTestCase = class( TObject )
  public
    Id: Integer;
    ParentID: Integer;
    ProjektID: Integer;
    Opis: String;
    Nazwa: String;
    DataWykonania: TDateTime;
    WynikTestu: String;
  end;

implementation

end.
