object DMReport: TDMReport
  OldCreateOrder = False
  Left = 274
  Top = 124
  Height = 150
  Width = 215
  object frxReport: TfrxReport
    Version = '4.6.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41733.649766782400000000
    ReportOptions.LastChange = 41733.746944618050000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo3OnAfterData(Sender: TfrxComponent);'
      'begin'
      '  if Value = 0 then'
      '     TfrxMemoView(Sender).Text := '#39#39';  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 24
    Top = 16
    Datasets = <
      item
        DataSet = frxDB1
        DataSetName = 'frxDB1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader1: TfrxPageHeader
        Height = 20.000000000000000000
        Top = 16.000000000000000000
        Width = 793.701300000000000000
        object Memo5: TfrxMemoView
          Left = 3.000000000000000000
          Top = 4.000000000000000000
          Width = 190.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Nazwa')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 193.000000000000000000
          Top = 4.000000000000000000
          Width = 333.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Opis')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 526.000000000000000000
          Top = 4.000000000000000000
          Width = 106.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Data wykonania')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 632.000000000000000000
          Top = 4.000000000000000000
          Width = 159.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Status wykonania')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 16.000000000000000000
        Top = 96.000000000000000000
        Width = 793.701300000000000000
        DataSet = frxDB1
        DataSetName = 'frxDB1'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 193.000000000000000000
          Width = 333.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[frxDB1."OPIS"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 3.000000000000000000
          Width = 190.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[frxDB1."TEST_NAZWA"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 526.000000000000000000
          Width = 106.000000000000000000
          Height = 16.000000000000000000
          OnAfterData = 'Memo3OnAfterData'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[frxDB1."DATA_WYKONANIA"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 632.000000000000000000
          Width = 159.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[frxDB1."STATUS_WYKONANIA"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 20.000000000000000000
        Top = 172.000000000000000000
        Width = 793.701300000000000000
      end
    end
  end
  object frxDB1: TfrxDBDataset
    UserName = 'frxDB1'
    CloseDataSource = False
    DataSet = SDQuery1
    Left = 80
    Top = 16
  end
  object SDQuery1: TSDQuery
    DatabaseName = 'RPT'
    Left = 136
    Top = 16
  end
end
