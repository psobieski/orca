unit UnitDMDatabase;

interface

uses
  SysUtils, Classes, DB, SDEngine;

type
  TDMDatabase = class(TDataModule)
    SDDatabase1: TSDDatabase;
    qListProjects: TSDQuery;
    qParam: TSDQuery;
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    procedure SetConnectionProps();
  public
    function Connect: Boolean;
    procedure LoadListProjects;
    function GenerateId(GenName: string; Increment: Integer): Integer;
  end;

var
  DMDatabase: TDMDatabase;

implementation

uses
  UnitClassDBSettings,
  uAbsConnConfig;

{$R *.dfm}

procedure TDMDatabase.DataModuleDestroy(Sender: TObject);
begin
  SDDatabase1.Close;
end;

function TDMDatabase.Connect: Boolean;
begin
  SetConnectionProps();
  SDDatabase1.Open;
  Result := SDDatabase1.Connected;
end;

procedure TDMDatabase.LoadListProjects;
begin
  qListProjects.Active := True;
end;

function TDMDatabase.GenerateId(GenName: string; Increment: Integer): Integer;
begin
  qParam.SQL.Text := 'SELECT GEN_ID(' + GenName + ',' +
    IntToStr(Increment) + ')as wynik from RDB$DATABASE';
  qParam.Open;
  Result := qParam.fieldbyname('wynik').AsInteger;
  qParam.Close;
end;

procedure TDMDatabase.SetConnectionProps();
var
  DontConnect: Boolean;
begin
  if DBStan = nil then
  begin
    DBStan := TAbsDBSettings.Create();
    try
      DBStan.LoadFromIni();
    except
      Exit;
    end;
  end;

  DontConnect := False;

  try
    SDDatabase1.Open;
  except
  end;

  while not DontConnect and not SDDatabase1.Connected do
  begin
    DontConnect := not TFoConfig.Execute(DBStan.DBPath);
    if not DontConnect then
    begin
      if DBStan.ConnType <> Local then
      begin
        SDDatabase1.RemoteDatabase := DBStan.IP + '/' + IntToStr(DBStan.Port) + ':' + DBStan.DBPath;
        SDDatabase1.DatabaseName := DBStan.DBPath;
      end
      else
      begin
        SDDatabase1.RemoteDatabase := '';
        SDDatabase1.DatabaseName := DBStan.DBPath;
      end;
      SDDatabase1.Open;
    end;
  end;
end;

end.
