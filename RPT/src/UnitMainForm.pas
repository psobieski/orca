unit UnitMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uAbsConnConfig,
  cxGraphics, ComCtrls, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  ToolWin, cxContainer, cxTreeView, Menus, cxControls, dxStatusBar, ElTree,
  ImgList, ElHeader,
  DB, cxDBEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  ExtCtrls, StdCtrls;

type
  TMainForm = class(TForm)
    dxStatusBar1: TdxStatusBar;
    mm1: TMainMenu;
    Plik1: TMenuItem;
    Edycja1: TMenuItem;
    Opcje1: TMenuItem;
    Pomoc1: TMenuItem;
    Konfig: TMenuItem;
    Wyjd1: TMenuItem;
    tlbFiltr: TToolBar;
    btnProjectS: TToolButton;
    Dodajtest1: TMenuItem;
    Usuntest1: TMenuItem;
    tvData: TElTree;
    btnAdd: TToolButton;
    ToolButton2: TToolButton;
    btnDeleteTest: TToolButton;
    ToolButton4: TToolButton;
    ImageList1: TImageList;
    ToolButton5: TToolButton;
    dsProjects: TDataSource;
    cbProjects: TcxLookupComboBox;
    Splitter1: TSplitter;
    mnuDataTree: TPopupMenu;
    mnuChangeName: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    Dodajtest2: TMenuItem;
    Usu1: TMenuItem;
    Wykonajtest1: TMenuItem;
    pnlBottom: TPanel;
    Panel1: TPanel;
    Raporty1: TMenuItem;
    Zestawieniewynikw1: TMenuItem;
    tvHistoria: TElTree;
    procedure FormDestroy(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnProjectSClick(Sender: TObject);
    procedure cbProjectsPropertiesEditValueChanged(Sender: TObject);
    procedure btnDeleteTestClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure KonfigClick(Sender: TObject);
    procedure Pomoc1Click(Sender: TObject);
    procedure tvDataDblClick(Sender: TObject);
    procedure tvDataItemSelectedChange(Sender: TObject; Item: TElTreeItem);
    procedure tvDataTryEdit(Sender: TObject; Item: TElTreeItem; Section:
        TElHeaderSection; var CellType: TFieldTypes; var CanEdit: Boolean);
    procedure tvDataValidateInplaceEdit(Sender: TObject; Item: TElTreeItem;
        Section: TElHeaderSection; var Text: string; var Accept: Boolean);
    procedure ZmienNazweTestu(Sender: TObject);
    procedure Wyjd1Click(Sender: TObject);
    procedure Zestawieniewynikw1Click(Sender: TObject);
  private
    Fediting: Boolean;
    WybranyProjekt: Integer;
    procedure WczytajTesty(Root: TeltreeItem; aTestId: Integer);
    procedure OdswiezWidok();
    procedure AddTest( Parent: TEltreeItem) ;
    procedure SaveTest( Item: TElTreeItem);
    procedure ClearObjects;
    Procedure WczytajWynikiTestow(aIdTEstu: Integer);
    Procedure UsunTest();
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses SDEngine,
  UnitClassTestCase,
  UnitProjectsEditor,
  UnitDMDatabase,
  uReklama,
  DataModuleReport,
  UnitFormWyniki;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  ClearObjects;
end;

procedure TMainForm.btnProjectSClick(Sender: TObject);
var
  aID: Integer;
begin
  aID := TProjectsForm.Execute(Self);
  cbProjects.EditValue := aID;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Self.Caption := Application.Title;
  WybranyProjekt := -1;
  OdswiezWidok();
  dsProjects.DataSet := DMDatabase.qListProjects;
  DMDatabase.LoadListProjects;
end;

procedure TMainForm.WczytajTesty(Root: TElTreeItem; aTestId: Integer);
var Q   : TSDQuery;
    item: TElTreeItem;
    p   : TTestCase;
begin
  Q:= TSDQuery.Create(self);
  Q.DatabaseName := 'RPT';
  try                                                                
  if aTestId = -1 then
    Q.SQL.Add(' select * from LIST_ALL_TESTS(:projectid) where ID_PARENT_TEST = 0 ORDER BY ID_PARENT_TEST DESC')
  else
  begin
    Q.SQL.Add(' select * from LIST_ALL_TESTS(:projectid) where ID_PARENT_TEST = :parentId ORDER BY ID_PARENT_TEST DESC');
    Q.ParamByname('parentId').AsInteger :=  aTestId;
  end;

  Q.ParamByname('projectID').AsInteger :=  WybranyProjekt;
  Q.Open;
  while not Q.Eof do
  begin
    p := TTestCase.Create;

    p.Id := Q.Fieldbyname('ID_TEST').AsInteger;
    p.ParentId := Q.Fieldbyname('ID_PARENT_TEST').AsInteger;
    p.ProjektID := Q.Fieldbyname('ID_PROJEKT').AsInteger;
    p.Opis := Q.Fieldbyname('OPIS').AsString;
    p.Nazwa :=  Q.Fieldbyname('TEST_NAZWA').AsString;
    p.DataWykonania := Q.Fieldbyname('DATA_WYKONANIA').AsDateTime;
    p.WynikTestu := Q.Fieldbyname('STATUS_WYKONANIA').AsString;

    if root <> nil then
         Item := TvData.Items.AddChild( root, p.Nazwa )
    else
         Item := TvData.Items.Add( nil, p.Nazwa );

    item.Data := p;
    Item.SubItems.Add(p.Opis);

    if q.FieldValues['DATA_WYKONANIA'] <> null then
        item.SubItems.Add( DateToStr( p.DataWykonania))
    else
        item.SubItems.Add( '');

    Item.SubItems.Add(p.WynikTestu);

    WczytajTesty( Item,  p.Id );

    Q.Next;
  end;
  finally
    Q.Close;
    Q.Free;
  end;

end;

procedure TMainForm.OdswiezWidok();
begin
  tvData.Items.BeginUpdate;
  try
    ClearObjects;
    tvData.Items.Clear;
    WczytajTesty(nil,-1);
  finally
    tvData.Items.EndUpdate;
  end;
end;

procedure TMainForm.AddTest( Parent: TEltreeItem ) ;
var selected: TelTreeItem;
    added: TelTreeItem;
    parenttest,p: TTestCase;
begin
  if Fediting then exit;
   selected := Parent;
   parentTest := nil;
   if selected <> nil then
   begin
      Added := tvData.items.addChild( selected, 'Nowy test');
      parentTest := TTestCase( selected.Data );
   end
   else
   begin
      Added := tvData.items.add( nil, 'Nowy test');
      Parenttest := nil;
   end;

   //tvData.Selected := added;

   p := TTestCase.Create;
   p.Id := -1;
   p.Opis := 'Nowy test';
   p.Nazwa := 'NoName';
   if ParentTest <> nil then
   begin
      p.ParentID := parentTest.id;
   end
   else
      p.ParentId := 0;
      
   p.projektID := WybranyProjekt;
   added.Text  := p.Nazwa;
   added.SubItems.Add( p.Opis);
   added.SubItems.Add( '');
   added.SubItems.Add( '');
   added.Data := p;
   added.EditText;
end;
   
procedure TMainForm.btnAddClick(Sender: TObject);
begin
  AddTest( tvData.Selected );
end;

procedure TMainForm.cbProjectsPropertiesEditValueChanged(Sender: TObject);
begin
  if cbProjects.EditValue <> WybranyProjekt then
  begin
    WybranyProjekt := cbProjects.EditValue;
    OdswiezWidok();
  end;
end;

procedure TMainForm.SaveTest( Item: TElTreeItem);
var Q:TSDQuery;
    TC: TTestCase;
begin
  Q := TSDQuery.Create( self );
  Q.DatabaseName := 'RPT';
  try
    tc := TTestCase( Item.Data );
    if TC.Id = -1 then
    begin
      q.SQL.Clear;
      q.SQL.Add('Select gen_id(gen_rpt_testy,1) as LASTID from rdb$database');
      q.Open;
      Tc.Id := q.FieldByName('LASTID').AsInteger;
      q.close;
      q.SQL.Clear;
      q.SQL.Add('insert into RPT_TESTY (ID_TEST,ID_PARENT_TEST,ID_PROJEKT,OPIS,TAG,NAZWA) values (:ID_TEST,:ID_PARENT_TEST,:ID_PROJEKT,:OPIS,:TAG,:NAZWA)');
    end
    else
    begin
      q.SQL.Add('update RPT_TESTY set ID_PARENT_TEST=:ID_PARENT_TEST, ID_PROJEKT=:ID_PROJEKT, OPIS=:opis, TAG=:tag, NAZWA=:nazwa where ID_TEST=:ID_TEST');
    end;

    q.ParamByName('ID_PARENT_TEST').AsInteger := tc.ParentID;
    q.ParamByName('ID_PROJEKT').AsInteger := tc.ProjektID;
    q.ParamByName('OPIS').AsString := tc.Opis;
    q.ParamByName('Tag').AsString := '';
    q.ParamByName('NAZWA').AsString := tc.Nazwa;
    q.ParamByName('ID_TEST').AsInteger := tc.Id;

    q.ExecSQL;

  finally
    Q.Free;
  end;
end;

procedure TMainForm.tvDataItemSelectedChange(Sender: TObject; Item:
    TElTreeItem);
var p:TTestCase;
begin
  if (item <> nil) and (tvData.Focused) then
  begin
    p := TTestCase( item.Data );
    if p.Id <> -1 then
      WczytajWynikiTestow(p.Id);
  end;
end;

procedure TMainForm.tvDataValidateInplaceEdit(Sender: TObject; Item:
    TElTreeItem; Section: TElHeaderSection; var Text: string; var Accept:
    Boolean);
var
    p:TTestCase;
begin
   accept := True;
   p := TTestCase( Item.Data );
   case Section.Index of
     0: p.Nazwa := Text;
     1: p.Opis := Text;
   end;
   SaveTest(Item);
   Fediting := False;
end;

procedure TMainForm.ClearObjects;
var i:Integer;
begin
  for i:=0 to tvData.Items.Count-1 do
  begin
    if tvData.Items[i].Data <> nil then
       TTestCase( tvData.Items[i].Data ).Free;
  end;
end;

procedure TMainForm.btnDeleteTestClick(Sender: TObject);
begin
  UsunTest;
end;

procedure TMainForm.ZmienNazweTestu(Sender: TObject);
begin
   if tvData.Selected <> nil then
   begin
      tvData.Selected.EditText;
   end;
end;

Procedure TMainForm.WczytajWynikiTestow(aIdTEstu: Integer);
var Q:TSDQuery;
   item: TElTreeItem;
begin
  tvHistoria.Items.Clear;
  Q:=TSDQuery.Create( self );
  try
    Q.DatabaseName := 'RPT';
    Q.SQL.Add('select w.data_wykonania,w.opis,s.id_wynik_status,s.nazwa as status from rpt_testy_wyniki w ');
    Q.SQL.Add(' inner join rpt_wyniki_status s on (s.id_wynik_status = w.id_wynik_status)');
    q.SQL.Add(' where w.id_test =:idTestu order by 1 DESC');
    q.ParamByName('idTestu').AsInteger := aIdTEstu;
    q.Open;
    while not q.Eof do
    begin
      item := tvHistoria.Items.Add(nil,'');
      item.Text := q.Fieldbyname('data_wykonania').AsString;
      item.SubItems.Add(q.Fieldbyname('OPIS').AsString);
      item.SubItems.Add(q.Fieldbyname('status').AsString);
      item.ParentStyle := False;
      item.ParentColors := False;
      item.BkColor := tvHistoria.BkColor;
      item.RowBkColor := tvHistoria.BkColor;

      case q.FieldByName('id_wynik_status').AsInteger of
        1: begin
              item.Bold  := False;
              item.Color := clGreen;
           end;
        2: begin
              item.Bold  := True;
              item.Color := clWhite;
              item.BkColor := clRed;
              item.RowBkColor := clRed;
           end;
        3: begin
              item.Bold  := False;
              item.Color := clOlive;
           end;
      end;
      
      Q.Next;
    end;
    q.close;
  finally
    FreeAndNil( Q );
  end;
end;

Procedure TMainForm.UsunTest();
var Q:TSDQuery;
    item: TElTreeItem;
    p: TTestCase;
begin
  if Fediting then exit;
  if tvData.Selected <> nil then
  begin
    p := TTestCase(tvData.selected.Data);
    if p.Id <> -1 then
    begin
      if MessageDlg('Czy na pewno skasowa� wybrany test?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
      begin
        Q:=TSDQuery.Create( self );
        try
          Q.DatabaseName := 'RPT';
          Q.SQL.Add('delete from RPT_TESTY where ID_TEST =:IdTestu');
          q.ParamByName('idTestu').AsInteger := p.Id;
          q.ExecSQL;

          tvData.Items.Delete( tvData.Selected );
          
        finally
          FreeAndNil( q );
        end;
      end;
    end else
        MessageDlg('Wybrany test nie zostal jeszcze zapisany.',mtInformation,[mbOk],0);
  end;
end;


procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := MessageDlg('Czy na pewno chcesz wyj�� z programu?', mtConfirmation,[mbYes, mbNo],0) = mrYes;
end;

procedure TMainForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if tvData.Focused then
  case Key of
    '+': btnAddClick(Sender);
    '-': btnDeleteTestClick(Sender);
    '*': btnProjectSClick(Sender);
    '/': ZmienNazweTestu(Sender);
  end;
end;

procedure TMainForm.KonfigClick(Sender: TObject);
begin
  TFoConfig.Execute();
end;

procedure TMainForm.Pomoc1Click(Sender: TObject);
var
  Opis: string;
begin
  Opis := 'Aplikacja do tworzenia test�w akceptacyjnych i ewidencji ich przeprowadzenia';
  ShowAboutProgram(Application.Title, Self.Color, Opis);
end;

procedure TMainForm.tvDataDblClick(Sender: TObject);
var
  okno: TFormWyniki;

begin
  if self.tvData.Selected <> nil then
  begin
    if ttestcase(self.tvData.Selected.Data).Id = -1 then
      exit;
      
    okno := TFormWyniki.Create(self);
    try
      okno.dodajWynikTestu(ttestcase(self.tvData.Selected.Data).Id);

      WczytajWynikiTestow(ttestcase(self.tvData.Selected.Data).Id)
    finally
      freeandnil(okno);
    end;
  end;
end;

procedure TMainForm.tvDataTryEdit(Sender: TObject; Item: TElTreeItem; Section:
    TElHeaderSection; var CellType: TFieldTypes; var CanEdit: Boolean);
begin
   CanEdit := True;
   Fediting := True;
end;

procedure TMainForm.Wyjd1Click(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.Zestawieniewynikw1Click(Sender: TObject);
begin
  DMReport.ReportZestawienieTestow(WybranyProjekt, -1);
end;

end.
