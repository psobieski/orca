unit UnitWynikiInterface;

interface

uses
  Classes, DB, SDEngine;

type

  IWynik = interface
    ['{48492519-D72A-49CE-A1CF-E5F167771F9C}']
    function GetId: Integer;
    function GetIdTest: Integer;
    function GetIdStatusWynik: Integer;
    function GetDataWykonania: TDateTime;
    function GetOpis: string;

    procedure SetId(value: Integer);
    procedure SetIdTest(value: Integer);
    procedure SetIdStatusWynik(values: integer);
    procedure SetDataWykonania(value: TDateTime);
    procedure SetOpis(value: String);

    procedure ReadFromDataSet(aDataset: TDataSet );
    procedure SaveToDatabase(DataBase: TSDDatabase );

    property Id: Integer read GetId write SetId;
    property IdTest: Integer read GetIdTest write SetIdTest;
    property IdStatusWynik: Integer read GetIdStatusWynik write SetIdStatusWynik;
    property DataWykonania: TDateTime read GetDataWykonania write SetDataWykonania;
    property Opis: String read GetOpis write SetOpis;

  end;

  IWynikiList = interface
    procedure LoadFromDatabase( aDatabase: TSDDatabase );
    procedure SaveToDatabase( aDataBase: TSDDatabase );
    procedure DeleteProject( aId: Integer );
  end;

implementation

end.
