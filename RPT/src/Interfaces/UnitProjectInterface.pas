unit UnitProjectInterface;

interface

uses
  Classes, DB, SDEngine;

type

  IProject = interface
    ['{33CA9118-4D40-4E35-9EAE-DF72C72CDE17}']
    function GetId: Integer;
    function GetName: String;
    function GetDescription: String;
    procedure SetName( value: String );
    procedure SetDescription( value: String );
    procedure SetId( value: Integer );
    procedure ReadFromDataSet( aDataset: TDataSet );
    procedure SaveToDatabase( DataBase: TSDDatabase );

    property Name: String read GetName write SetName;
    property Id: Integer read GetId write SetId;
    property Description: String read GetDescription write SetDescription;
  end;

  IProjectList = interface
    procedure LoadFromDatabase( aDatabase: TSDDatabase );
    procedure SaveToDatabase( aDataBase: TSDDatabase );
    procedure DeleteProject( aId: Integer );
  end;
  
implementation

end.
