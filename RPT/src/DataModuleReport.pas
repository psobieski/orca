unit DataModuleReport;

interface

uses
  SysUtils, Classes, Forms, frxClass, frxDBSet, DB, SDEngine;

type
  TDMReport = class(TDataModule)
    frxReport: TfrxReport;
    frxDB1: TfrxDBDataset;
    SDQuery1: TSDQuery;
  private
    procedure ShowReport();
    procedure LoadReportFromFile(fileName: string);
  public
    procedure ReportZestawienieTestow(projektId, testId :Integer);
  end;

var
  DMReport: TDMReport;

implementation

uses
  Windows;

{$R *.dfm}

procedure TDMReport.ShowReport();
begin
  frxReport.ShowReport();
end;

procedure TDMReport.LoadReportFromFile(fileName:  string);
var
  ApplicationDir :string;
begin
  ApplicationDir := ExtractFileDir(Application.ExeName);
  if FileExists(ApplicationDir + '\reports\' + fileName) then
  begin
    frxReport.LoadFromFile(ApplicationDir + '\reports\' + fileName);
    frxReport.Variables.Clear();
    frxReport.Variables.LoadFromFile(ApplicationDir + '\reports\' + fileName);
  end
  else
    Application.MessageBox('Brak pliku wydruku. Nale�y ponownie zainstalowa� program.',
      'Brak pliku wydruku.', MB_ICONWARNING or MB_OK);
end;

procedure TDMReport.ReportZestawienieTestow(projektId, testId :Integer);
begin
  LoadReportFromFile('Zestawienie.fr3');
  if testId = -1 then
    SDQuery1.SQL.Text :=' select * from LIST_ALL_TESTS(:projectid) ORDER BY ID_PARENT_TEST DESC'
  else
  begin
    SDQuery1.SQL.Text := ' select * from LIST_ALL_TESTS(:projectid) where ID_PARENT_TEST = :parentId ORDER BY ID_PARENT_TEST DESC';
    SDQuery1.ParamByname('parentId').AsInteger :=  testId;
  end;
  SDQuery1.ParamByname('projectID').AsInteger :=  projektId;
  SDQuery1.Open;
  try
    //frxDB1.DataSource := SDQuery1.DataSource;
    ShowReport();
  finally
    SDQuery1.Close;
  end;
end;

end.
