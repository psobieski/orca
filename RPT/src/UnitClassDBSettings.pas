unit UnitClassDBSettings;

interface

uses
  Windows,
  SysUtils,
  Classes;

const
  dtInterbase         = 0;
  dtMSSQL             = 1;
  dtOracle            = 2;
  AppData        = '.\';
  MainIni        = 'RPT.ini';
  IniSerwerSect  = 'DB';
  IniOperatSect  = 'OPER';

  IniNetItem     = 'NET';
  IniServerItem  = 'SERVER';
  IniIpItem      = 'IP';
  IniDBPathItem  = 'DB_PATH';
  IniDBType      = 'DB_TYPE';
  IniConnType    = 'CONN_TYPE';
  IniDBPort      = 'DB_PORT';

  IniUserItem      = 'USER';
  IniEncodePswItem = 'ENCODE_PSW';
  IniPasswdItem    = 'PASSWD';

  IniMemUsrItem        = 'MEM_USR';
  IniMemPwdItem        = 'MEM_PWD';
  IniMemPwdItemVisible = 'MEM_PWD_VISIBLE';

  IniRunOnInterbase = 'RUN_ON_IB';

type
  TProtocol = (TCP, SPX, NamedPipe, Local);

  TAbsDBSettings = class
  private
    // settingi zwiazane z baza danych
    function IsDBAlias(const DatabaseName: string): Boolean;
    function ExtractLinuxPath(const DatabaseName: string): string;
  public
    // nie-zapisywane do ini
    SysLogin,
      SpecialPassword: Boolean; //znacznik czy u�ytkownik otrzyma� specjalne has�o daj�ce dost�p do ukrytych opcji
    // zapisywane do ini
    Server, IP, DBPath: string;
    Port : Integer;
    ConnType: TProtocol;
    DBType: Integer;
    LastUser, LastPwd: string;
    EncodePsw: Boolean;
    LastUserName: string;
    MemUser, MemPwd, MemPwdVisible: Boolean;
    RunOnInterbase:Boolean;
    constructor Create;
    destructor Destroy; override;
    function SaveToIni(IniFilePath: string = ''): Boolean; virtual;
    function SaveMemToIni(IniFilePath: string = ''): Boolean; virtual;
    function LoadFromIni(IniFilePath: string = ''): Boolean; virtual;

    function GetServer(FullName:Boolean=True): string;
    function Net(): Boolean; overload;
    function GetConnType(): TProtocol; overload;
    function GetConnType(vType: Integer): TProtocol; overload;
    function GetConnType(vType: TProtocol): Integer; overload;
    function GetDBInfoPath(InfoPath: string): string; virtual;
    function GetDBRejestrPath(RejestrPath: string): string; virtual;
    function IsConnNet():Boolean;
    function GetDefPort():Integer;
    function IsSYSDBA():Boolean;
    function GetRemoteBackupLocation(DBFile:string): string;
    // usuwa tekst "Word" z wej�ciowego �acucha SQL
    function DeleteWordFromSQL( SQL : string; Word : string ) : string;
    // zwraca kod SQL kontaktacji z ew. dodaniem spacji
    //  w ilo�ci "Spaces" w zalezno�ci od uzytego servera danych:
    //  DBType == -1 == domy�lny z  DBStan
    //  DBType >= 0 == narzucony (np. inny w og�lnym DBStan)
    function GetDBConcatStr( Spaces : integer = 0; DBType : integer = -1 ) : string;
    // dla MS SQL zwraca warto�� "[dbo]." dla FireBird ""
    //   DBType == patrz wy�ej
    function SQLFunctionPrefix( DBType : integer = -1 ) : string;
  end;

var
  DBStan              : TAbsDBSettings;
  DefUsr              : string;
  DefMemUsr           : Boolean;
  DefMemPwd           : Boolean;
  cSysDBA             : string = 'SYSDBA';

implementation

uses
  CStrings, IniFiles, uEncrypt;

{ TAbsDBSettings }

constructor TAbsDBSettings.Create;
begin
  Server := '';
  IP := '';
  DBPath := '';
  LastUser := DefUsr;
  LastPwd := '';
  ConnType := Local;
  MemUser := DefMemUsr;
  MemPwd := DefMemPwd;
  RunOnInterbase := False;
end;

destructor TAbsDBSettings.Destroy;
begin
  inherited;
end;

function TAbsDBSettings.SaveToIni(IniFilePath: string): Boolean;
begin
  if IniFilePath = '' then
    IniFilePath := AppData + '\' + MainIni;
  with TIniFile.Create(IniFilePath) do try
    WriteBool(IniSerwerSect, IniNetItem, Net);
    WriteString(IniSerwerSect, IniServerItem, Server);
    WriteString(IniSerwerSect, IniIpItem, IP);
    WriteString(IniSerwerSect, IniDBPathItem, DBPath);
    WriteInteger(IniSerwerSect, IniDBType, DBType);
    WriteInteger(IniSerwerSect, IniConnType, GetConnType(ConnType));
    WriteInteger(IniSerwerSect, IniDBPort, Port);
    if not DBStan.SpecialPassword then begin
      WriteString(IniOperatSect, IniUserItem, LastUser);
      if not(EncodePsw) then begin
        EncodePsw:=not(EncodePsw);//zmiana wartosci od razu w zmiennej ktora moze byc wykrozystywana po SaveToIni
        WriteBool(IniOperatSect, IniEncodePswItem, EncodePsw);
      end;
      WriteString(IniOperatSect, IniPasswdItem, ConvertStrToStrInt(SimpleCode(LastPwd)));
    end;
    Result := True;
  finally
    Free;
  end;
end;

function TAbsDBSettings.LoadFromIni(IniFilePath: string): Boolean;
begin
  if IniFilePath = '' then
    IniFilePath := AppData + '\' + MainIni;
  with TIniFile.Create(IniFilePath) do try
    Server := ReadString(IniSerwerSect, IniServerItem, '');
    IP := ReadString(IniSerwerSect, IniIpItem, '10.58.10.156');
    DBPath := ReadString(IniSerwerSect, IniDBPathItem, 'RPT');
    LastUser := ReadString(IniOperatSect, IniUserItem, DefUsr);
    EncodePsw:=ReadBool(IniOperatSect, IniEncodePswItem, False);
    LastPwd := ReadString(IniOperatSect, IniPasswdItem, '');
    if EncodePsw then
      LastPwd := SimpleDeCode(ConvertStrIntToStr(ReadString(IniOperatSect, IniPasswdItem, '')));
    MemUser := ReadBool(IniOperatSect, IniMemUsrItem, DefMemUsr);
    MemPwd := ReadBool(IniOperatSect, IniMemPwdItem, DefMemPwd);
    //wartosc ta jest tymczasowa, potem zczytywana jest z bazy w AfterConnect
    MemPwdVisible := ReadBool(IniOperatSect, IniMemPwdItemVisible, True);
    ConnType := GetConnType(ReadInteger(IniSerwerSect, IniConnType, 1));
    DBType := ReadInteger(IniSerwerSect, IniDBType, 0);
    Port := ReadInteger(IniSerwerSect, IniDBPort, GetDefPort());
    RunOnInterbase := ReadBool(IniSerwerSect,IniRunOnInterbase,RunOnInterbase);//czy program moze pracowac na serwerze Interbase
    Result := True;
  finally
    Free;
  end;
end;

function TAbsDBSettings.GetConnType(vType: Integer): TProtocol;
begin
  Result := Local;
  case vType of
    0: Result := Local;
    1, 2: Result := TCP;
  end
end;

function TAbsDBSettings.GetConnType(vType: TProtocol): Integer;
begin
  case vType of
    Local: Result := 0;
    TCP: Result := 1;
  else
    Result := 1;
  end;
end;

function TAbsDBSettings.Net: Boolean;
begin
  Result := (DBType <> dtInterbase) or (ConnType <> Local);
end;

function TAbsDBSettings.GetServer(FullName: Boolean = True{czy nazwa serwera razem z portem}): string;
var
  iPort: Integer;
  sIP: string;
begin
  Result := 'localhost';
  iPort := Port;
  sIP := IP;
  if iPort = 0 then
    iPort := 3050;
  if sIP = '' then
    sIP := Result;

  case ConnType of
    TCP: if FullName then
         begin
           if DBType = dtInterbase then
           begin
             if (Pos('/', sIP) = 0) then
               Result := sIP + '/' + IntToStr(iPort)
             else
               Result := sIP;
           end
           else
             Result := sIP
         end
         else
           Result := CopyBefore(sIP, '/');
    Local:
      begin
        //nazwa serwera razem z portem
        if FullName then
        begin
          if (Pos('/', sIP) = 0 ) then
            Result := Result + '/' + IntToStr(iPort);
        end;
      end;
  end;
end;

function TAbsDBSettings.GetConnType: TProtocol;
begin
  if ConnType=Local then begin
    if Win32Platform = VER_PLATFORM_WIN32_NT then
      Result := TCP
    else
      Result := ConnType;
  end
  else
    Result := ConnType;
end;

function TAbsDBSettings.GetDBInfoPath(InfoPath: string): string;
var
  Dir                 : string;
  I                   : Integer;
begin

  I := LastDelimiter('/\', DBPath);
  if (I > 1) and ((DBPath[I] = '\') or (DBPath[I] = '/')) and
    (not IsDelimiter('/\', DBPath, I - 1)) then Dec(I);
  if I>0 then
    Dir := Copy(DBPath, 1, I + 1)
  else
    Dir:='';
  // Linux: jezeli glowna baza danych jest w katalogu zbiory, to zachowujemy wielkosc liter
  // w nazwie katalogu

  Result := Dir + InfoPath;
end;

function TAbsDBSettings.GetDBRejestrPath(RejestrPath: string): string;
begin
  Result := '';
end;  

function TAbsDBSettings.SaveMemToIni(IniFilePath: string): Boolean;
begin
  if IniFilePath = '' then
    IniFilePath := AppData + '\' + MainIni;
  with TIniFile.Create(IniFilePath) do try
    WriteBool(IniOperatSect, IniMemUsrItem, MemUser);
    WriteBool(IniOperatSect, IniMemPwdItemVisible, True);//(TUrParametr.GetParam(DM.DBMain,UR_PAR_PASS_MEM)=False));
    WriteBool(IniOperatSect, IniMemPwdItem, (MemPwdVisible)and(MemPwd));
    Result := True;
  finally
    Free;
  end;
end;

function TAbsDBSettings.GetDefPort: Integer;
begin
  case DBType of
    dtInterbase:
      Result:=3050;
    dtMSSQL:
      Result:=1433;
    dtOracle:
      Result:=1521;
    else
      Result:=0;
  end;
end;

function TAbsDBSettings.GetDBConcatStr( Spaces : integer = 0; DBType : integer = -1 ) : string;
var
  S : string;
begin
  if DBType < 0 then
    DBType := DBStan.DBType;
  case DBType of
    dtInterbase:
      Result:='||';
    dtMSSQL:
      Result:='+';
  end;
  if Spaces > 0 then
  begin
    SetLength(S,Spaces);
    FillChar(S[1],Spaces,#32);
    Result := Result+#39+S+#39+Result;
  end;
end;

// dla MS SQL zwraca warto�� "[dbo]." dla FireBird ""
function TAbsDBSettings.SQLFunctionPrefix( DBType : integer = -1 ) : string;
begin
  if DBType < 0 then
    DBType := DBStan.DBType;
  case DBType of
    dtInterbase:
      Result:='';
    dtMSSQL:
      Result:='[dbo].';
  end;
end;

// usuwa z �a�cucha SQL ��dany ci�g znak�w zwraca TRUE je�li wykonano
function TAbsDBSettings.DeleteWordFromSQL( SQL : string; Word : string ) : string;
var
  P : integer;
begin
  P := pos(Word,SQL);
  if P <> 0 then
    system.Delete(SQL,P,length(Word));
  Result := SQL;
end;

function TAbsDBSettings.IsSYSDBA: Boolean;
begin
  Result := (UpperCase(cSysDBA) = 'SYSDBA');
end;

function TAbsDBSettings.IsConnNet: Boolean;
var
  S: string;
begin
  S := GetServer(False);
  if (S = '') or (S = '127.0.0.1') or (LowerCase(S) = 'localhost') then
    Result := False
  else
    Result := True;
end;

function TAbsDBSettings.GetRemoteBackupLocation(DBFile: string): string;
begin
  if not IsDBAlias(DBFile) then
  begin
    Result := ExtractLinuxPath(DBFile);
    if Result = '' then
      Result := ExtractFilePath(DBFile);
  end
  else
    Result:='';//na razie jesli alias to nie robimy archiwum
end;

function TAbsDBSettings.IsDBAlias(const DatabaseName: string): Boolean;
begin
  Result := (Pos('\', DatabaseName) = 0) and (Pos('/', DatabaseName) = 0);
end;

function TAbsDBSettings.ExtractLinuxPath(
  const DatabaseName: string): string;
var
  i:Integer;
begin
  I := LastDelimiter('/', DatabaseName);
  Result := Copy(DatabaseName, 1, I);
end;

end.
