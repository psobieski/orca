unit uAbsConnConfig;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  ExtCtrls,
  cxLookAndFeelPainters,
  cxButtons,
  cxTextEdit,
  cxControls,
  cxContainer,
  cxEdit,
  cxGroupBox,
  cxMaskEdit,
  cxDropDownEdit,
  Menus,
  cxGraphics, cxSpinEdit, UnitClassDBSettings;

type
  TFoConfig = class(TForm)
    Panel1: TPanel;
    GroupBox2: TcxGroupBox;
    Label2: TLabel;
    lblPath: TLabel;
    eMserverIP: TcxTextEdit;
    eMpathC: TcxTextEdit;
    eMpath: TcxButton;
    eMTyp: TcxComboBox;
    Label1: TLabel;
    opDlg: TOpenDialog;
    bCancel: TcxButton;
    bOk: TcxButton;
    eMDBTyp: TcxComboBox;
    Label4: TLabel;
    btnSearch: TcxButton;
    ePort: TcxSpinEdit;
    Label3: TLabel;
    procedure eMpathClick(Sender: TObject);
    procedure eMpathCDblClick(Sender: TObject);
    procedure eMTypPropertiesChange(Sender: TObject);
    procedure eMDBTypPropertiesChange(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure eMserverIPPropertiesChange(Sender: TObject);
  private
    { Private declarations }
    PathDefault_ : string;
    function PathVisible():Boolean;
  public
    { Public declarations }
    class function Execute(PathDefault: string = ''):Boolean; //ShowConfigJedn;
  end;

var
  FoConfig            : TFoConfig;

implementation

uses
  CStrings,
  UnitMainForm;

{$R *.DFM}

function Dir_Up(k: string): string;
var
  i, len, kop         : integer;
begin
  Result := '';
  len := length(k);
  kop := 0;
  for i := len downto 1 do begin
    if k[i] = '\' then begin
      kop := i;
      break;
    end;
  end;
  Result := Copy(k, 1, kop - 1);
end;

procedure TFoConfig.eMpathClick(Sender: TObject);
var
  temp                : string;
begin
  //zmieni� GetDBPath je�li ma by� domy�lny inny katalog
  temp := ExtractFileDir(Application.ExeName);
  if FileExists(temp) then
    opDlg.FileName := temp
  else
    opDlg.InitialDir := '.';

  if opDlg.Execute then
    eMpathC.Text := opDlg.FileName;
end;

procedure TFoConfig.eMpathCDblClick(Sender: TObject);
begin
  with CreateMessageDialog('Czy wprowadzi� domy�ln� warto�� �cie�ki?', mtConfirmation, [mbYes, mbNo]) do
  try
    if ShowModal = mrYes then
    begin
      if PathDefault_ <> '' then
        eMPathC.Text := PathDefault_
      else
        eMPathC.Text := '.\' + 'RPT.gdb';
    end;
  finally
    Free;
  end;
end;

class function TFoConfig.Execute(PathDefault: string):Boolean;
begin
  if DBStan = nil then
  begin
    DBStan := TAbsDBSettings.Create();
    DBStan.LoadFromIni(AppData + '\' + MainIni);
  end;
  
  Result := False;
  with Create(nil) do try
    PathDefault_ := PathDefault;
    eMpathC.Text := DBStan.DBPath;
    eMserverIP.Text := DBStan.IP;
    if DBStan.GetConnType(DBSTan.ConnType) < 2 then
      eMTyp.ItemIndex := DBStan.GetConnType(DBSTan.ConnType)
    else
      eMTyp.ItemIndex := 1;
    eMTyp.Properties.OnChange(nil);

    //if not(DBUtils.IsSQLServerSupported()) then  //zawsze not supported
      if eMDBTyp.Properties.Items.Count>1 then
        eMDBTyp.Properties.Items.Delete(1);

    case DBStan.DBType of
      dtInterbase:
        eMDBTyp.ItemIndex := 0;
      dtMSSQL:
        eMDBTyp.ItemIndex := 1;
    end;
    eMpath.Visible:=PathVisible();
    ePort.Value := DBStan.Port;
    if ShowModal = mrOK then begin
      DBStan.ConnType := DBStan.GetConnType(eMTyp.ItemIndex);
      DBStan.IP := eMserverIP.Text;
      DBStan.DBPath := Remove(['"'], Trim(eMpathC.Text));
      DBStan.Port := ePort.Value;
      case eMDBTyp.ItemIndex of
        0: DBStan.DBType := dtInterbase;
        1: DBStan.DBType := dtMSSQL;
      end;
      DBStan.SaveToIni(AppData + '\' + MainIni);
      Result := True;
    end;
  finally
    Release;
  end;
end;

procedure TFoConfig.eMTypPropertiesChange(Sender: TObject);
begin
  case eMTyp.ItemIndex of
    0: begin
        eMserverIP.Enabled := False;
        btnSearch.Enabled := False;
      end;
    1: begin
        eMserverIP.Enabled := True;
        btnSearch.Enabled := True;
      end;
  end;
  eMpath.Visible:=PathVisible();
end;

procedure TFoConfig.eMDBTypPropertiesChange(Sender: TObject);
begin
  case eMDBTyp.ItemIndex of
    0: {//interbase} begin
        lblPath.Caption := '�cie�ka do zbioru';
        if eMTyp.ItemIndex = 0 then
          eMpath.Visible := True
        else
          eMpath.Visible := False;
        ePort.Value := 3050;
      end;
    1: {//mssql} begin
        lblPath.Caption := 'Nazwa bazy danych';
        eMpath.Visible := False;
        ePort.Value := 1433;        
      end;
  end;
end;

procedure TFoConfig.btnSearchClick(Sender: TObject);
//var
//  IP,CompName,Instance:string;
begin
//  case eMDBTyp.ItemIndex of
//    0: {//interbase} begin
//      if NetScan(dtInterbase,ePort.Value,IP,CompName,Instance) then
//        eMserverIP.Text:=IP;
//    end;
//    1: {//mssql} begin
//      if NetScan(dtMSSQL,ePort.Value,IP,CompName,Instance) then
//        eMserverIP.Text:=Instance;
//    end;
//  end;
end;

function TFoConfig.PathVisible: Boolean;
begin
  if ((eMTyp.ItemIndex=1) and (eMserverIP.Text<>'127.0.0.1') and (UpperCase(eMserverIP.Text)<>'LOCALHOST')) or (DBStan.DBType <> dtInterbase) then
    Result := False
  else
    Result := True;
end;

procedure TFoConfig.eMserverIPPropertiesChange(Sender: TObject);
begin
  eMpath.Visible:=PathVisible();
end;

end.
