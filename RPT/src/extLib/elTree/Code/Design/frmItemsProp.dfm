�
 TITEMSPROPDLG 0  TPF0TItemsPropDlgItemsPropDlgLeft"Top� BorderStylebsDialogCaptionElTree Items EditorClientHeight� ClientWidth2Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnCreate
FormCreatePixelsPerInch`
TextHeight 	TGroupBoxItemsGBLeft Top Width1Height� CaptionItemsTabOrder TElTreeTreeLeftTopWidth� Height� ActiveBorderType	fbtSunkenAlwaysKeepSelection
AutoExpand
AutoLookupAutoLineHeight	BarStyleBorderStylebsSingleChangeStateImageCustomCheckboxesCustomPlusMinusDeselectChildrenOnCollapseDoInplaceEditDraggableSectionsDragAllowed	DragTrgDrawMode	ColorRectDragTypedtDelphiDragImageModedimNeverDrawFocusRect	ExpandOnDblClick	ExpandOnDragOverFilteredVisibilityFlat	FlatFocusedScrollbars	FocusedSelectColorclHighlightForcedScrollBarsssNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style FullRowSelect	HeaderActiveFilterColorclBlackHeaderHeightHeaderHotTrackHeaderInvertSortArrowsHeaderSections.Data
   ����    HeaderFilterColor	clBtnText
HeaderFlatHideFocusRectHideHintOnTimer	HideHintOnMove	HideSelectColor	clBtnFaceHideSelectionHorizontalLines	HideHorzScrollBarHideVertScrollBarHorzScrollBarStyles.ActiveFlat#HorzScrollBarStyles.BlendBackground	HorzScrollBarStyles.Color	clBtnFaceHorzScrollBarStyles.Flat	 HorzScrollBarStyles.MinThumbSizeHorzScrollBarStyles.OwnerDraw$HorzScrollBarStyles.SecondaryButtons!HorzScrollBarStyles.SecondBtnKindsbkOpposite"HorzScrollBarStyles.ShowLeftArrows	#HorzScrollBarStyles.ShowRightArrows	!HorzScrollBarStyles.ShowTrackHintHorzScrollBarStyles.ThumbModeetmAutoHorzScrollBarStyles.ThumbSize HorzScrollBarStyles.WidthInactiveBorderTypefbtFlatLeftPosition 
LineHeight
LinesColor	clBtnFace
LinesStylepsSolidLockHeaderHeightMainTreeColumn MoveColumnOnDragMoveFocusOnCollapseMultiSelectOwnerDrawMask~~@~~PathSeparator RightAlignedTextRightAlignedTreeRowHotTrack	RowSelect	ScrollTrackingSelectColumn�ShowButtons	ShowColumnsShowCheckboxes
ShowImages		ShowLines	ShowRoot	ShowRootButtons	SelectionModesmUsualSortDirsdAscendSortModesmNoneSortSection SortTypestCustomStickyHeaderSectionsTabOrderTabStop	Tracking	UnderlineTracked	VerticalLinesVertScrollBarStyles.ActiveFlat#VertScrollBarStyles.BlendBackground	VertScrollBarStyles.Color	clBtnFaceVertScrollBarStyles.Flat	 VertScrollBarStyles.MinThumbSizeVertScrollBarStyles.OwnerDraw$VertScrollBarStyles.SecondaryButtons!VertScrollBarStyles.SecondBtnKindsbkOpposite"VertScrollBarStyles.ShowLeftArrows	#VertScrollBarStyles.ShowRightArrows	!VertScrollBarStyles.ShowTrackHint	VertScrollBarStyles.ThumbModeetmAutoVertScrollBarStyles.ThumbSize VertScrollBarStyles.Width	TextColorclWindowTextBkColorclWindowOnItemFocusedTreeItemFocused
OnDragDropTreeDragDrop
OnDragOverTreeDragOverOnStartDragTreeStartDrag  TButton
NewItemBtnLeft� TopWidthIHeightCaptionNew ItemTabOrder OnClickNewItemBtnClick  TButton
SubitemBtnLeft� Top0WidthIHeightCaptionNew SubitemTabOrderOnClickSubitemBtnClick  TButton	DeleteBtnLeft� TopPWidthIHeightCaptionDeleteTabOrderOnClickDeleteBtnClick  TButtonSaveBtnLeft� Top� WidthIHeightCaptionSave TabOrderOnClickSaveBtnClick  TButtonLoadBtnLeft� Top� WidthIHeightCaptionLoadTabOrderOnClickLoadBtnClick  TButtonEditBtnLeft� ToppWidthIHeightCaptionEdit ...TabOrderOnClickEditBtnClick   TButtonOKBtnLeft?Top� WidthKHeightCaptionOKDefault	ModalResultTabOrder OnClick
OKBtnClick  TButton	CancelBtnLeft� Top� WidthKHeightCancel	CaptionCancelModalResultTabOrder  TButtonApplyBtnLeft� Top� WidthKHeightCaptionApplyTabOrderOnClick
OKBtnClick  TOpenDialogOpenDlg
DefaultExt*.bwtFilter3ElTree Items file (*.elt)|*.elt|All files (*.*)|*.*OptionsofHideReadOnlyofPathMustExistofFileMustExistofShareAware TitleElTree Items load dialogTop�   TSaveDialogSaveDlg
DefaultExt*.bwtFilter3ElTree Items file (*.elt)|*.elt|All files (*.*)|*.*OptionsofHideReadOnlyofPathMustExist TitleElTree ITems save dialogLeft Top�    