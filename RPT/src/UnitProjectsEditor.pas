unit UnitProjectsEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ExtCtrls, cxLabel, cxEdit, cxTextEdit, cxControls, cxContainer,
  DataModuleStyles, DB,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid;

type
  TProjectsForm = class(TForm)
    edtSearch: TcxTextEdit;
    cxLabel1: TcxLabel;
    pnlBottom: TPanel;
    btnOk: TcxButton;
    btnCancel: TcxButton;
    btnAdd: TcxButton;
    btnRemove: TcxButton;
    tvLista: TcxGridDBTableView;
    gListaLevel1: TcxGridLevel;
    gLista: TcxGrid;
    pnlRight: TPanel;
    pnlSzukaj: TPanel;
    gvcName: TcxGridDBColumn;
    gvcDescription: TcxGridDBColumn;
    gvcId: TcxGridDBColumn;
    DSMain: TDataSource;
    procedure btnRemoveClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
  private

  public
    constructor Create(aOwner: TComponent);
    destructor Destroy; override;
    class function Execute(aOwner: TComponent): Integer;
  end;

implementation

uses UnitDMDatabase;

{$R *.dfm}

class function TProjectsForm.Execute(aOwner: TComponent): Integer;
var
  aID: Integer;
begin
  with TProjectsForm.Create(aOwner) do
  begin
    DSMain.DataSet := DMDatabase.qListProjects;
    DMDatabase.LoadListProjects;
    aID := DSMain.DataSet.FieldByName('ID_PROJEKT').AsInteger;
    if ShowModal = mrCancel then
      DSMain.DataSet.Locate('ID_PROJEKT',aID,[]);
    Result := DSMain.DataSet.FieldByName('ID_PROJEKT').AsInteger;
  end;
end;

constructor TProjectsForm.Create(aOwner: TComponent);
begin
  inherited;
end;

destructor TProjectsForm.Destroy;
begin
  inherited;
end;

procedure TProjectsForm.btnRemoveClick(Sender: TObject);
begin
  if not DSMain.DataSet.IsEmpty then
    if MessageDlg('Czy na pewno chcesz usun�� wybrany projekt?', mtConfirmation,[mbYes, mbNo],0) = mrYes then
      DSMain.DataSet.Delete;
end;

procedure TProjectsForm.btnAddClick(Sender: TObject);
begin
  DSMain.DataSet.Append;
  DSMain.DataSet.FieldByName('ID_PROJEKT').AsInteger := DMDatabase.GenerateId('GEN_RPT_PROJEKTY',1);
end;

end.
