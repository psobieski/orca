unit uEncrypt;

interface

function MyEncrypt(const S: AnsiString; Key: Word = 1223): AnsiString;
function EncodeKod(kod: string): string;
function CreateGuid(max: integer): string;

function SimpleCode(Value: string): string;
function SimpleDeCode(Value: string): string;
function ConvertStrIntToStr(Value: string; Separator: Char = ','): string;
function ConvertStrToStrInt(Value: string; Separator: Char = ','): string;

implementation
uses Sysutils,
  cUtils,
  cStrings;

const
  //sta�e do kodowania
  StartKey            = 9;
  MultKey             = 17;
  AddKey              = 35;
  C1                  = 23322;
  C2                  = 4343;

  {$R-}
  {$Q-}

function Encode(const S: AnsiString): AnsiString;
const
  Map                 : array[0..9] of Char = '0123456789';
var
  I                   : LongInt;
begin
  I := 0;
  Move(S[1], I, Length(S));
  case Length(S) of
    1:
      Result := Map[I mod 10];
    2:
      Result := Map[(I shr 6) mod 10] +
        Map[(I shr 12) mod 10];
    3:
      Result := Map[(I shr 6) mod 10] +
        Map[(I shr 12) mod 10] + Map[(I shr 18) mod 10]
  end
end;

function PostProcess(const S: AnsiString): AnsiString;
var
  SS                  : AnsiString;
begin
  SS := S;
  Result := '';
  while SS <> '' do begin
    Result := Result + Encode(Copy(SS, 1, 3));
    Delete(SS, 1, 3)
  end
end;

function InternalEncrypt(const S: AnsiString; Key: Word): AnsiString;
var
  I                   : Word;
  Seed                : Word;
begin
  Result := S;
  Seed := Key;
  for I := 1 to Length(Result) do begin
    Result[I] := Char(Byte(Result[I]) xor (Seed shr 8));
    Seed := (Byte(Result[I]) + Seed) * Word(C1) + Word(C2)
  end
end;

function MyEncrypt(const S: AnsiString; Key: Word): AnsiString;
begin
  Result := PostProcess(InternalEncrypt(S, Key))
end;

function EncodeKod(kod: string): string;
var
  tekst               : string;
  i                   : integer;
begin
  tekst := MyEncrypt(kod);
  Result := '';
  for i := 1 to length(tekst) do
    Result := Result + tekst[i];
end;

function CreateGuid(max: integer): string;
var
  i, znak             : integer;
begin
  Randomize;
  Result := '';
  for i := 1 to max do begin
    znak := Random(9);
    Result := Result + IntToStr(znak);
  end;
end;

function SimpleCode(Value: string): string;
var
  I                   : Byte;
  SKey                : Integer;
begin
  SKey := StartKey;
  Result := '';
  for I := 1 to Length(Value) do begin
    Result := Result + CHAR(Byte(Value[I]) xor (SKey shr 8));
    SKey := (Byte(Result[I]) + StartKey) * MultKey + AddKey;
  end;
end;

function SimpleDeCode(Value: string): string;
var
  I                   : Byte;
  SKey                : Integer;
begin
  Result := '';
  SKey := StartKey;
  for I := 1 to Length(Value) do begin
    Result := Result + CHAR(Byte(Value[I]) xor (SKey shr 8));
    SKey := (Byte(Value[I]) + StartKey) * MultKey + AddKey;
  end;
end;

{$R+}
{$Q+}

function ConvertStrIntToStr(Value: string; Separator: Char = ','): string;
var
  Tab                 : StringArray;
  i                   : integer;
begin
  Tab := Split(Value, Separator);
  Result := '';
  for i := Low(Tab) to High(Tab) do
    if IsInteger(Tab[i]) then
      Result := Result + Chr(StrToInt(Tab[i]));
end;

function ConvertStrToStrInt(Value: string; Separator: Char = ','): string;
var
  i                   : integer;
begin
  Result := '';
  for i := 1 to Length(Value) do
    Result := Result + IntToStr(Ord(Value[i])) + Separator;
end;

end.
