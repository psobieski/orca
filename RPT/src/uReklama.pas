unit uReklama;

interface

uses
  
  
  
  Classes,
  Graphics,
  Controls,
  Forms,
  
  StdCtrls,
  ExtCtrls,
  RUrllabel,
  
  cxButtons, Menus, cxLookAndFeelPainters, jpeg;

type

  TFOAutorze = class(TForm)
    pnlMain: TPanel;
    pnlLogo: TPanel;
    NazwaProgramu: TLabel;
    ikona: TImage;
    lOpis: TLabel;
    Panel1: TPanel;
    btOK: TcxButton;
    imgWolters: TImage;
    procedure btOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  end;

var
  FOAutorze: TFOAutorze;

procedure ShowAboutProgram(NameProg: string; sColor: TColor =
  cl3DLight; Opis: string = '');

implementation

{$R *.dfm}

procedure ShowAboutProgram(NameProg: string; sColor: TColor; Opis: string);
begin
  with TFOAutorze.Create(nil) do
  try
    Color := sColor;
    Ikona.Picture.Icon := Application.Icon;
    NazwaProgramu.Caption := NameProg;
    lOpis.Caption := Opis;
    ShowModal;
  finally
    Free;
  end;
end;

procedure TFOAutorze.btOKClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TFOAutorze.FormShow(Sender: TObject);
begin
  //Color := MainForm.Color;
end;

end.
