unit UnitFormWyniki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxGraphics, cxLabel, cxMemo, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxDBEdit, DB, SDEngine,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, UnitClassWyniki;

type
  TFormWyniki = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btnOk: TcxButton;
    btnCancel: TcxButton;
    memoKomentarz: TcxMemo;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    dsStatusWynik: TDataSource;
    QueryStatusWynik: TSDQuery;
    lookUpStatusWynik: TcxLookupComboBox;
    QueryWynik: TSDQuery;
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    fIdWynik : integer;
    fIdTest  : integer;
    procedure openQueryStatus;
    procedure dajDane;
    procedure saveWynik();
    procedure DodajIZapiszWynik();
    procedure czyscDane;
  public
    { Public declarations }
    procedure setIdWynik(i: integer);
    procedure setIdTest(i: integer);
    procedure showWynikTestu(idwynik: integer; idTest: integer);
    procedure dodajWynikTestu(idTest: integer);
  end;



implementation


{$R *.dfm}
procedure TFormWyniki.btnCancelClick(Sender: TObject);
begin
  close;
end;

procedure TFormWyniki.FormCreate(Sender: TObject);
begin
  openQueryStatus;
end;

procedure TFormWyniki.openQueryStatus;
begin
  QueryStatusWynik.DatabaseName := 'RPT';
  QueryStatusWynik.Close;
  QueryStatusWynik.sql.Text := 'select * from RPT_WYNIKI_STATUS order by ID_WYNIK_STATUS';
  QueryStatusWynik.open;
end;

procedure TFormWyniki.saveWynik;
begin
  QueryWynik.Close;
  QueryWynik.SQL.Clear;
  QueryWynik.SQL.Add('update RPT_WYNIKI_STATUS');
  QueryWynik.SQL.Add('set opis=:opis');
  QueryWynik.SQL.Add('where ID_WYNIK = :ID_WYNIK');
  QueryWynik.ParamByName('ID_WYNIK').AsInteger := fIdWynik;
  QueryWynik.ParamByName('ID_TEST').AsInteger := fIdTest;
  QueryWynik.ParamByName('ID_WYNIK_STATUS').AsInteger := lookUpStatusWynik.EditValue;
  QueryWynik.ParamByName('opis').AsBlob := memoKomentarz.Text;
  QueryWynik.ParamByName('DATA_WYKONANIA').AsDateTime := now;
  QueryWynik.ExecSQL;
end;

procedure TFormWyniki.dajDane;
begin
  QueryWynik.Close;
  QueryWynik.SQL.Clear;
  QueryWynik.SQL.Text := 'select * from RPT_TESTY_WYNIKI where ID_WYNIK = :ID_WYNIK';
  QueryWynik.ParamByName('ID_WYNIK').AsInteger := fIdWynik;
  QueryWynik.open;

  lookUpStatusWynik.EditValue := QueryWynik.fieldbyname('ID_WYNIK').asinteger;
  memoKomentarz.Text := QueryWynik.fieldbyname('OPIS').AsString;
end;

procedure TFormWyniki.showWynikTestu(idwynik: integer; idTest: integer);
begin

  with TFormWyniki.Create(nil) do
  try
    setIdTest(idTest);
    setIdWynik(idwynik);

    dajDane;
    if ShowModal = mrOk then
      saveWynik;
  finally
    Free;
  end;
end;

procedure TFormWyniki.dodajWynikTestu(idTest: integer);
begin

  with TFormWyniki.Create(nil) do
  try
    setIdTest(idTest);
    
    czyscDane;
    if ShowModal = mrOk then
      DodajIZapiszWynik;
  finally
    Free;
  end;
end;

procedure TFormWyniki.czyscDane;
begin
  lookUpStatusWynik.EditValue := 1;
  memoKomentarz.Clear;
end;

procedure TFormWyniki.DodajIZapiszWynik;
begin
  QueryWynik.Close;
  QueryWynik.SQL.Clear;
  QueryWynik.SQL.Add('insert into RPT_TESTY_WYNIKI');
  QueryWynik.SQL.Add('(ID_TEST,ID_WYNIK_STATUS,opis,DATA_WYKONANIA)');
  QueryWynik.SQL.Add('values(:ID_TEST,:ID_WYNIK_STATUS,:opis,:DATA_WYKONANIA)');
  QueryWynik.ParamByName('ID_TEST').AsInteger := fIdTest;
  QueryWynik.ParamByName('ID_WYNIK_STATUS').AsInteger := lookUpStatusWynik.EditValue;
  QueryWynik.ParamByName('opis').AsBlob := memoKomentarz.Text;
  QueryWynik.ParamByName('DATA_WYKONANIA').AsDateTime := now;
  QueryWynik.ExecSQL;;
end;

procedure TFormWyniki.setIdTest(i: integer);
begin
  fIdTest := i;
end;

procedure TFormWyniki.setIdWynik(i: integer);
begin
  fIdWynik := i;
end;

end.
