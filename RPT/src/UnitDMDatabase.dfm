object DMDatabase: TDMDatabase
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Left = 819
  Top = 444
  Height = 366
  Width = 401
  object SDDatabase1: TSDDatabase
    LoginPrompt = False
    DatabaseName = 'RPT'
    DesignOptions = [ddoIsDefaultDatabase, ddoStoreConnected]
    IdleTimeOut = 0
    Params.Strings = (
      'USER NAME=sysdba'
      'PASSWORD=masterkey')
    RemoteDatabase = '10.58.10.156/30555:RPT'
    ServerType = stFirebird
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    Left = 55
    Top = 88
  end
  object qListProjects: TSDQuery
    DatabaseName = 'rpt'
    RequestLive = True
    SQL.Strings = (
      'select * from RPT_PROJEKTY')
    Left = 55
    Top = 27
  end
  object qParam: TSDQuery
    DatabaseName = 'rpt'
    Left = 121
    Top = 30
  end
end
