unit ORCAInteger;

interface

uses
  Classes, ORCASimpleDataTypesIntf, ORCABaseObject;

type
  TORCAInteger = class (TORCABaseObject, IORCAInteger)
  private
    fValue: Integer;
  protected
		function GetValue: Integer; virtual;
    procedure SetValue( aValue: Integer ); virtual;
  public
    constructor Create( const aValue: Integer = 0); virtual;
    function Add( const aValue: Integer ): IORCAInteger;
    function Subtract( const aValue: Integer ): IORCAInteger;
    function Multiply( const aValue: Integer ): IORCAInteger;
    function Divide( const aValue: Integer ): IORCAInteger;
    function Modulo( const aValue: Integer ): IORCAInteger;
    property Value: Integer read GetValue write SetValue;
  end;

  function ToInteger(const aValue: Variant): IORCAInteger;

implementation

uses Variants;

constructor TORCAInteger.Create( const aValue: System.Integer = 0);
begin
  inherited Create;
  fValue := aValue;
end;

function TORCAInteger.Add( const aValue: System.Integer ): IORCAInteger;
begin
  Result := TORCAInteger.Create( fValue + aValue ); 
end;

function TORCAInteger.Subtract( const aValue: System.Integer ): IORCAInteger;
begin
  Result := TORCAInteger.Create( fValue - aValue);
end;

function TORCAInteger.Multiply( const aValue: System.Integer ): IORCAInteger;
begin
  Result := TORCAInteger.Create( fValue * aValue );
end;

function TORCAInteger.Divide( const aValue: System.Integer ): IORCAInteger;
begin
	Result := TORCAInteger.Create( fValue div aValue );
end;

function TORCAInteger.Modulo( const aValue: System.Integer ): IORCAInteger;
begin
  Result := TORCAInteger.Create( fValue mod aValue);
end;

function TORCAInteger.GetValue: System.Integer;
begin
  Result := fValue;
end;

procedure TORCAInteger.SetValue( aValue: System.Integer );
begin
  if (fvalue <> aValue) then
    fValue := aValue;
end;

function ToInteger(const aValue: Variant): IORCAInteger;
var v_TmpInt: System.Integer;
begin
	v_TmpInt := VarAsType( aValue, varInteger );
  Result := TORCAInteger.Create( v_TmpInt );
end;

end.
