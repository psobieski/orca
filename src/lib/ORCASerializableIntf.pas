unit ORCASerializableIntf;

interface

uses
  Classes;

type
 	IORCASerializable = interface( IInterface )
  	['{75F0A544-995E-404C-9B48-D5781DBD43A2}']
    function Serialize: String; stdcall;
    procedure DeSerialize( aData: String ); stdcall;
    function GetText: string; stdcall;
    property Text: String read GetText;
  end;

implementation

end.
