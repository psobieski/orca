unit ORCAExceptions;

interface

uses
  Sysutils, Classes;

const
  SERROR_NIL_ASSIGNMENT = 'Can''t assign %s to nil';
  SERROR_SELF_ASSIGNEMNT = 'Can''t assign to self';
  SERROR_COMPARE_OBJECTS = 'I can''t compare %s with %s.';

type
  EORCABaseException = class(Exception);

  EORCAObjectAssignError = class(EORCABaseException)
  public
    constructor Create(aSource, aDestination: TObject); virtual;
  end;

  EORCAObjectCompareError = class(EORCABaseException)
  public
    constructor Create(aSource, aDestination: TObject); virtual;
  end;

  EORCAStringException = class(EORCABaseException);

  EORCAStringIndexOutOfBounds = class(EORCAStringException);


implementation

constructor EORCAObjectAssignError.Create(aSource, aDestination: TObject);
var v_Msg: string;
begin
  if (aDestination = nil) then
    v_Msg := Format(SERROR_NIL_ASSIGNMENT, [aSource.ClassName])
  else if (aSource = aDestination) then
    v_Msg := SERROR_SELF_ASSIGNEMNT;

  inherited Create(v_Msg);
end;

constructor EORCAObjectCompareError.Create(aSource, aDestination: TObject);
begin
  inherited CreateFmt(SERROR_COMPARE_OBJECTS,[aSource.ClassName, aDestination.ClassName]);
end;

end.
