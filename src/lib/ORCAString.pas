unit ORCAString;

interface

uses
  SysUtils, Classes, ORCASimpleDataTypesIntf, ORCAIteratorIntf,
  ORCAStringBuilderIntf, ORCABaseObject, RegExpr ;

type
  TORCAString = class (TORCABaseObject, IORCAString )
  private
    fValue: String;
    procedure SetUpRegEx( aRegExp: TRegExpr; aOptions: TORCARegExpModifiers );
  protected
	  function GetText: String; virtual;
    function GetChar( Index: Integer ): Char; virtual;
    function IsValidIndex(Index: Integer): Boolean;
  public
    constructor Create( const aValue: String = ''); virtual;
		function TrimLeft: IORCAString;
    function TrimRight: IORCAString;
		function Trim: IORCAString;
    function SubString(const aIndex, aCount: Integer): IORCAString;
    function UpperCase: IORCAString;
    function LowerCase: IORCAString;
    function FormatFloat( const aFormat: String; const aValue: Double): IORCAString; overload;
    function FormatFloat( const aFormat: IORCAString; const aValue: Double): IORCAString; overload;
    function Compare(const aSource: string): Integer; overload;
    function Compare(const aSource: IORCAString): Integer; overload;
    function StringOfChar( const aChar: Char; const aCount: Integer): IORCAString;
    function Contains( aSubString: String ): Boolean;
    function Pos( aSubstring: String ): Integer;
    function Match( const aRegExp: String; aModifiers: TORCARegExpModifiers = [] ): Boolean;
    function RegExReplace( const aRegExp: String; aSubstitution: string; aModifiers: TORCARegExpModifiers ): String;
    function UnitName: String; override;
    function UnitScope: String; override;
    function Iterator: IORCAIterator;
  	property Text: String read GetText;
    property Chars[ index: Integer ]: Char read GetChar ; default;
  end;

implementation

uses Variants, ORCAChar, ORCAIterators, ORCAExceptions;

constructor TORCAString.Create( const aValue: String = '');
begin
  inherited Create;
  fValue := aValue;
end;

function TORCAString.GetText: String;
begin
  result := fValue;
end;

function TORCAString.GetChar( Index: Integer ): Char;
begin
  Result := #0;
  if ( IsValidIndex( Index ) ) then
  	Result := fValue[Index]
  else begin
    raise EORCAStringIndexOutOfBounds.CreateFmt('String index out of bounds %d',[Index]);
  end;
end;

function TORCAString.Contains( aSubString: String ): Boolean;
begin
  Result := System.Pos( aSubString, fValue ) <> 0;
end;

function TORCAString.Pos( aSubstring: String ): Integer;
begin
  Result := System.Pos( aSubstring, fValue );
end;

{ need test }
function TORCAString.Match( const aRegExp: String; aModifiers: TORCARegExpModifiers = [] ): Boolean;
var v_RegExpr: TRegExpr;
begin
  Result := False;
	v_RegExpr := TRegExpr.Create();
  SetUpRegEx(v_RegExpr, aModifiers);
  try
    v_RegExpr.Expression := aRegExp;
    Result := v_RegExpr.Exec( fValue );
  finally
    FreeAndNil( v_RegExpr );
  end;
end;

{ need test }
function TORCAString.RegExReplace( const aRegExp: String; aSubstitution: string; aModifiers: TORCARegExpModifiers ): String;
var v_RegExpr: TRegExpr;
begin
  Result := fValue;
	v_RegExpr := TRegExpr.Create();
  SetUpRegEx(v_RegExpr, aModifiers);
  try
    v_RegExpr.Expression := aRegExp;
    Result := v_RegExpr.Replace(fValue, aSubstitution, true);
  finally
    FreeAndNil( v_RegExpr );
  end;
end;

function TORCAString.IsValidIndex( Index: Integer ): Boolean;
begin
  Result := (Index >= 1) and (Index <= Length( fValue));
end;

function TORCAString.UnitName: String;
begin
  Result := 'ORCAString';
end;

function TORCAString.UnitScope: String;
begin
	Result := 'public';
end;

function TORCAString.TrimLeft: IORCAString;
begin
  fValue := SysUtils.TrimLeft(fValue);
  Result := Self;
end;

function TORCAString.TrimRight: IORCAString;
begin
  fValue := SysUtils.TrimRight(fValue);
  Result := Self;
end;

function TORCAString.Trim: IORCAString;
begin
	fValue := SysUtils.Trim(fValue);
  Result := Self;
end;

function TORCAString.SubString(const aIndex, aCount: Integer): IORCAString;
begin
  Result := TORCAString.Create( System.Copy( fValue, aIndex, aCount) );
end;

function TORCAString.UpperCase: IORCAString;
begin
  fValue := SysUtils.UpperCase( fValue );
  Result := Self;
end;

function TORCAString.LowerCase: IORCAString;
begin
  fValue := SysUtils.LowerCase( fValue );
  Result := Self;
end;

function TORCAString.FormatFloat( const aFormat: String; const aValue: Double): IORCAString;
begin
	fValue := Sysutils.FormatFloat( aFormat, aValue);
  Result := Self;
end;

function TORCAString.FormatFloat( const aFormat: IORCAString; const aValue: Double): IORCAString;
begin
  fValue := SysUtils.FormatFloat( aFormat.Text, aValue);
  Result := Self;
end;

function TORCAString.Compare(const aSource: string): Integer;
begin
  Result := 0;
  if (fValue > aSource) then
  	Result := 1
  else if (fValue < aSource) then
    Result := -1;
end;

function TORCAString.Compare(const aSource: IORCAString): Integer;
begin
  Result := Compare( aSource.Text );
end;

function TORCAString.StringOfChar( const aChar: Char; const aCount: Integer): IORCAString;
var v_Result: String;
    i: Integer;
begin
  v_Result := '';
  for i:=1 to aCount do
    v_Result := v_Result + aChar;

  fValue := v_Result;
  Result := Self;
end;

function TORCAString.Iterator: IORCAIterator;
begin
  result := TORCAStringIterator.Create( fValue ) as IORCAIterator;
end;

procedure TORCAString.SetUpRegEx( aRegExp: TRegExpr; aOptions: TORCARegExpModifiers );
var i:TORCARegExpModifier;

  procedure SetModifierState( aModifer:TORCARegExpModifier; aState: Boolean);
  begin
     case aModifer of
        remCaseLess: aRegExp.ModifierI := aState;
        remGreedy: aRegExp.ModifierG := aState;
        remSingleLine: aRegExp.ModifierS := aState;
        remMultiLine: aRegExp.ModifierM := aState;
        remExtended : aRegExp.ModifierX := aState;
     end
  end;

begin
	for i:=remCaseLess to remExtended do
  begin
     if i in aOptions then
     begin
       SetModifierState(i, True);
     end
     else
     begin
       SetModifierState(i, False);
     end;
  end;
end;

end.
