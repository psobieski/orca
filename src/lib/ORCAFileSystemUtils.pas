unit ORCAFileSystemUtils;

interface

uses
  SysUtils, Classes, ORCABaseObject, ORCABaseObjectIntf;

type
  IORCAFileInfo = interface(IORCABaseObject)
  	['{C252D88E-9311-4ADB-A682-86D4561FB543}']
    function GetFileName: string;
    function GetExtension: String;
    function GetPath: string;
    function GetAttributes: Integer;
    function GetChangeDateTime: TDateTime;
    function GetSize(): Int64;
    procedure SetFileInfo(aFileName: String); overload;
    procedure SetFileInfo(aSearchRecord: TSearchRec); overload;
	  property FileName: String read GetFileName;
    property Extension: String read GetExtension;
    property Path: String read GetPath;
    property Size: Int64 read GetSize;
    property Attributes: Integer read GetAttributes;
    property ChangeDateTime: TDateTime read GetChangeDateTime;
  end;

  TORCAFileInfo = class(TORCABaseObject , IORCAFileInfo)
  private
    fName: string;
    fExtension: string;
    fPath: String;
    fChangeDateTime: TDateTime;
    fSize: Int64;
    fAttributes: Integer;
  protected
    function GetFileName: string;
    function GetExtension: String;
    function GetPath: string;
    function GetAttributes: Integer;
    function GetChangeDateTime: TDateTime;
    function GetSize(): Int64;
  public
	  procedure Assign( aSource: TObject ); override;
    procedure SetFileInfo(aFileName: String); overload;
    procedure SetFileInfo(aSearchRecord: TSearchRec); overload;
    function Equals( aSource: TObject ): Boolean; override;
    function ToString: String; override;
    function UnitName: String; override;
    function UnitScope: String; override;
    property FileName: String read GetFileName;
    property Extension: String read GetExtension;
    property Path: String read GetPath;
    property Size: Int64 read GetSize;
    property Attributes: Integer read GetAttributes;
    property ChangeDateTime: TDateTime read GetChangeDateTime;
  end;

  IORCADirectoryFilter = interface(IORCABaseObject)
  	['{0F3AC9E0-23A0-4C77-9090-659FE0FBF8CA}']
    function Mask: String;
    function Attributes: Integer;
  end;

  IORCADirectoryReader = interface(IORCABaseObject)
  	['{2FA0C97E-0E2C-42D0-876A-A37447F59651}']
    procedure SetRecursiveReads(aRecursive: Boolean);
    procedure SetFilter( aFilter: IORCADirectoryFilter );
    procedure SetPath( aPath: String );
    procedure ReadDirectory(aFileList: IInterfaceList);
  end;

  TORCADirectoryFilter = class( TORCABaseObject, IORCADirectoryFilter )
  private
    fMask: String;
    fAttributes: Integer;
  public
    constructor Create( aMask: String; aAttributes: Integer);
    function Mask: String;
    function Attributes: Integer;
  end;

  TORCADirectoryReader = class( TORCABaseObject, IORCADirectoryReader )
  private
    fRecursive: Boolean;
    fPath: String;
    fFilter : IORCADirectoryFilter;
  public
    constructor Create( aPath: string; aRecursive: Boolean = False; aFilter: IORCADirectoryFilter = nil);
    procedure SetRecursiveReads(aRecursive: Boolean);
    procedure ReadDirectory(aFileList: IInterfaceList);
    procedure SetFilter( aFilter: IORCADirectoryFilter );
    procedure SetPath( aPath: String );
  end;

  IORCADirectory = interface( IORCAFileInfo )
  	['{C3FC287C-7264-43FB-B536-1446881D7BA9}']
    procedure ReadDirectory;
  end;

  TORCADirectory = class (TORCAFileInfo, IORCADirectory)
  private
    fFiles: IInterfaceList;
    fFilter: IORCADirectoryFilter;
    fReader: IORCADirectoryReader;
    fPath: String;
  protected
    function GetReader: IORCADirectoryReader;
    function GetFilter: IORCADirectoryFilter;
  public
    constructor Create(const aPath: String; aFilter: IORCADirectoryFilter = nil);
    destructor Destroy; override;
    procedure ReadDirectory;
  end;

implementation

uses ORCAExceptions;

procedure TORCAFileInfo.SetFileInfo(aSearchRecord: TSearchRec);
begin
  fName := aSearchRecord.Name;
  fExtension := ExtractFileExt( fName );
  fChangeDateTime := FileDateToDateTime( aSearchRecord.Time );
  fSize := aSearchRecord.Size;
  fAttributes := aSearchRecord.Attr;
end;

function TORCAFileInfo.GetExtension: String;
begin
  result := fExtension;
end;

function TORCAFileInfo.GetPath: string;
begin
	result := fPath;
end;

function TORCAFileInfo.GetAttributes: Integer;
begin
	result := fAttributes;
end;

function TORCAFileInfo.GetChangeDateTime: TDateTime;
begin
  Result := fChangeDateTime;
end;

procedure TORCAFileInfo.Assign( aSource: TObject );
var v_SourceData: TORCAFileInfo;
begin
  if (aSource is TORCAFileInfo) then
  begin
    v_SourceData    := TORCAFileInfo( aSource );
    fName 		      := v_SourceData.FileName;
    fExtension      := v_SourceData.Extension;
    fPath				    := v_SourceData.Path;
    fChangeDateTime := v_SourceData.ChangeDateTime;
    fSize					  := v_SourceData.Size;
    fAttributes 	  := v_SourceData.Attributes;
  end else
	  inherited;
end;

function TORCAFileInfo.Equals( aSource: TObject ): Boolean;
var  v_SourceData: TORCAFileInfo;
begin
  Result := inherited Equals(aSource);
  if (Result) then
  	Exit;
  if (not (aSource is TORCAFileInfo)) then
    raise EORCAObjectCompareError.Create(self, aSource);

  v_SourceData := TORCAFileInfo( aSource );

  Result := Result and (fName = v_SourceData.FileName);
  Result := Result and (fExtension = v_SourceData.Extension);
  Result := Result and (fPath = v_SourceData.Path);
  Result := Result and (fChangeDateTime = v_SourceData.ChangeDateTime);
  Result := Result and (fSize = v_SourceData.Size);
  Result := Result and (fAttributes = v_SourceData.Attributes);
end;

function TORCAFileInfo.ToString: String;
begin
	result := FileName;
end;

function TORCAFileInfo.UnitName: String;
begin
  Result := 'ORCAFileSystemUtils';
end;

function TORCAFileInfo.UnitScope: String;
begin
  Result := 'public';
end;

function TORCAFileInfo.GetFileName(): string;
begin
  result := IncludeTrailingPathDelimiter(fPath)+fName;
end;

function TORCAFileInfo.GetSize(): Int64;
begin
  Result := fSize;
end;

procedure TORCAFileInfo.SetFileInfo(aFileName: String);
begin
	fName := ExtractFileName(aFileName);
  fExtension := ExtractFileExt(aFileName);
  fPath := ExtractFilePath(aFileName);
end;

constructor TORCADirectory.Create(const aPath: String; aFilter: IORCADirectoryFilter = nil);
begin
  inherited Create;
  fPath := aPath;
  fFiles := TInterfaceList.Create;
  fReader := TORCADirectoryReader.Create(fPath);
end;

destructor TORCADirectory.Destroy;
begin
  inherited;
  fFiles := nil;
end;

function TORCADirectory.GetReader: IORCADirectoryReader;
begin
  result := fReader;
end;

function TORCADirectory.GetFilter: IORCADirectoryFilter;
begin
	result := fFilter;
end;

procedure TORCADirectory.ReadDirectory;
begin
  fReader.ReadDirectory(fFiles);
end;

constructor TORCADirectoryFilter.Create( aMask: String; aAttributes: Integer);
begin
  inherited Create;
  fMask := aMask;
  fAttributes := aAttributes;
end;

function TORCADirectoryFilter.Mask: String;
begin
  result := fMask;
end;

function TORCADirectoryFilter.Attributes: Integer;
begin
  Result := fAttributes;
end;

constructor TORCADirectoryReader.Create( aPath: string; aRecursive: Boolean = False; aFilter: IORCADirectoryFilter = nil);
begin
  inherited Create;
  fPath := aPath;
  fFilter := aFilter;
  fRecursive := aRecursive;
end;

procedure TORCADirectoryReader.SetRecursiveReads(aRecursive: Boolean);
begin
	fRecursive := aRecursive;
end;

procedure TORCADirectoryReader.SetFilter( aFilter: IORCADirectoryFilter );
begin
  fFilter := aFilter;
end;

procedure TORCADirectoryReader.SetPath( aPath: String );
begin
  fPath := aPath;
end;

procedure TORCADirectoryReader.ReadDirectory(aFileList: IInterfaceList);
var v_Sr:TSearchRec;
		v_FileInfoIntf: IORCAFileInfo;
    v_DirIntf: IORCADirectory;
    v_Mask: String;
    v_aAttrs: integer;
begin

	if (fFilter = nil) then
  begin
    v_Mask := '*.*';
    v_aAttrs := faAnyFile;
  end
  else
  begin
    v_Mask := fFilter.Mask;
    v_aAttrs := fFilter.Attributes;
  end;

  if FindFirst(IncludeTrailingPathDelimiter(fPath) + v_Mask, v_aAttrs, v_Sr) = 0 then
  begin
    repeat
      if (v_Sr.Attr and faDirectory) <> 0 then
      begin
        if (v_Sr.Name <> '.') and (v_Sr.Name <> '..') then
        begin
          if (fRecursive) then
          begin
 						v_DirIntf := TORCADirectory.Create( IncludeTrailingPathDelimiter(fPath) + v_Sr.name );
            v_DirIntf.ReadDirectory;
            v_DirIntf.SetFileInfo( v_Sr );
            aFileList.Add(v_DirIntf);
          end;
        end;
      end
      else
      if (v_Sr.Attr and v_aAttrs) = v_Sr.Attr then
      begin
				v_FileInfoIntf := TORCAFileInfo.Create();
        v_FileInfoIntf.SetFileInfo( IncludeTrailingPathDelimiter(fPath)+v_sr.Name );
        v_FileInfoIntf.SetFileInfo( v_Sr );
        aFileList.Add(v_FileInfoIntf);
      end;
    until FindNext(v_Sr) <> 0;
    FindClose(v_Sr);
  end;
end;


end.

