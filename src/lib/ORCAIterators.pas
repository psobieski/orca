unit ORCAIterators;

interface

uses
  Classes, ORCAIteratorIntf, ORCABaseObject, Variants;

type

	TORCAEnumeratorAbstract = class( TORCABaseObject, IORCAEnumerable)
  public
    procedure ForEach(aFunction: TEnumeratorCallBack); virtual; abstract;
  end;

  TORCAIteratorAbstract = class( TORCAEnumeratorAbstract, IORCAIterator )
	public
    procedure Reset; virtual; abstract;
    function Next: Boolean; virtual; abstract;
    function Current: IInterface; virtual; abstract;
  end;

	TORCAStringIterator = class( TORCAIteratorAbstract, IORCAEnumerable, IORCAIterator )
  private
    fValue: String;
    fPosition: Integer;
    function IsValidPosition: Boolean;
  public
    Constructor Create( aValue: string );
    procedure Reset; override;
    function Next: Boolean; override;
    function Current: IInterface; override;
    procedure ForEach(aFunction: TEnumeratorCallBack); override;
  end;

  TORCAArrayIterator = class(TORCAIteratorAbstract)
  private
    fArray: Variant;
    fIteratorPosition: Integer;
    fLowBound,
    fHighBound: Integer;
    function IsValidIndex(aIndex: Integer): Boolean;
	public
    constructor Create(aValues: array of Variant);
    procedure Reset; override;
    function Next: Boolean; override;
    function Current: IInterface; override;
    procedure ForEach(aFunction: TEnumeratorCallBack); override;
  end;

function Iterator(aValue: String): IORCAIterator; overload;
function Iterator(aValues: array of Variant): IORCAIterator; overload;
function Enumerator(aValues: array of Variant): IORCAEnumerable; overload;

implementation

uses
  SysUtils, ORCAString, ORCAInteger, ORCAFloat, ORCAChar,
  ORCASimpleDataTypesIntf;


function Iterator(aValue: String): IORCAIterator;
begin
  result := TORCAStringIterator.Create( aValue ) as IORCAIterator;
end;

function Iterator(aValues: array of Variant ): IORCAIterator;
begin
	Result := TORCAArrayIterator.Create(aValues) as IORCAIterator;
end;

function Enumerator(aValues: array of Variant): IORCAEnumerable;
begin
	Result := TORCAArrayIterator.Create(aValues) as IORCAEnumerable;
end;

{ TORCAArrayIterator }

constructor TORCAArrayIterator.Create(aValues: array of Variant);
begin
  inherited Create;
  fArray := VarArrayOf( aValues );
  fLowBound := VarArrayLowBound(fArray,1);
  fHighBound := VarArrayHighBound( fArray, 1);
  fIteratorPosition := -1;
end;

procedure TORCAArrayIterator.Reset;
begin
	fIteratorPosition := -1;
end;

function TORCAArrayIterator.Next: Boolean;
begin
  Result := IsValidIndex( fIteratorPosition +1 );
  if (Result) then
    Inc(fIteratorPosition);
end;

function TORCAArrayIterator.Current: IInterface;
var v_CurrentValue: Variant;
begin
  Result := nil;
  v_CurrentValue := fArray[fIteratorPosition];
  case VarType(v_CurrentValue) of
    varOleStr,
  	varString: Result := TORCAString.Create( v_CurrentValue );
    varSmallint,
    varInteger,
    varSingle,
    varByte,
    varWord,
    varShortInt: Result := TORCAInteger.Create( v_CurrentValue );
    varCurrency,
    varDouble: Result := TORCAFloat.Create( v_CurrentValue );
  end
end;


procedure TORCAArrayIterator.ForEach(aFunction: TEnumeratorCallBack);
var i: Integer;
begin
	for i:= fLowBound to fHighBound do
  begin
    fIteratorPosition := i;
    if (Assigned(aFunction)) then
      aFunction( Current );
  end;
end;

function TORCAArrayIterator.IsValidIndex(aIndex: Integer): Boolean;
begin
  Result := (aIndex >= fLowBound) and (aIndex <= fHighBound);
end;

{ TORCAArrayIterator }


{TORCAStringIterator}

Constructor TORCAStringIterator.Create( aValue: string );
begin
	fValue:= aValue;
  fPosition := 0;
end;

procedure TORCAStringIterator.Reset;
begin
	fPosition := 0;
end;

function TORCAStringIterator.Next: Boolean;
begin
  Inc(fPosition);
	Result := IsValidPosition;
end;

function TORCAStringIterator.Current: IInterface;
begin
	Result := nil;
  if (IsValidPosition) then
    Result := TORCAChar.Create( fValue[ fPosition ]) as IORCAChar;
end;

procedure TORCAStringIterator.ForEach(aFunction: TEnumeratorCallBack);
begin
	Reset();
  while Next() do
  begin
    if (Assigned( aFunction )) then
      aFunction( Current() );
  end;
end;

function TORCAStringIterator.IsValidPosition: Boolean;
begin
  Result := (fPosition <= Length( fValue )) and (fValue <> '');
end;
{TORCAStringIterator}

end.
