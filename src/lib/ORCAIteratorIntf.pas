unit ORCAIteratorIntf;

interface

uses
  SysUtils, Classes, ORCABaseObjectIntf;

const
  S_MSG_INTF_CAST_ERRROR = 'An object of type %s doesn''t support Interface %s';

type

	EORCAIntfCastError = class( EIntfCastError );

  TEnumeratorCallBack = procedure ( aCurrent: IInterface ) of object;

  IORCAEnumerable = interface(IORCABaseObject)
  	['{408082AE-74F2-4F25-B7C0-59B43ED6AE1B}']
  	procedure ForEach(aFunction: TEnumeratorCallBack);
  end;

  IORCAIterator = interface (IORCAEnumerable)
  	['{F1242919-89BB-4A4F-886C-FC982DFCAB24}']
    procedure Reset;
    function Next: Boolean;
    function Current: IInterface;
  end;

  function Iterator( aObject: TObject ): IORCAIterator;
  function Enumerator( aObject: TObject ): IORCAEnumerable;

implementation

function Iterator( aObject: TObject ): IORCAIterator;
var v_OutIterator: IORCAIterator;
begin
  result := nil;
  if (Supports(aObject, IORCAIterator, v_OutIterator)) then
    result := v_OutIterator
  else
    raise EORCAIntfCastError.CreateFmt(S_MSG_INTF_CAST_ERRROR,[aObject.ClassName, 'IORCAIterator' ]);
end;

function Enumerator( aObject: TObject ): IORCAEnumerable;
var v_OutIterator: IORCAEnumerable;
begin
  result := nil;
  if (Supports(aObject, IORCAEnumerable, v_OutIterator)) then
    result := v_OutIterator
  else
    raise EORCAIntfCastError.CreateFmt(S_MSG_INTF_CAST_ERRROR,[aObject.ClassName, 'IORCAEnumerable' ]);
end;

end.
