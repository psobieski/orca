unit ORCAStringUtils;

interface

uses
  SysUtils, Classes, ORCABaseObject, ORCAStringBuilderIntf, ORCAIteratorIntf, ORCASimpleDataTypesIntf;

type
  TORCAStringBuilder = class( TORCABaseObject, IORCAStringBuilder )
  private
    fValue: String;
  public
    public constructor Create(aValue: String = '');
		function Append( aValue: String ): IORCAStringBuilder;
    function Insert( const aPosition: Integer; aValue: String ): IORCAStringBuilder;
    function Prepend( aValue: String ): IORCAStringBuilder;
    function AppendFormat( aFormat: String; const aArgs: array of const ): IORCAStringBuilder; overload;
    function AppendFormat( aFormat: String; const aArgs: array of const; const aFormatSettings: TFormatSettings ): IORCAStringBuilder; overload;
    function Delete( const aPos, aLength: Integer ): IORCAStringBuilder;
    function Copy( const aPos, aLength: Integer): IORCAStringBuilder;
    function Clear: IORCAStringBuilder;
    function Replace( const aOcurrence, aReplaceWith: String; aReplaceFlags: TReplaceFlags ): IORCAStringBuilder;
    function AppendLine: IORCAStringBuilder;
    function Iterator: IORCAIterator;
    function ToString: String; override;
  end;

  function NewString(const aValue: Variant): IORCAString;
  function StringBuilder(const aValue: Variant): IORCAStringBuilder;

implementation

uses
  Variants, ORCAString, ORCAIterators;

function NewString(const aValue: Variant): IORCAString;
var v_TmpString: string;
begin
	v_TmpString := VarAsType(aValue, varString);
  result := TORCAString.Create(v_TmpString);
end;

function StringBuilder(const aValue: Variant): IORCAStringBuilder;
var v_TmpString: string;
begin
	v_TmpString := VarAsType( aValue, varString);
  result := TORCAStringBuilder.Create( v_TmpString);
end;


constructor TORCAStringBuilder.Create(aValue: String = '');
begin
	inherited Create;
  fValue := aValue;
end;

function TORCAStringBuilder.Append( aValue: String ): IORCAStringBuilder;
begin
  fValue := fValue + aValue;
  Result := Self;
end;

function TORCAStringBuilder.Insert( const aPosition: Integer; aValue: String ): IORCAStringBuilder;
begin
  System.Insert( aValue, fValue , aPosition );
  Result := Self;
end;

function TORCAStringBuilder.Prepend( aValue: String ): IORCAStringBuilder;
begin
  Insert( 1, aValue );
  Result := Self;
end;

function TORCAStringBuilder.AppendFormat( aFormat: String;  const aArgs: array of const ): IORCAStringBuilder;
begin
	fValue := fValue + SysUtils.Format( aFormat, aArgs );
  Result := Self;
end;

function TORCAStringBuilder.AppendFormat( aFormat: String; const aArgs: array of const; const aFormatSettings: TFormatSettings ): IORCAStringBuilder;
begin
  fValue := Fvalue + SysUtils.Format( aFormat, aArgs, aFormatSettings );
  Result := Self;
end;

function TORCAStringBuilder.Delete( const aPos, aLength: Integer ): IORCAStringBuilder;
begin
  System.Delete( fValue, aPos, aLength );
  Result := Self;
end;

function TORCAStringBuilder.Copy( const aPos, aLength: Integer ): IORCAStringBuilder;
begin
	Result := TORCAStringBuilder.Create( System.Copy( fValue, aPos, aLength ));
end;

function TORCAStringBuilder.Clear: IORCAStringBuilder;
begin
  fValue := '';
  Result := Self;
end;

function TORCAStringBuilder.Replace( const aOcurrence, aReplaceWith: String; aReplaceFlags: TReplaceFlags ): IORCAStringBuilder;
begin
  fValue := StringReplace( fValue, aOcurrence, aReplaceWith, aReplaceFlags );
  Result := Self;
end;

function TORCAStringBuilder.AppendLine: IORCAStringBuilder;
begin
  fValue := fValue+#13#10;
  Result := Self;
end;

function TORCAStringBuilder.Iterator: IORCAIterator;
begin
  Result := TORCAStringIterator.Create( fValue) as IORCAIterator;
end;

function TORCAStringBuilder.ToString: String;
begin
  Result := fValue;
end;
end.
