unit ORCABaseObjectIntf;

interface

uses
  Classes;

type

  IORCABaseObject = interface ( IInterface )
  	['{A54713B2-E011-4F73-ADFA-858AC9ADC92E}']
		procedure Assign( aSource: TObject );  stdcall;
    function Equals( aSource: TObject): Boolean; stdcall;
    function AsObject: TObject; stdcall;
    function ToString: String; stdcall;
    function UnitName: String; stdcall;
    function UnitScope: String; stdcall;
    function QualifiedClassName: String; stdcall;
    function GetHashCode: Integer; stdcall;
  end;
  
implementation

end.
