unit ORCAFloat;

interface

uses
  Classes, ORCASimpleDataTypesIntf, ORCABaseObject;

type
  TORCAFloat = class( TORCABaseObject, IORCAFloat )
  private
    fValue: Extended;
    function GetValue: Extended;
    procedure SetValue( aValue: Extended );
  public
    constructor Create( aValue: Extended = 0); virtual;
    function Add( const aValue: Extended ): IORCAFloat;
    function Subtract( const aValue: Extended ): IORCAFloat;
    function Multiply( const aValue: Extended ): IORCAFloat;
    function Divide( const aValue: Extended ): IORCAFloat;
    function Trunc(): IORCAInteger;
    function Round(): IORCAInteger;
    function Frac(): IORCAFloat;
    property Value: Extended read GetValue write SetValue;
  end;

  function Float(const aValue: Variant): IORCAFloat;

implementation

uses
	ORCAInteger, Variants;

constructor TORCAFloat.Create( aValue: Extended);
begin
  inherited Create;
  fValue := aValue;
end;

function TORCAFloat.GetValue: Extended;
begin
  result := FValue;
end;

procedure TORCAFloat.SetValue( aValue: Extended );
begin
  if fvalue <> aValue then
  begin
    fValue := aValue;
  end;

end;

function TORCAFloat.Add( const aValue: Extended ): IORCAFloat;
begin
	result := TORCAFloat.Create( fvalue + aValue);
end;

function TORCAFloat.Subtract( const aValue: Extended ): IORCAFloat;
begin
  Result := TORCAFloat.Create( fValue - aValue) ;
end;

function TORCAFloat.Multiply( const aValue: Extended ): IORCAFloat;
begin
  Result := TORCAFloat.Create( fValue * aValue);
end;

function TORCAFloat.Divide( const aValue: Extended ): IORCAFloat;
begin
  result := TORCAFloat.Create( fvalue / aValue);
end;

function TORCAFloat.Trunc(): IORCAInteger;
begin
  result := TORCAInteger.Create( System.Trunc( fValue ));
end;

function TORCAFloat.Round(): IORCAInteger;
begin
  result := TORCAInteger.Create( system.Round(fValue));
end;

function TORCAFloat.Frac(): IORCAFloat;
begin
  result := TORCAFloat.Create( System.Frac( fValue ) );
end;

function Float(const aValue: Variant): IORCAFloat;
var v_Value: Extended;
begin
 v_Value := VarAsType( aValue, varDouble);
 Result := TORCAFloat.Create( v_Value );
end;

end.
