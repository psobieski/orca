unit ORCABaseObject;

interface

uses
  SysUtils, Classes,  ORCABaseObjectIntf;

type

  TORCABaseObjectClass = class of TORCABaseObject;

  TORCABaseObject = class( TInterfacedObject, IORCABaseObject )
  public
    constructor Create; overload; virtual;
    constructor Create(aObject: TObject); overload; virtual;
    function AsObject: TObject; virtual; stdcall;
    procedure Assign(aSource: TObject); virtual; stdcall;
    function Clone(aCloneClass: TORCABaseObjectClass = nil ): TORCABaseObject; virtual;
    function Equals(aSource: TObject): Boolean; virtual; stdcall;
    function QualifiedClassName: String; virtual; stdcall;
    function ToString: String; virtual; stdcall;
    function UnitName: String; virtual; stdcall;
    function UnitScope: String; virtual; stdcall;
    function GetHashCode: Integer; virtual; stdcall;
  end;

implementation

uses ORCAExceptions;

constructor TORCABaseObject.Create;
begin
  inherited Create;
end;

constructor TORCABaseObject.Create(aObject: TObject);
begin
  inherited Create;
  Assign( aObject );
end;

function TORCABaseObject.AsObject: TObject; stdcall;
begin
  Result := Self;
end;

procedure TORCABaseObject.Assign( aSource: TObject ); stdcall;
begin
 if ((not Assigned(aSource)) or (Self = aSource)) then
   raise EORCAObjectAssignError.Create(self, aSource);
end;

function TORCABaseObject.Clone( aCloneClass: TORCABaseObjectClass = nil): TORCABaseObject;
begin
  if (Assigned(aCloneClass)) then
    Result := aCloneClass.Create
  else
  begin
    Result := TORCABaseObjectClass( Self.ClassType ).Create;
  end;
  Result.Assign( Self );
end;

function TORCABaseObject.Equals( aSource: TObject ): Boolean; stdcall;
begin
  Result := (Self = aSource);
end;

function TORCABaseObject.QualifiedClassName: String; stdcall;
begin
  Result := UnitName()+'.'+ClassName;
end;

function TORCABaseObject.ToString: String; stdcall;
begin
  Result := Self.ClassName;
end;

function TORCABaseObject.UnitName: String; stdcall;
begin
  Result := 'ORCABaseObject';
end;

function TORCABaseObject.UnitScope: String; stdcall;
begin
  Result := 'public';
end;

function TORCABaseObject.GetHashCode: Integer; stdcall;
begin
  Result := Integer( Self );
end;

end.
