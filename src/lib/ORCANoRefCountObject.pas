unit ORCANoRefCountObject;

interface

uses
  Classes, ORCABaseObject;

type
	TORCANoRefCountObject = class (TORCABaseObject)
  protected
		function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

implementation

uses
	SysUtils;

function TORCANoRefCountObject.QueryInterface(const IID: TGUID; out Obj): HResult;
const
  E_NOINTERFACE = HResult($80004002);
begin
	if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TORCANoRefCountObject._AddRef: Integer;
begin
  Result := -1;
end;

function TORCANoRefCountObject._Release: Integer;
begin
	Result := -1;
end;

end.
