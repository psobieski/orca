unit ORCASimpleDataTypesIntf;

interface

uses
  SysUtils, Classes, ORCAIteratorIntf, ORCABaseObjectIntf;

type
  TORCARegExpModifier = (remCaseLess, remGreedy, remSingleLine, remMultiLine, remExtended);
  TORCARegExpModifiers = set of TORCARegExpModifier;

  IORCAString = interface ( IORCABaseObject )
  	['{5A27C138-7280-41CF-A8D0-36E26A3FC366}']
    function GetText: String;
    function GetChar( Index: Integer ): Char;
    function TrimLeft: IORCAString;
    function TrimRight: IORCAString;
		function Trim: IORCAString;
    function Contains( aSubString: String ): Boolean;
    function Pos( aSubstring: String ): Integer;
    function Match( const aRegExp: String; aModifiers: TORCARegExpModifiers = [] ): Boolean;
    function RegExReplace( const aRegExp: String; aSubstitution: string; aModifiers: TORCARegExpModifiers ): String;
    function SubString(const aIndex, aCount: Integer): IORCAString;
    function UpperCase: IORCAString;
    function LowerCase: IORCAString;
    function FormatFloat( const aFormat: String; const aValue: Double): IORCAString; overload;
    function FormatFloat( const aFormat: IORCAString; const aValue: Double): IORCAString; overload;
    function Compare(const aSource: string): Integer; overload;
    function Compare(const aSource: IORCAString): Integer; overload;
    function StringOfChar( const aChar: Char; const aCount: Integer): IORCAString;
    function Iterator: IORCAIterator;
  	property Text: String read GetText;
    property Chars[ index: Integer ]: Char read GetChar; default;
  end;

  IORCAInteger = interface( IORCABaseObject )
  	['{C63A5332-7F3C-42CE-85F9-8B4FEA4059DE}']
    function GetValue: Integer;
    procedure SetValue( aValue: Integer );
    function Add( const aValue: Integer ): IORCAInteger;
    function Subtract( const aValue: Integer ): IORCAInteger;
    function Multiply( const aValue: Integer ): IORCAInteger;
    function Divide( const aValue: Integer ): IORCAInteger;
    function Modulo( const aValue: Integer ): IORCAInteger;
    property Value: Integer read GetValue;
  end;

  IORCAFloat = interface( IORCABaseObject )
  	['{2910F4F0-7543-4277-B61A-30213F46A336}']
    function GetValue: Extended;
    procedure SetValue( aValue: Extended );
    function Add( const aValue: Extended ): IORCAFloat;
    function Subtract( const aValue: Extended ): IORCAFloat;
    function Multiply( const aValue: Extended ): IORCAFloat;
    function Divide( const aValue: Extended ): IORCAFloat;
    function Trunc(): IORCAInteger;
    function Round(): IORCAInteger;
    function Frac(): IORCAFloat;
    property Value: Extended read GetValue;
  end;

  IORCADate = interface(IORCABaseObject)
  	['{51CCF93A-5F6C-447F-A35F-ECD0F49E4372}']
    function GetDate: TDateTime;
    procedure SetDate( value: TDateTime );
    procedure DecodeDate( var year, month, day: Word);
    function EncodeDate( year, month, day: Word ): IORCADate;
    function DayOfWeek: IORCAInteger;
    function DayOfMonth: IORCAInteger;
    function DayOfYear: IORCAInteger;
    function WeekOfYear: IORCAInteger;
    function WeekOfMonth: IORCAInteger;
    function Year: IORCAInteger; overload;
    function Day: IORCAInteger; overload;
    function Month: IORCAInteger; overload;
    function IsLeapYear: Boolean;
    function AddYear(const aValue: Word): IORCADate;
    function AddMonth(const aValue: Word): IORCADate;
    function AddDay(const aValue: Word): IORCADate;
    function MonthLength: IORCAInteger;
    function IsBetween(const aStartDate, aEndDate: TDateTime): Boolean; overload;
    function IsBetween(aStartDate, aEndDate: IORCADate): Boolean; overload;
    property Value: TDateTime read GetDate;
  end;

  IORCATime = interface(IORCABaseObject)
  	['{42444EC5-0EFE-4F5D-ADA4-DCDE2DDD98CE}']
    function GetTime: TDateTime;
    property Value: TDateTime read GetTime;
    function TimeToStr: IORCAString;
    function StrToTime: IORCATime;
    function Hour: IORCAInteger;
    function Minuts: IORCAInteger;
    function Seconds: IORCAInteger;
    function EncodeTime(aHour, aMin, aSec, aMSec: Word): IORCATime;
    procedure DecodeTime(var aHour, aMin, aSec, aMSec: Word); overload;
    procedure DecodeTime(out aTime: IORCATime); overload; 
    function AddHours( const aHours: Word): IORCATime;
    function AddMinuts( const aMinuts: Word): IORCATime;
    function AddSeconds( const aSeconds: Word): IORCATime;
  end;

  IORCAChar = interface( IORCABaseObject )
  	['{866C316A-78CF-4F56-AE4D-27DC35C4DC7C}']
    function GetValue: Char;
    property Value: Char read GetValue;
  end;

implementation

end.
