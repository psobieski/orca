unit ORCADirectoryIterator;

interface

uses
  Classes, ORCAIterators, ORCAIteratorIntf, ORCABaseObject, ORCAFileSystemUtils;

type

  TORCADirectoryIterator = class( TORCAIteratorAbstract )
  private
    fFiles: IInterfaceList;
    fIteratorPosition: Integer;
    function IsValidIndex(const aIndex: Integer): Boolean;
  protected
    procedure ListFiles(const aPath, aMask: String; const aAttributes: Integer); virtual;
  public
		constructor Create(const aPath, aMask: String; const aAttributes: Integer);
		procedure Reset; override;
    function Next: Boolean; override;
    function Current: IInterface; override;
    procedure ForEach(aFunction: TEnumeratorCallBack); override;
  end;

  function DirectoryIterator( const aPath, aMask: string; aAttributes: Integer): IORCAIterator;

implementation

uses
  SysUtils;

function DirectoryIterator( const aPath, aMask: string; aAttributes: Integer): IORCAIterator;
var v_Iterator: IORCAIterator;
begin
  v_Iterator := TORCADirectoryIterator.Create( aPath, aMask, aAttributes);
  result := v_Iterator;	
end;

constructor TORCADirectoryIterator.Create(const aPath, aMask: String; const aAttributes: Integer);
begin
	inherited Create;
  fFiles:=TInterfaceList.Create();
  fIteratorPosition := -1;
  {Pretty shitty huh ? ;/ }
  ListFiles( aPath, aMask, aAttributes);
end;

procedure TORCADirectoryIterator.Reset;
begin
  fIteratorPosition := -1;
end;

function TORCADirectoryIterator.Next: Boolean;
begin
	Result := (IsValidIndex( fIteratorPosition +1));
  if (Result) then
    Inc(fIteratorPosition);
end;

function TORCADirectoryIterator.Current: IInterface;
begin
  Result := nil;
  if (IsValidIndex( fIteratorPosition)) then
    Result := IORCAFileInfo(fFiles[ fIteratorPosition ]);
end;

function TORCADirectoryIterator.IsValidIndex(const aIndex: Integer): Boolean;
begin
  Result := (fIteratorPosition >= 0) and (fIteratorPosition<= fFiles.Count-1);
end;

procedure TORCADirectoryIterator.ForEach(aFunction: TEnumeratorCallBack);
var i:Integer;
begin
	for i:=0 to fFiles.Count-1 do
  begin
    fIteratorPosition := i;
    if (Assigned(aFunction)) then
      aFunction(Current);
  end;
end;

procedure TORCADirectoryIterator.ListFiles(const aPath, aMask: String; const aAttributes: Integer);
var v_Sr:TSearchRec;
		v_FileInfoIntf: IORCAFileInfo;
begin
  if FindFirst(IncludeTrailingPathDelimiter(aPath) + aMask, aAttributes, v_Sr) = 0 then
  begin
    repeat
      if (v_Sr.Attr and aAttributes) = v_Sr.Attr then
      begin
				v_FileInfoIntf := TORCAFileInfo.Create();
        v_FileInfoIntf.SetFileInfo( IncludeTrailingPathDelimiter(aPath)+v_sr.Name );
        fFiles.Add(v_FileInfoIntf);
      end;
    until FindNext(v_Sr) <> 0;
    FindClose(v_Sr);
  end;
end;

end.
