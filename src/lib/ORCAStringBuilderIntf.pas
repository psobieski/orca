unit ORCAStringBuilderIntf;

interface

uses
  SysUtils, Classes, ORCABaseObjectIntf, ORCAIteratorIntf;

type

  IORCAStringBuilder = interface ( IORCABaseObject )
  	['{C07EE1ED-40B2-4FD9-9DF7-1E6D6474BAE8}']
    function Append(aValue: String): IORCAStringBuilder;
    function Insert(const aPosition: Integer; aValue: String): IORCAStringBuilder;
    function Prepend(aValue: String): IORCAStringBuilder;
    function AppendFormat(aFormat: String; const aArgs: array of const): IORCAStringBuilder; overload;
    function AppendFormat(aFormat: String; const aArgs: array of const; const aFormatSettings: TFormatSettings): IORCAStringBuilder; overload;
    function Delete(const aPos, aLength: Integer): IORCAStringBuilder;
    function Copy(const aPos, aLength: Integer): IORCAStringBuilder;
    function Clear: IORCAStringBuilder;
    function Replace(const aOcurrence, aReplaceWith: String; aReplaceFlags: TReplaceFlags): IORCAStringBuilder;
    function AppendLine: IORCAStringBuilder;
    function Iterator: IORCAIterator;
  end;

implementation

end.
