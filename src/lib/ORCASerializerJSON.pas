unit ORCASerializerJSON;

interface

uses
  Classes, ORCASerializer;

type
  TORCAJSONSerializer = class( TORCASerializerAbstract )
  public
    function Serialize: String; override;
    procedure DeSerialize( aData: String ); override;
  end;

implementation

uses
  TypInfo;

function TORCAJSONSerializer.Serialize: String;
var
	i: integer;
  lPropInfo: PPropInfo;
  lPropCount: integer;
  lPropList: PPropList;
  lPropType: PPTypeInfo;
  lMethodCount: Integer; 
  propValue: String;
  oSerializer: TORCAJSONSerializer;
  SubObject: TObject;
begin
  StringBuilder.AppendFormat('{ "%s": ',[fObject.ClassName]);
  lMethodCount := GetPropList(PTypeInfo(fObject.ClassInfo),tkMethods, lPropList);
  lPropCount := GetPropList(PTypeInfo(fObject.ClassInfo), lPropList);

  if ((lPropCount - lMethodCount)-1) >= 0 then
  begin
    StringBuilder.Append('{');
    StringBuilder.Append(#13#10);
  end;

  for i := 0 to lPropCount - 1 do
  begin
    lPropInfo := lPropList^[i];
    lPropType := lPropInfo^.PropType;
    if lPropType^.Kind = tkMethod then
    Continue;
    if (lPropType^.Kind in [tkClass, tkInterface]) then
    begin
      SubObject := GetObjectProp( fObject, lPropInfo.Name);
      if (Assigned(SubObject)) then
      begin
	      oSerializer := TORCAJSONSerializer.Create(  SubObject );
  	    StringBuilder.Append( #9+oSerializer.Serialize ());
      end else
        StringBuilder.AppendFormat(#9'"%s": "%s"',[lPropInfo.Name, 'nil']);
    end else begin
      propValue := GetPropValue( fObject, lPropInfo.Name, True);
      StringBuilder.AppendFormat(#9'"%s": "%s"',[lPropInfo.Name, propvalue]);
    end;

    if (i <> (lPropCount - lMethodCount)-1) then
    begin
      StringBuilder.Append(',');
    end;
    StringBuilder.Append(#13#10);
  end;

  if ((lPropCount - lMethodCount)-1) >= 0 then
    StringBuilder.Append('}');

  StringBuilder.Append('}');
  result := StringBuilder.ToString();
end;

procedure TORCAJSONSerializer.DeSerialize;
begin

end;
end.
