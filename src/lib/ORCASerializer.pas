unit ORCASerializer;

interface

uses
  SysUtils, Classes, ORCASerializableIntf, ORCAStringBuilderIntf, ORCABaseObject,
  ORCAExceptions;

type

  ESerializerError = class(EORCABaseException);

	TORCASerializerAbstract = class( TORCABaseObject, IORCASerializable )
  private
    fStringBuilder: IORCAStringBuilder;
  protected
    fObject: TObject;
    function GetText: string; virtual; stdcall;
    property StringBuilder: IORCAStringBuilder read fStringBuilder;
  public
    constructor Create( aObjectToSerialize: TObject ); override;
    function Serialize: String; virtual; stdcall; abstract;
    procedure DeSerialize( aData: String ); virtual; stdcall; abstract;
    property Text: String read GetText ;
  end;

implementation

uses ORCAString, ORCAStringUtils;

const
  ERR_MSG_NO_OBJECT = 'Nie mog� wykona� (de)/serializacji na pustym obiekcie';

constructor TORCASerializerAbstract.Create( aObjectToSerialize: TObject );
begin
  if (not Assigned( aObjectToSerialize )) then
     raise ESerializerError.Create(ERR_MSG_NO_OBJECT);

  inherited Create;
  fObject := aObjectToSerialize;
  fStringBuilder := TORCAStringBuilder.Create('');
end;

function TORCASerializerAbstract.GetText: string; stdcall;
begin
  result := fStringBuilder.ToString();
end;

end.
