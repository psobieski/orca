unit ORCAChar;

interface

uses
  Classes, ORCASimpleDataTypesIntf, ORCABaseObject;

type
  TORCAChar = class( TORCABaseObject, IORCAChar )
  private
    fValue: Char;
  protected
    function GetValue: Char; virtual;
  public
    constructor Create( aValue: Char ); virtual;
    property Value: Char read GetValue;
  end;

implementation


constructor TORCAChar.Create( aValue: Char );
begin
  inherited Create;
  fValue:= aValue; 
end;

function TORCAChar.GetValue: Char;
begin
  result := fValue;
end;


end.
