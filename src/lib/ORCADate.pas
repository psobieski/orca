unit ORCADate;

interface

uses
  Classes, ORCASimpleDataTypesIntf, ORCABaseObject;

type
  TORCADate = class ( TORCABaseObject, IORCADate)
  private
    fDate: TDateTime;
		function GetDate: TDateTime;
    procedure SetDate( value: TDateTime );
  public
    constructor Create( aDate: TDateTime = 0); virtual;
    procedure DecodeDate( var Year, Month, Day: Word);
    function EncodeDate( Year, Month, Day: Word ): IORCADate;
    function DayOfWeek: IORCAInteger;
    function DayOfMonth: IORCAInteger;
    function DayOfYear: IORCAInteger;
    function WeekOfYear: IORCAInteger;
    function WeekOfMonth: IORCAInteger;
    function Year: IORCAInteger;
    function Day: IORCAInteger;
    function Month: IORCAInteger;
    function IsLeapYear: Boolean;
    function AddYear(const aValue: Word): IORCADate;
    function AddMonth(const aValue: Word): IORCADate;
    function AddDay(const aValue: Word): IORCADate;
    function MonthLength: IORCAInteger;
    function IsBetween(const aStartDate, aEndDate: TDateTime): Boolean; overload;
    function IsBetween(aStartDate, aEndDate: IORCADate): Boolean; overload;
    property Value: TDateTime read GetDate;
  end;

  function Date( const aValue: TDateTime ): IORCADate;

implementation

uses
  DateUtils, ORCAInteger, SysUtils;

constructor TORCADate.Create( aDate: TDateTime = 0);
begin
  inherited Create;
  fDate := Trunc( aDate );
end;

function TORCADate.GetDate: TDateTime;
begin
  Result := fDate;
end;

procedure TORCADate.SetDate( value: TDateTime );
begin
  if (fDate <> value ) then
    fDate := value;
end;

procedure TORCADate.DecodeDate( var Year, Month, Day: Word);
begin
  Sysutils.DecodeDate(fDate, year, month, day);
end;

function TORCADate.EncodeDate( Year, Month, Day: Word ): IORCADate;
begin
  Result := TORCADate.Create( SysUtils.EncodeDate( Year, Month, Day) );
end;

function TORCADate.DayOfWeek: IORCAInteger;
begin
  Result := TORCAInteger.Create( Sysutils.DayOfWeek( fDate ) );
end;

function TORCADate.DayOfMonth: IORCAInteger;
begin
  Result := TORCAInteger.Create( DayOfTheMonth( fDate ) );
end;

function TORCADate.DayOfYear: IORCAInteger;
begin
	Result := TORCAInteger.Create( DayOfTheYear( fDate ) );
end;

function TORCADate.WeekOfYear: IORCAInteger;
begin
  Result := TORCAInteger.Create (WeekOfTheYear( fDate) );
end;

function TORCADate.WeekOfMonth: IORCAInteger;
begin
  Result := TORCAInteger.Create( WeekOfTheMonth( fDate ) );
end;

function TORCADate.Year: IORCAInteger;
var v_Year, v_Month,v_Day: Word;
begin
	SysUtils.DecodeDate(fDate, v_Year,v_Month, v_Day);
  Result := TORCAInteger.Create( v_Year );
end;

function TORCADate.Day: IORCAInteger;
var v_Year, v_Month,v_Day: Word;
begin
	SysUtils.DecodeDate(fDate, v_Year,v_Month, v_Day);
  Result := TORCAInteger.Create( v_Month );
end;

function TORCADate.Month: IORCAInteger;
var v_Year, v_Month,v_Day: Word;
begin
	SysUtils.DecodeDate(fDate, v_Year,v_Month, v_Day);
  Result := TORCAInteger.Create( v_Day );
end;

function TORCADate.IsLeapYear: Boolean;
begin
  Result := SysUtils.IsLeapYear( Year().Value );
end;

function TORCADate.AddYear(const aValue: Word): IORCADate;
begin
  Result := TORCADate.Create( DateUtils.IncYear( fDate, aValue) );
end;

function TORCADate.AddMonth(const aValue: Word): IORCADate;
begin
  Result := TORCADate.Create ( IncMonth(fDate, aValue));
end;

function TORCADate.AddDay(const aValue: Word): IORCADate;
begin
  Result := TORCADate.Create( IncDay( fDate, aValue) );
end;

function TORCADate.MonthLength: IORCAInteger;
begin
  Result := TORCAInteger.Create( DaysInMonth( fDate ) );
end;

function TORCADate.IsBetween(const aStartDate, aEndDate: TDateTime): Boolean;
begin
  Result := (Trunc(aStartDate)<= Trunc(fDate)) and (Trunc(aEndDate) >= Trunc(fDate));
end;

function TORCADate.IsBetween(aStartDate, aEndDate: IORCADate): Boolean;
begin
  Result := IsBetween( aStartDate.Value, aEndDate.Value );
end;

function Date( const aValue: TDateTime ): IORCADate;
begin
  Result := TORCADate.Create( aValue );
end;

end.
