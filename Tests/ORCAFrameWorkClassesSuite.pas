unit ORCAFrameWorkClassesSuite;

interface

uses
  TestFramework;

type
  TORCAFrameworkClassesSuite = class( TObject )
  public
    class function GetAllTests(): ITestSuite;
  end;

implementation

uses UnitTestORCABaseObject, UnitTestORCAStringBuilder,
  UnitTestORCAStringIterator, UnitTestORCAJSONSerializer,
  UnitTestORCAArrayIterator;

class function TORCAFrameworkClassesSuite.GetAllTests(): ITestSuite;
begin
  Result := TTestSuite.Create('Classes',[
				TTestORCABaseObject.Suite(),
        TTestORCAStringBuilder.Suite(),
        TTesTORCAStringIterator.Suite(),
        TTestORCAArrayIterator.Suite(),
        TTestORCAJSONSerializer_TFORM.Suite()
  	]);
end;
end.
