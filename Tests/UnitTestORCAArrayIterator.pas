unit UnitTestORCAArrayIterator;

interface

uses
  TestFramework, ORCAIteratorIntf;

type
  TTestORCAArrayIterator = class( TTestCase )
  private
    fIterator: IORCAIterator;
  published
    procedure Test_IteracjaPoPustymZbiorze_NiePrzestawiWskaznikaPozycji;
    procedure Test_IteracjaPoZbiorzeStringow_ZwrociIORCAStringDlaKazdegoZElementow;
  end;

implementation

uses ORCAIterators, Variants, ORCASimpleDataTypesIntf, SysUtils;

procedure TTestORCAArrayIterator.Test_IteracjaPoPustymZbiorze_NiePrzestawiWskaznikaPozycji;
begin
	fIterator := Iterator( [] );
  while fIterator.Next do
     CheckTrue( False );
  fIterator := NIl;
end;

procedure TTestORCAArrayIterator.Test_IteracjaPoZbiorzeStringow_ZwrociIORCAStringDlaKazdegoZElementow;
begin
  fIterator := Iterator(['test1','test2','test3']);
  while fIterator.Next do
  begin
		CheckTrue( Supports( fIterator.Current, IORCAString) );
  end;
  fIterator := NIl;
end;
end.
