unit ORCAObjectTestcase;

interface

uses
  TestFramework, ORCABaseObject;

type
  TORCABaseObjectTestCase = class(TTestCase)
  private
    procedure RaiseAssignExceptionWhenAssigningSameSource;
    procedure RaiseAssignExceptionWhenAssigningNilSource;
  protected
    function GetObject: TORCABaseObject; virtual; abstract;
    property TheTestObject: TORCABaseObject read GetObject;
  published
    procedure Test_Equals_Returns_True_For_Equal_Objects; virtual;
    procedure Test_Equals_Returns_False_For_Different_Objects; virtual;
    procedure Test_Assign_Method_Assigning_Properly; virtual; abstract;
    procedure Test_Assign_With_Same_Source_Raises_An_Exception;
    procedure Test_Assign_With_Nil_Source_Raises_An_Exception;
  end;

implementation

uses SysUtils, ORCAExceptions;

procedure TORCABaseObjectTestCase.RaiseAssignExceptionWhenAssigningSameSource;
begin
  TheTestObject.Assign( TheTestObject );
end;


procedure TORCABaseObjectTestCase.RaiseAssignExceptionWhenAssigningNilSource;
begin
	TheTestObject.Assign( nil );
end;

procedure TORCABaseObjectTestCase.Test_Assign_With_Same_Source_Raises_An_Exception;
begin
  CheckException(RaiseAssignExceptionWhenAssigningSameSource , EORCAObjectAssignError);
end;

procedure TORCABaseObjectTestCase.Test_Assign_With_Nil_Source_Raises_An_Exception;
begin
  CheckException( RaiseAssignExceptionWhenAssigningNilSource , EORCAObjectAssignError);
end;

procedure TORCABaseObjectTestCase.Test_Equals_Returns_True_For_Equal_Objects;
begin
	CheckTrue( TheTestObject.Equals( TheTestObject ));
end;

procedure TORCABaseObjectTestCase.Test_Equals_Returns_False_For_Different_Objects;
var v_Clone: TORCABaseObject;
begin
	v_Clone := TheTestObject.Clone( TORCABaseObject );
  CheckFalse( TheTestObject.Equals( v_Clone ));
  FreeAndNil( v_Clone );
end;

end.
