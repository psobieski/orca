unit ORCAFrameWorkBaseSuite;

interface

uses
  TestFramework;

type
  TORCAFrameWorkSuite = class( TObject )
  public
    class function GetAllTests(): ITestSuite;
  end;

implementation

uses ORCAFrameWorkClassesSuite;

class function TORCAFrameWorkSuite.GetAllTests(): ITestSuite;
begin
  Result := TTestSuite.Create('ORCA Framework',[
   	TORCAFrameworkClassesSuite.GetAllTests()
  ]);
end;

initialization
  RegisterTest( TORCAFrameWorkSuite.GetAllTests() );
end.
