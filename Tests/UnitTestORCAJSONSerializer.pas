unit UnitTestORCAJSONSerializer;

interface

uses
  TestFramework, ORCASerializableIntf, Forms;

type
  TTestORCAJSONSerializer_TFORM = class(TTestCase)
  private
    testObject: TForm;
    fSerializer: IORCASerializable;
  protected
    procedure Setup; override;
    procedure TearDown; override;
  published
    procedure Test_Serializacja_Obiketu_TForm_Wygeneruje_Poprawny_Text;
  end;

implementation

uses
  SysUtils, Classes, ORCASerializerJSON;

procedure TTestORCAJSONSerializer_TFORM.Setup;
begin
  testObject := TForm.Create( nil );
  fSerializer := TORCAJSONSerializer.Create(testObject);
end;

procedure TTestORCAJSONSerializer_TFORM.TearDown;
begin
	fSerializer := nil;
  FreeAndNil( testObject );
end;

procedure TTestORCAJSONSerializer_TFORM.Test_Serializacja_Obiketu_TForm_Wygeneruje_Poprawny_Text;
var v_TestText: String;
    ls: TStringList;
begin
  fSerializer.Serialize;
	v_TestText := fSerializer.Text;
  CheckTrue( Pos('{ "TForm":', v_TestText) <> 0);
  ls := TStringList.Create;
  ls.Text := v_TestText;
  ls.SaveToFile('test.out.txt');
  ls.Free;
end;
end.
