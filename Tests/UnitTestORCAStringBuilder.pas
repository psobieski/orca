unit UnitTestORCAStringBuilder;

interface

uses
  TestFramework, ORCAStringBuilderIntf;

type
  TTestORCAStringBuilder = class( TTestCase )
  private
    fBuilder: IORCAStringBuilder;
  protected
    procedure Setup; override;
    procedure TearDown; override;
  published
    {
    function Copy( const aPos, aLength: Integer): IStringBuilder;
    function Replace( const aOcurrence, aReplaceWith: String; aReplaceFlags: TReplaceFlags ): IStringBuilder;
    }
    procedure Test_Wlasciwosc_Text_Zawiera_Dolaczony_Funkcja_Append_Text;
    procedure Test_Wlasciwosc_Text_Jest_Pustym_Stringiem_Po_Wywolaniu_Clear;
    procedure Test_Wlasciwosc_Text_Zawiera_Wstawiony_Funkcja_Prepend_Text;
    procedure Test_Wlasciwosc_Text_Zawiera_Tekst_Wstawiony_Funkcja_Insert_Na_Wskazanej_Pozycji;
    procedure Test_Wlasciwosc_Text_Nie_Zawiera_Tekstu_Usunietego_Funkcja_Delete_Na_Wskazanej_Pozycji;
  end;

implementation

uses
  ORCAString, ORCASimpleDataTypesIntf, ORCAStringUtils;

procedure TTestORCAStringBuilder.Setup;
begin
  fBuilder := TORCAStringBuilder.Create('');
end;

procedure TTestORCAStringBuilder.TearDown;
begin
  fBuilder := nil;
end;

procedure TTestORCAStringBuilder.Test_Wlasciwosc_Text_Zawiera_Dolaczony_Funkcja_Append_Text;
var v_TestText: String;
begin
  v_TestText := fBuilder.Append('ABCD').ToString();
  CheckTrue( Pos('ABCD', v_TestText) <> 0);
  CheckEquals('ABCD', v_TestText);
end;

procedure TTestORCAStringBuilder.Test_Wlasciwosc_Text_Jest_Pustym_Stringiem_Po_Wywolaniu_Clear;
begin
  fBuilder.Append('ABCD');
  CheckTrue( fBuilder.Clear().ToString() = '');
end;

procedure TTestORCAStringBuilder.Test_Wlasciwosc_Text_Zawiera_Wstawiony_Funkcja_Prepend_Text;
var v_TestText: string;
begin
	v_TestText := fBuilder.Prepend( 'ABCD' ).ToString();
  CheckTrue( Pos('ABCD', v_TestText) <> 0);
  CheckEquals('ABCD',v_TestText);
end;

procedure TTestORCAStringBuilder.Test_Wlasciwosc_Text_Zawiera_Tekst_Wstawiony_Funkcja_Insert_Na_Wskazanej_Pozycji;
var v_TestText: string;
begin
  v_TestText := fbuilder.Prepend('ABCD').Insert(2,'00').ToString();
  CheckTrue( Pos('00', v_TestText) <> 0);
  CheckEquals( '00', Copy(v_TestText,2,2));
end;

procedure TTestORCAStringBuilder.Test_Wlasciwosc_Text_Nie_Zawiera_Tekstu_Usunietego_Funkcja_Delete_Na_Wskazanej_Pozycji;
var v_TestText: string;
begin
  v_TestText := fBuilder.Append('ABCD').Delete(2,2).ToString();
  CheckTrue( Pos('BC', v_TestText) = 0);
end;

end.
