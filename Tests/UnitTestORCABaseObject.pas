unit UnitTestORCABaseObject;

interface

uses
  TestFramework, ORCAObjectTestcase, ORCABaseObject;

type
  TTestORCABaseObject = class( TORCABaseObjectTestCase )
  private
    fObject: TORCABaseObject;
  protected
    procedure Setup; override;
    procedure TearDown; override;
    function GetObject: TORCABaseObject; override;
  published
    procedure Test_Assign_Method_Assigning_Properly; override;
  end;

implementation

uses
  SysUtils;

procedure TTestORCABaseObject.Setup;
begin
  fObject := TORCABaseObject.Create();
end;

procedure TTestORCABaseObject.TearDown;
begin
	FreeAndNil(fObject);
end;

function TTestORCABaseObject.GetObject: TORCABaseObject;
begin
 Result := fObject;
end;

procedure TTestORCABaseObject.Test_Assign_Method_Assigning_Properly;
begin

end;


end.
