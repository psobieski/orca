unit UnitTestORCASQLBuilder;

interface

uses Classes, TestFramework, ORCASQLBuilder;

type
  TTestORCASQLBuilder = class( TTestCase )
  private
    fBuilder: TORCASelectBuilder;
  protected
    procedure Setup; override;
    procedure TearDown; override;
  published
    procedure Test_Select_ZwrociPustyLancuch_BezPodanejListyTablic;
    procedure Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWStringuLisciePolZPodanymAliasem;
    procedure Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWStringuLisciePolBezPodanegoALiasu;
    procedure Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWArrayuLisciePolZPodanymAliasem;
    procedure Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWArrayuLisciePolBezPodanegoAliasu;
    procedure Test_CombineAliasWithFields_ZwrociZformatowanyParametremAliasLancuch;
    procedure Test_CombineAliasWithFields_ZwrociDokladnieTakaSamWynikJakPodanaListaPol_GdyParametrAliasJestNiePodany;
  end;

implementation

uses SysUtils;

procedure TTestORCASQLBuilder.Setup;
begin
	fBuilder := TORCASelectBuilder.Create();
end;

procedure TTestORCASQLBuilder.TearDown;
begin
	FreeAndNil( fBuilder );
end;

procedure TTestORCASQLBuilder.Test_Select_ZwrociPustyLancuch_BezPodanejListyTablic;
begin
  CheckEquals('', fBuilder.Select('').ToString());
end;

procedure TTestORCASQLBuilder.Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWStringuLisciePolZPodanymAliasem;
begin
  CheckEquals('select test.a1,test.a2,test.a3', fBuilder.Select('a1,a2,a3','test').ToString());
end;

procedure TTestORCASQLBuilder.Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWStringuLisciePolBezPodanegoALiasu;
begin
  CheckEquals('select a1,a2,a3', fBuilder.Select('a1,a2,a3','').ToString());
end;

procedure TTestORCASQLBuilder.Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWArrayuLisciePolZPodanymAliasem;
begin
  CheckEquals('select test.a1,test.a2,test.a3', fBuilder.Select(['a1','a2','a3'],'test').ToString());
end;

procedure TTestORCASQLBuilder.Test_Select_ZwrociPoprawnieZbudowanaKlauzuleSQL_DlaPodanejWArrayuLisciePolBezPodanegoAliasu;
begin
	CheckEquals('select a1,a2,a3', fBuilder.Select(['a1','a2','a3'],'').ToString());
end;

procedure TTestORCASQLBuilder.Test_CombineAliasWithFields_ZwrociZformatowanyParametremAliasLancuch;
var v_testString: String;
begin
  v_testString := fBuilder.CombineAliasWithFields('a1,a2,a3','test');
  CheckEquals('test.a1,test.a2,test.a3', v_testString);
end;

procedure TTestORCASQLBuilder.Test_CombineAliasWithFields_ZwrociDokladnieTakaSamWynikJakPodanaListaPol_GdyParametrAliasJestNiePodany;
var v_testString: String;
begin
  v_testString := fBuilder.CombineAliasWithFields('a1,a2,a3','');
  CheckEquals('a1,a2,a3', v_testString);
end;
end.
