program UnitTestORCALibrary;

uses
  FastMM4,
  FastMM4Messages,
  SysUtils,
  TestFramework,
  GUITestRunner,
  ORCAIteratorIntf in '..\src\lib\ORCAIteratorIntf.pas',
  ORCAString in '..\src\lib\ORCAString.pas',
  UnitTestORCAStringIterator in 'UnitTestORCAStringIterator.pas',
  UnitTestORCAStringBuilder in 'UnitTestORCAStringBuilder.pas',
  ORCASimpleDataTypesIntf in '..\src\lib\ORCASimpleDataTypesIntf.pas',
  ORCAInteger in '..\src\lib\ORCAInteger.pas',
  ORCABaseObjectIntf in '..\src\lib\ORCABaseObjectIntf.pas',
  ORCASerializableIntf in '..\src\lib\ORCASerializableIntf.pas',
  ORCASerializer in '..\src\lib\ORCASerializer.pas',
  ORCASerializerJSON in '..\src\lib\ORCASerializerJSON.pas',
  UnitTestORCAJSONSerializer in 'UnitTestORCAJSONSerializer.pas',
  ORCAFloat in '..\src\lib\ORCAFloat.pas',
  ORCAObjectTestcase in 'ORCAObjectTestcase.pas',
  UnitTestORCABaseObject in 'UnitTestORCABaseObject.pas',
  ORCAFrameWorkBaseSuite in 'ORCAFrameWorkBaseSuite.pas',
  ORCABaseObject in '..\src\lib\ORCABaseObject.pas',
  ORCANoRefCountObject in '..\src\lib\ORCANoRefCountObject.pas',
  ORCADate in '..\src\lib\ORCADate.pas',
  ORCAChar in '..\src\lib\ORCAChar.pas',
  ORCAIterators in '..\src\lib\ORCAIterators.pas',
  UnitTestORCAArrayIterator in 'UnitTestORCAArrayIterator.pas',
  ORCADirectoryIterator in '..\src\lib\ORCADirectoryIterator.pas',
  ORCAFileSystemUtils in '..\src\lib\ORCAFileSystemUtils.pas',
  ORCAStringUtils in '..\src\lib\ORCAStringUtils.pas',
  ORCAExceptions in '..\src\lib\ORCAExceptions.pas',
  ORCAStringBuilderIntf in '..\src\lib\ORCAStringBuilderIntf.pas';

begin
  GUITestRunner.RunTest( TORCAFrameWorkSuite.GetAllTests() );
end.
