unit UnitTestORCAStringIterator;

interface

uses
	TestFramework, ORCAIteratorIntf;

type
  TTesTORCAStringIterator = class( TTestCase )
  private
    fIterator: IORCAIterator;
    fTestString: String;
    procedure Iterate( aCurrent: IInterface );
  published
    procedure Test_Iteracja_Po_Pustym_Lancuchu_Nie_Zmieni_Wskaznika_Iteratora;
    procedure Test_Lancuch_Zbudowany_Przez_Skladanie_Elementow_Podczas_Iteracji_Jest_Identyczny_Z_Podanym_Przy_Inicjacji;
    procedure Test_Lancuch_Zbudowany_Przez_Skladanie_Elementow_Podczas_Iteracji_ForEach_Jest_Identyczny_Z_Podanym_Przy_Inicjacji;
  end;

implementation

uses
  SysUtils, ORCAString, ORCASimpleDataTypesIntf;

const
	TEST_NOT_EMPTY_STRING = 'ABSCDEFGHIJKLMNOPQRSTUWXYZ';
  TEST_EMPTY_STING = '';

procedure TTesTORCAStringIterator.Test_Iteracja_Po_Pustym_Lancuchu_Nie_Zmieni_Wskaznika_Iteratora;
begin
	fIterator := TORCAString.Create().Iterator();
  while fIterator.Next do
    CheckTrue(False);
  fIterator := nil;
end;

procedure TTesTORCAStringIterator.Test_Lancuch_Zbudowany_Przez_Skladanie_Elementow_Podczas_Iteracji_Jest_Identyczny_Z_Podanym_Przy_Inicjacji;
var v_TestString: string;
begin
	fIterator := TORCAString.Create(TEST_NOT_EMPTY_STRING).Iterator;
  v_TestString := '';
  while fIterator.Next do
  begin
		v_TestString := v_TestString + IORCAChar( fIterator.Current() ).Value;
  end;
  CheckEquals( v_TestString, TEST_NOT_EMPTY_STRING);
  fIterator := nil;
end;

procedure TTesTORCAStringIterator.Iterate( aCurrent: IInterface );
begin
  if (Supports(aCurrent, IORCAChar)) then
    fTestString := fTestString + IORCAChar( aCurrent ).Value;
end;

procedure TTesTORCAStringIterator.Test_Lancuch_Zbudowany_Przez_Skladanie_Elementow_Podczas_Iteracji_ForEach_Jest_Identyczny_Z_Podanym_Przy_Inicjacji;
begin
  fTestString := '';
  fIterator := TORCAString.Create(TEST_NOT_EMPTY_STRING).Iterator;
  fIterator.ForEach( Iterate );
  CheckEquals( TEST_NOT_EMPTY_STRING, fTestString );
end;

initialization
  RegisterTest( TTesTORCAStringIterator.Suite );
end.
