unit UnitTestORCADbClasses;

interface

uses
  SysUtils, Classes, TestFramework, ORCADb;

type

	TTestORCADbTable = class( TTestCase )
  private
    //fORCADbTable: IORCADbTable;
  protected
    procedure Setup; override;
    procedure TearDown; override;
  published
  	procedure Test_GetField_ZwrociIstniejacePolezListy_ZawierajcePoprawneWlasciwosci;
    procedure Test_GetField_ZwrociNil_DlaNieIstniejacegoPolawLiscie;
  end;

implementation

procedure TTestORCADbTable.Setup;
begin
	//fORCADbTable := TORCADbTable.Create('TEST_TABLE','T1');
  //fORCADbTable.AddField('name')
  //						.AddField('Surname')
  //            .AddField('DOB');
end;

procedure TTestORCADbTable.TearDown;
begin
	//fORCADbTable := nil;
end;

procedure TTestORCADbTable.Test_GetField_ZwrociIstniejacePolezListy_ZawierajcePoprawneWlasciwosci;
var v_DbFieldIntf: IORCADbField;
begin
	//v_DbFieldIntf := fORCADbTable['name'];
  CheckNotNull( v_DbFieldIntf );
  //CheckEquals( 'name', v_DbFieldIntf.Name );
  //CheckEquals( 'T1', v_DbFieldIntf.Alias );
end;

procedure TTestORCADbTable.Test_GetField_ZwrociNil_DlaNieIstniejacegoPolawLiscie;
var v_DbFieldIntf: IORCADbField;
begin
	//v_DbFieldIntf := fORCADbTable['nofield'];
  CheckNull( v_DbFieldIntf );
end;

end.
