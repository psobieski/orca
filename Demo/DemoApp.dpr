program DemoApp;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ORCAFloat,
  ORCAInteger,
  ORCAString,
  ORCADate,
  ORCAIteratorIntf,
  ORCADirectoryIterator,
  ORCAFileSystemUtils;

var BaseValue,
		newValue: System.Integer;
    BaseDate,
    NewDate : TDateTime;
    it, it2: IORCAIterator;
begin
   BaseValue := 2;
   NewValue := ToInteger( BaseValue ).Add(12).Subtract(4).Value;
   Writeln( Format(' base value: %d , new value : %d',[baseValue, newValue]) );
   BaseDate := Date( Sysutils.Now() ).Value;
   NewDate  := Date ( BaseDate ).AddYear( 2 ).Value;
   Writeln( Format(' base value: %s , new value : %s',[DateToStr(BaseDate), DateToStr(NewDate)]) );
   Readln;
   //Directory Iterator
   it := DirectoryIterator('c:\','*.*',faAnyFile);
   while it.Next do
   begin
     if it.Current <> nil then
	     WriteLn( IORCAFileInfo(it.Current).FileName );
   end;
   Readln;
   Readln;
end.
