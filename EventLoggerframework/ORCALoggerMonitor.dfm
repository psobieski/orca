object FormLoggerMonitor: TFormLoggerMonitor
  Left = 824
  Top = 244
  Width = 689
  Height = 553
  Caption = 'ORCA Event Manager'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Menu = MainMenu
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 177
    Top = 24
    Height = 452
    Color = clGray
    ParentColor = False
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 476
    Width = 673
    Height = 19
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 24
    Width = 177
    Height = 452
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 4
    TabOrder = 1
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 169
      Height = 21
      Align = alTop
      AutoSize = False
      Caption = 'Komponenty'
    end
    object tvComponents: TTreeView
      Left = 4
      Top = 25
      Width = 169
      Height = 423
      Align = alClient
      BorderStyle = bsNone
      HideSelection = False
      Indent = 19
      PopupMenu = popupComponents
      ReadOnly = True
      TabOrder = 0
      OnChange = tvComponentsChange
    end
  end
  object Panel2: TPanel
    Left = 180
    Top = 24
    Width = 493
    Height = 452
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 4
    TabOrder = 2
    object Label2: TLabel
      Left = 4
      Top = 4
      Width = 485
      Height = 21
      Align = alTop
      AutoSize = False
      Caption = 'Handlery'
    end
    object Splitter2: TSplitter
      Left = 4
      Top = 193
      Width = 485
      Height = 3
      Cursor = crVSplit
      Align = alTop
      Color = clGray
      ParentColor = False
    end
    object lvHandlers: TListView
      Left = 4
      Top = 25
      Width = 485
      Height = 168
      Align = alTop
      BorderStyle = bsNone
      Checkboxes = True
      Columns = <
        item
          AutoSize = True
          Caption = 'Nazwa'
        end
        item
          AutoSize = True
          Caption = 'Nazwa klasy'
        end
        item
          AutoSize = True
          Caption = 'Obs'#322'ugiwany event'
        end
        item
          AutoSize = True
          Caption = 'Typ eventu'
        end
        item
          Caption = 'Nazwa metody'
          MinWidth = 50
          Width = 100
        end
        item
          AutoSize = True
          Caption = 'Adres metody'
        end
        item
          Alignment = taRightJustify
          AutoSize = True
          Caption = 'Ilo'#347'c wykonan'
        end>
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      PopupMenu = popupHandlers
      TabOrder = 0
      ViewStyle = vsReport
      OnSelectItem = lvHandlersSelectItem
    end
    object Panel3: TPanel
      Left = 4
      Top = 196
      Width = 485
      Height = 252
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 485
        Height = 21
        Align = alTop
        AutoSize = False
        Caption = 'Observery'
      end
      object lvObservers: TListView
        Left = 0
        Top = 21
        Width = 485
        Height = 231
        Align = alClient
        BorderStyle = bsNone
        Checkboxes = True
        Columns = <
          item
            AutoSize = True
            Caption = 'Nazwa'
          end
          item
            AutoSize = True
            Caption = 'Nazwa klasy'
          end
          item
            AutoSize = True
            Caption = 'Status'
          end>
        ReadOnly = True
        RowSelect = True
        PopupMenu = popupObservers
        TabOrder = 0
        ViewStyle = vsReport
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 673
    Height = 24
    AutoSize = True
    Caption = 'ToolBar1'
    EdgeBorders = [ebBottom]
    Flat = True
    Images = ImageList1
    TabOrder = 3
  end
  object MainMenu: TMainMenu
    Left = 56
    Top = 32
    object mnuItemFile: TMenuItem
      Caption = 'Plik'
      SubMenuImages = ImageList1
      object Wczytajschemat1: TMenuItem
        Caption = 'Wczytaj schemat'
      end
      object Zapiszschemat1: TMenuItem
        Caption = 'Zapisz schemat'
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object mnuItemClose: TMenuItem
        Caption = 'Zamknij'
        ImageIndex = 19
        ShortCut = 16465
        OnClick = mnuItemCloseClick
      end
    end
    object mnuItemComponents: TMenuItem
      Caption = 'Komponenty'
      object mnuItemDleteComponent: TMenuItem
        Caption = 'Usu'#324
        Enabled = False
        ShortCut = 24622
      end
    end
    object mnuItemhandlers: TMenuItem
      Caption = 'Handlery'
      SubMenuImages = ImageList1
      object mnuItemDeleteHandler: TMenuItem
        Caption = 'Usu'#324
        Enabled = False
        ImageIndex = 7
        ShortCut = 16430
        OnClick = RemoveSelectedHandler
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object mnuItemDisableHandler: TMenuItem
        Caption = 'Wy'#322#261'cz'
        Enabled = False
        ImageIndex = 18
        ShortCut = 16471
        OnClick = DisableSelectedHandler
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object mnuItemRunHandler: TMenuItem
        Caption = 'Uruchom'
        Enabled = False
        ImageIndex = 2
        ShortCut = 16466
        OnClick = RunSelectedHandler
      end
      object mnuItemRungHandlerSource: TMenuItem
        Caption = 'Uruchom oryginaln'#261' metod'#281
        Enabled = False
        ImageIndex = 3
        ShortCut = 24658
        OnClick = RunSelectedHandlerSourceMethod
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object mnuItemHandlerInstall: TMenuItem
        Caption = 'Odepnij'
        Enabled = False
        ImageIndex = 21
        ShortCut = 16457
        OnClick = InstallOrUnistallHandler
      end
    end
    object mnuItemOptions: TMenuItem
      Caption = 'Opcje'
      object mnuItemStayOnTop: TMenuItem
        Caption = 'Zawsze na wierzchu'
        Checked = True
        OnClick = mnuItemStayOnTopClick
      end
    end
    object mnuItemHelp: TMenuItem
      Caption = 'Pomoc'
      SubMenuImages = ImageList1
      object mnuItemAbout: TMenuItem
        Caption = 'Informacje'
        ImageIndex = 0
      end
    end
  end
  object popupHandlers: TPopupMenu
    Left = 88
    Top = 32
    object pmItemDeleteHandler: TMenuItem
      Caption = 'Usu'#324
      ShortCut = 16430
    end
    object Wyacz1: TMenuItem
      Caption = 'Wy'#322#261'cz'
    end
    object Wyczwszystkie1: TMenuItem
      Caption = 'Wy'#322#261'cz wszystkie'
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Uruchom1: TMenuItem
      Caption = 'Uruchom'
    end
    object Uruchomoryginalnmetod1: TMenuItem
      Caption = 'Uruchom oryginaln'#261' metod'#281
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Odepnij1: TMenuItem
      Caption = 'Odepnij'
    end
    object Odepnijwszystkie1: TMenuItem
      Caption = 'Odepnij wszystkie'
    end
  end
  object ImageList1: TImageList
    Left = 120
    Top = 32
  end
  object XPManifest1: TXPManifest
    Left = 176
    Top = 69
  end
  object popupObservers: TPopupMenu
    Left = 88
    Top = 64
    object Dodaj1: TMenuItem
      Caption = 'Dodaj'
    end
    object Usu1: TMenuItem
      Caption = 'Usu'#324
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Wycz1: TMenuItem
      Caption = 'Wy'#322#261'cz'
    end
    object N7: TMenuItem
      Caption = '-'
    end
  end
  object popupComponents: TPopupMenu
    Left = 88
    Top = 96
    object Dodaj2: TMenuItem
      Caption = 'Dodaj...'
    end
    object Usu2: TMenuItem
      Caption = 'Usu'#324
      ShortCut = 24622
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Disableallhandlers1: TMenuItem
      Caption = 'Wy'#322#261'cz handlery'
    end
    object Odepnijhandlery1: TMenuItem
      Caption = 'Odepnij handlery'
    end
  end
end
