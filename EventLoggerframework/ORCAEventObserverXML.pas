unit ORCAEventObserverXML;

interface

uses
  Classes,ORCAEventLoggerInterafces,ORCAEventObservers, ORCADesignPatterns, XMLDoc, XMLIntf;

type

  TORCAEventObserverXML = class( TORCAEventObserverAbstract, IFileReader, IFileWriter )
  private
	  fXML      : IXMLDocument;
    fFileName : String;
    fRootNode : IXMLNode;
    procedure CreateOrGetRootNode();
		function CreateOrGetFormNode(const NodeName: String): IXMLNode;    
  protected
		function GetStatus: String; override;
    function GetDisplayName: String; override;
    procedure ActiveChanged(); override;
    function FindParentForm( aComponent: TComponent ): TComponent; virtual;
  public
    constructor Create( aFileName: String ); reintroduce; virtual;
    destructor Destroy; override;
    procedure UpdateObserver( Data: TObject ); override;
    procedure FreeInstance; override;
    procedure LoadFromFile(const aFileName: String = '');
    procedure SaveToFile(const aFileName: String = '');
  published
  end;

implementation

uses Controls,SysUtils, FOrms;

constructor TORCAEventObserverXML.Create( aFileName: String );
begin
  inherited Create;
  fFileName := aFileName;

  fXML := NewXMLDocument();
  fXML.Options := [	doNodeAutoCreate, doNodeAutoIndent, doAttrNull,
  									doAutoPrefix, doNamespaceDecl, doAutoSave ];
	//
  fXML.ParseOptions := [ poValidateOnParse, poPreserveWhiteSpace ];

  if not FileExists( fFileName ) then
  begin
    Fxml.Active := True;
    fXML.Encoding := 'WINDOWS-1250';
    fXML.Version  := '1.0';
    CreateOrGetRootNode;
    fXML.SaveToFile( fFileName );
  end else
    fXML.LoadFromFile(fFileName);

  fxml.Active := True;
  Active := True;
end;

destructor TORCAEventObserverXML.Destroy;
begin 
  inherited;
end;

function TORCAEventObserverXML.GetStatus: String;
begin

end;

function TORCAEventObserverXML.GetDisplayName: String;
begin
  Result:= 'ORCA XML Observer';
end;

procedure TORCAEventObserverXML.ActiveChanged();
begin

end;

procedure TORCAEventObserverXML.UpdateObserver( Data: TObject );
var formNode,CompNode: IXMLNode;
    aEventSource: IORCAEventHandler;
    fRootParent: TComponent;
    CurrentCalls: Integer;
    sName: String;
begin
  if ( not Active ) then
    exit;

  if  not Supports( Data, IORCAEventHandler, aEventSource)  then exit;

  CreateOrGetRootNode();

  fRootParent := FindParentForm( aEventSource.GetEventSource() );
  if fRootParent = nil then
  begin
    formNode := fRootNode;
    CompNode := fRootNode;
  end
  else begin

    formNode := CreateOrGetFormNode( fRootParent.Name );
    sName := aEventSource.GetEventSource().Name;

    if (sName = '') then
      sName := fRootParent.Name+'_'+aEventSource.GetEventSource().Owner.Name+'_'+aEventSource.GetEventSource().ClassName;

    CompNode := formNode.ChildNodes.FindNode( sName );

    if CompNode = nil then
       CompNode := formNode.AddChild( sName );
  end;

  if CompNode.HasAttribute( aEventSource.GetEventName() ) then
     CurrentCalls:= CompNode.Attributes[ aEventSource.GetEventName() ]
  else
     CurrentCalls := 1;


  CompNode.Attributes[ aEventSource.GetEventName() ] := CurrentCalls +1;
  CompNode.Attributes[ 'event_type' ] := aEventSource.getEventType();
  CompNode.Attributes[ 'event_source' ] := aEventSource.getEventSource.Name;
  {
    co by tu jeszcze zalogowac
  }
  //fXML.SaveToFile( fFileName );
end;

function TORCAEventObserverXML.FindParentForm( aComponent: TComponent ): TComponent;
begin
  if (aComponent is TControl) then
    Result := GetParentForm( Tcontrol(Acomponent))
  else
    Result := aComponent.Owner;
end;

procedure TORCAEventObserverXML.FreeInstance;
begin
  Inherited;
end;

procedure TORCAEventObserverXML.CreateOrGetRootNode();
begin

  fRootNode := fxml.ChildNodes.FindNode('events_logs');
  if fRootNode = nil then
     fRootNode := fXML.AddChild('events_logs');

end;

function TORCAEventObserverXML.CreateOrGetFormNode( const NodeName: String ): IXMLNode;
var FormNode: IXMLNode;
begin
  formNode := fRootNode.ChildNodes.FindNode( NodeName );
  if formNode = nil then
     formNode := fRootNode.AddChild( NodeName );

  result := formNode;
end;

procedure TORCAEventObserverXML.LoadFromFile(const aFileName: String='');
var sFileName: string;
begin
  if (Assigned(Fxml)) then
  begin
    if aFileName = '' then
      sFileName := aFileName
    else
      sFileName := fFileName;

    Fxml.LoadFromFile( sFileName );
  end;
end;

procedure TORCAEventObserverXML.SaveToFile(const aFileName: String ='');
var sFileName: string;
begin
  if (Assigned(Fxml)) then
  begin
    if aFileName = '' then
      sFileName := aFileName
    else
      sFileName := fFileName;

    Fxml.SaveToFile( sFileName );
  end;
end;
end.
