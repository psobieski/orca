program UnitTestORCAELF;

uses
  FastMM4,
  FastMMMemLeakMonitor,
  SysUtils,
  TestFramework,
  TestExtensions,
  GUITestRunner,
  TextTestRunner,
  UnitTestORCAEventHandlers in 'UnitTestORCAEventHandlers.pas',
  ORCAEventLoggerFramework in '..\ORCAEventLoggerFramework.pas',
  ORCAEventLoggerInterafces in '..\ORCAEventLoggerInterafces.pas',
  ORCADesignPatterns in '..\ORCADesignPatterns.pas',
  ORCAEventHandlers in '..\ORCAEventHandlers.pas',
  ORCAEventObservers in '..\ORCAEventObservers.pas',
  ORCAEventObserverXML in '..\ORCAEventObserverXML.pas';

{$IFDEF DUNIT_CONSOLE_MODE}
  {$APPTYPE CONSOLE}
{$ELSE}
  {$R *.RES}
{$ENDIF}

begin
{$IFDEF DUNIT_CONSOLE_MODE}
  if not FindCmdLineSwitch('Graphic', ['-','/'], True) then
    TextTestRunner.RunRegisteredTests(rxbHaltOnFailures)
  else
{$ENDIF}
    GUITestRunner.RunRegisteredTests;
end.
