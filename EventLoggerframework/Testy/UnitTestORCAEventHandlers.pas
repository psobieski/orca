unit UnitTestORCAEventHandlers;

interface

uses
  Classes,TestFramework, StdCtrls, ORCAEventLoggerFramework, ORCAEventHandlers;

type
    TTestButton = class (TButton)
    public
      constructor Create( aOwner: TComponent ); override;
    published
      procedure DoButtonClick( Sender: TObject );
    end;

    TTestORCANotifyEventHandler = class( TTestCase )
    private
      fHandler    : TORCANotifyEventHandler;
      fTestButton : TTestButton;
    protected
      procedure Setup; override;
      procedure TearDown; override;
    published
      procedure TestExecuteCountIsEqualToZeroAfterFirstCallOfInstall;
      procedure TestgetClassNameReturnsHandlerClassName;
      procedure TestgetEventNameReturnsHandledEventName;
      procedure TestInstalledIsEqualToFalseAfterCallOfUninstall;
      procedure TestInstalledIsEqualToTrueAfterFirstCallOfInstall;
      procedure TestOriginalMethodAddressEqualsToOnClickAdressOfTTestButton;
      procedure TestOriginalMethodNameIsEqualToOriginalMethodName;
      procedure TestOriginalMethodNameIsNotEmptyString;
      //procedure TestAddObserver
    end;

implementation

const
  ORIGINAL_METHOD_NAME = 'DoButtonClick';
  HANDLED_EVENT_NAME   = 'OnClick';
  HANDLER_CLASS_NAME   = 'TORCANotifyEventHandler';

constructor TTestButton.Create( aOwner: TComponent );
begin
   inherited Create( aOwner );
   OnClick := DoButtonClick;
end;

procedure TTestButton.DoButtonClick( Sender: TObject );
begin
   self.Tag := Integer(True);
end;

procedure TTestORCANotifyEventHandler.Setup;
begin
  fTestButton := TTestButton.Create( nil );
  fHandler := TORCANotifyEventHandler.Create( fTestButton, HANDLED_EVENT_NAME);
end;

procedure TTestORCANotifyEventHandler.TearDown;
begin
  fHandler.Free;
  fTestButton.Free;
end;

procedure TTestORCANotifyEventHandler.TestExecuteCountIsEqualToZeroAfterFirstCallOfInstall;
begin
  fHandler.Install();
  CheckEquals( 0 , fHandler.ExecuteCount );
end;

procedure TTestORCANotifyEventHandler.TestgetClassNameReturnsHandlerClassName;
begin
  CheckTrue( fHandler.getClassName() = fHandler.ClassName );
end;

procedure TTestORCANotifyEventHandler.TestgetEventNameReturnsHandledEventName;
begin
  CheckTrue( fHandler.getEventName = HANDLED_EVENT_NAME );
end;

procedure TTestORCANotifyEventHandler.TestInstalledIsEqualToFalseAfterCallOfUninstall;
begin
  fHandler.Install();
  fHandler.Uninstall();
  CheckFalse( fHandler.Installed );
end;

procedure TTestORCANotifyEventHandler.TestInstalledIsEqualToTrueAfterFirstCallOfInstall;
begin
  fHandler.Install();
  CheckTrue( fHandler.Installed );
end;

procedure TTestORCANotifyEventHandler.TestOriginalMethodAddressEqualsToOnClickAdressOfTTestButton;
begin
  fHandler.Install();
  CheckEquals( Integer(fTestButton.MethodAddress(ORIGINAL_METHOD_NAME)), Integer( fHandler.OriginalMethodAddress ));
end;

procedure TTestORCANotifyEventHandler.TestOriginalMethodNameIsEqualToOriginalMethodName;
begin
  CheckTrue( fHandler.OriginalMethodName() = ORIGINAL_METHOD_NAME );
end;

procedure TTestORCANotifyEventHandler.TestOriginalMethodNameIsNotEmptyString;
begin
  CheckTrue( fHandler.OriginalMethodName() <> '');
end;

initialization
  RegisterTest( TTestORCANotifyEventHandler.Suite );
end.
