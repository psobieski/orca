unit ORCADesignPatterns;

interface

uses
  Classes;
  
type

  IObserver = interface
    ['{0AB250CA-E558-44C6-B13F-000A94E8CF2A}']
    procedure UpdateObserver( Data: TObject );
  end;

  IObservable = interface
    ['{C34EA9AF-B647-4D02-A9D2-33D15F919283}']
    procedure AddObserver( aObserver: IObserver );
    procedure RemoveObserver( aObserver: IObserver );
    procedure UpdateObservers( Data: TObject );
  end;

  IFileReader = interface
    ['{EF67290F-009E-41CD-A265-425955BA6DC5}']
    procedure LoadFromFile(const aFileName: String );
  end;

  IStreamReader = interface
    ['{EF67290F-009E-41CD-A265-425955BA6DC5}']
    procedure LoadFromStream( aStream: TStream );
  end;

  IFileWriter = interface
    ['{092BEAF8-579E-414B-B7A6-2B66B9680FB2}']
    procedure SaveToFile( const aFileName: String );
  end;

  IStreamWriter = interface
    ['{692DC162-A817-4807-B12F-823139F5C085}']
    procedure SaveToStream( aStream: TStream );
  end;

implementation

end.
