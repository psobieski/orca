unit ORCALoggerMonitor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ImgList, ORCAEventLoggerInterafces, ExtCtrls, StdCtrls,
  ToolWin, XPMan, ORCADesignPatterns;

type
  TFormLoggerMonitor = class(TForm)
    MainMenu: TMainMenu;
    StatusBar: TStatusBar;
    mnuItemFile: TMenuItem;
    mnuItemhandlers: TMenuItem;
    mnuItemOptions: TMenuItem;
    mnuItemHelp: TMenuItem;
    mnuItemAbout: TMenuItem;
    mnuItemClose: TMenuItem;
    popupHandlers: TPopupMenu;
    mnuItemStayOnTop: TMenuItem;
    mnuItemDeleteHandler: TMenuItem;
    N1: TMenuItem;
    mnuItemDisableHandler: TMenuItem;
    N2: TMenuItem;
    mnuItemRunHandler: TMenuItem;
    ImageList1: TImageList;
    mnuItemComponents: TMenuItem;
    mnuItemDleteComponent: TMenuItem;
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    tvComponents: TTreeView;
    ToolBar1: TToolBar;
    Label2: TLabel;
    lvHandlers: TListView;
    Splitter2: TSplitter;
    Panel3: TPanel;
    Label3: TLabel;
    lvObservers: TListView;
    XPManifest1: TXPManifest;
    mnuItemRungHandlerSource: TMenuItem;
    N3: TMenuItem;
    mnuItemHandlerInstall: TMenuItem;
    pmItemDeleteHandler: TMenuItem;
    Wyacz1: TMenuItem;
    N4: TMenuItem;
    Uruchom1: TMenuItem;
    Uruchomoryginalnmetod1: TMenuItem;
    N5: TMenuItem;
    Odepnij1: TMenuItem;
    popupObservers: TPopupMenu;
    Usu1: TMenuItem;
    N6: TMenuItem;
    Wycz1: TMenuItem;
    N7: TMenuItem;
    Dodaj1: TMenuItem;
    popupComponents: TPopupMenu;
    Usu2: TMenuItem;
    N8: TMenuItem;
    Disableallhandlers1: TMenuItem;
    Odepnijhandlery1: TMenuItem;
    Odepnijwszystkie1: TMenuItem;
    Wyczwszystkie1: TMenuItem;
    Dodaj2: TMenuItem;
    N9: TMenuItem;
    Wczytajschemat1: TMenuItem;
    Zapiszschemat1: TMenuItem;
    procedure mnuItemCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnuItemStayOnTopClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure tvComponentsChange(Sender: TObject; Node: TTreeNode);
    procedure lvHandlersSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure RunSelectedHandler(Sender: TObject);
    procedure RemoveSelectedHandler(Sender: TObject);
    procedure DisableSelectedHandler(Sender: TObject);
    procedure RunSelectedHandlerSourceMethod(Sender: TObject);
    procedure InstallOrUnistallHandler(Sender: TObject);
  private
    fManager: IORCAEventHandlerManager;
    fSelectedComponent: TComponent;
    fSelectedHandler: IORCAEventHandler;
    fSelectedObserver: IObserver;
    procedure SetMenuState;
    function AlreadyInTree( Acomponent: TComponent ): Boolean;
    procedure ListComponents();
    procedure ListHandlers( aComponent: TComponent );
    procedure ListListeners( aEventHandler: IORCAEventHandler );
  public
    constructor Create( aOwner: TComponent; aManager: IORCAEventHandlerManager); reintroduce; virtual;
    Class procedure Execute( aManager: IORCAEventHandlerManager); virtual;
    property EventsHandlersManager: IORCAEventHandlerManager read fManager;
  end;

var
  FormLoggerMonitor: TFormLoggerMonitor;

implementation

{$R *.dfm}

procedure TFormLoggerMonitor.mnuItemCloseClick(Sender: TObject);
begin
  Self.Close();
end;

Class procedure TFormLoggerMonitor.Execute( aManager: IORCAEventHandlerManager );
var Mgr: TFormLoggerMonitor;
begin
	mgr := TFormLoggerMonitor.Create( nil, aManager );
  mgr.Show;
end;

procedure TFormLoggerMonitor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  action := CaFree;
end;

constructor TFormLoggerMonitor.Create( aOwner: TComponent; aManager: IORCAEventHandlerManager);
begin
  fManager := aManager;
  inherited Create( aOwner );
end;

procedure TFormLoggerMonitor.ListComponents();
var i:Integer;
	  aHandledComponent: TComponent;
    aEventHandler: IORCAEventHandler;
    arootNode, CompNode: TTreeNode;
    sNodeCaption: String;
begin
  tvComponents.Items.Clear();
	for i:=0 to fManager.Handlers.Count -1 do
  begin
    sNodeCaption := '';
    arootNode := nil;
    if Supports( fManager.Handlers.Items[i], IORCAEventHandler, aEventHandler ) then
    begin
      aHandledComponent := aEventHandler.GetEventSource();
      if not ( AlreadyInTree( aHandledComponent ) ) then
      begin
        if aHandledComponent.GetParentComponent <> nil then
        begin
           //arootNode := FindParentComponent( aHandledComponent );
        end else
           arootNode := nil;
        sNodeCaption := Format('%s:%s',[aHandledComponent.Name, aHandledComponent.ClassName]);
        if arootNode <> nil then
           CompNode := tvComponents.Items.AddChildObject( arootNode, sNodeCaption, aHandledComponent)
        else
           CompNode := tvComponents.Items.AddObject( nil, sNodeCaption, aHandledComponent)
      end;
    end;
  end;
end;
procedure TFormLoggerMonitor.mnuItemStayOnTopClick(Sender: TObject);
begin
  TMenuItem(sender).Checked := not TMenuItem(sender).Checked;
  if TMenuItem(sender).Checked then
    Self.FormStyle := fsStayOnTop
  else
    self.FormStyle := fsNormal;
end;

procedure TFormLoggerMonitor.FormActivate(Sender: TObject);
begin
 tvComponents.Items.BeginUpdate;
  try
    ListComponents();
  finally
    tvComponents.Items.EndUpdate;
  end;
end;

procedure TFormLoggerMonitor.tvComponentsChange(Sender: TObject;
  Node: TTreeNode);
begin
  lvHandlers.Items.BeginUpdate;
  try
    ListHandlers( TComponent( Node.Data) );
  finally
    lvHandlers.Items.EndUpdate;
  end;
  SetMenuState;
end;

procedure TFormLoggerMonitor.ListHandlers( aComponent: TComponent );
var I:integer;
    aEventhandler: IORCAEventHandler;
    aEventSource: TComponent;
    item: TListItem;
begin
  fSelectedHandler := nil;
  lvHandlers.Items.clear;       
  for i:=0 to fManager.Handlers.Count -1 do
  begin
    if Supports( fManager.Handlers.Items[i], IORCAEventHandler, aEventhandler) then
    begin
      aEventSource := aEventhandler.GetEventSource();
      if aEventSource = aComponent then
      begin
         item := lvHandlers.Items.Add;
         item.Caption := aEventhandler.getDisplayName();
         item.SubItems.Add( aEventhandler.getClassName() );
         item.SubItems.Add( aEventhandler.getEventName() );
         item.SubItems.Add( aEventhandler.getEventType() );
         item.SubItems.Add( aEventhandler.OriginalMethodName() );
         item.SubItems.Add( Format('%p',[aEventhandler.OriginalMethodAddress()]));
         item.SubItems.Add( IntToStr(aEventhandler.ExecuteCount));
         item.Checked := aEventhandler.Active;
         item.Data := Pointer( aEventhandler );
      end;
    end;
  end;
end;

function TFormLoggerMonitor.AlreadyInTree( Acomponent: TComponent ): Boolean;
var i:Integer;
begin
  result := False;
  for i:=0 to tvComponents.Items.Count -1 do
  begin
    if tvComponents.Items[i].Data <> nil then
    begin
      if tvComponents.Items[i].Data = Pointer( Acomponent ) then
      begin
        result := True;
        break;
      end;
    end;
  end;
end;

procedure TFormLoggerMonitor.ListListeners( aEventHandler: IORCAEventHandler );
begin

end;

procedure TFormLoggerMonitor.lvHandlersSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);

var
	aEventHandler: IORCAEventHandler;
  i:Integer;
  aSTDObsever: IObserver;
  aitem : TListItem;
begin
  if (item.Deleting) then exit;

  lvObservers.Items.BeginUpdate;
  try
    if item <> nil then
    begin
      { TODO -oRadek : Decomposition of this method }
      if Item.Data <> nil then
      begin
        lvObservers.Items.Clear;
        aEventHandler := IORCAEventHandler( Item.Data ) ;
        for i:= 0 to aEventHandler.Observers.Count -1 do
        begin
          (*
          if Supports(aEventHandler.Observers.Items[i], IORCAObserver, aIORCAObserver ) then
          begin
            aItem := lvObservers.Items.Add;
            aitem.Caption := aIORCAObserver.GetDisplayName();
            aitem.SubItems.add( aIORCAObserver.ClassName() );
            aitem.SubItems.Add( aIORCAObserver.Status );
            aitem.data := Pointer(aIORCAObserver);
          end else *)
          if Supports(aEventHandler.Observers.Items[i], IObserver, aSTDObsever ) then
          begin
            aitem := lvObservers.Items.Add;
            aitem.Caption := 'Nieznany observer';
            aitem.SubItems.Add('Nieznana');
            aitem.SubItems.Add('Nieznany');
            aitem.Data := Pointer(aSTDObsever);
          end;
        end;
      end;
      SetMenuState;
    end;
  finally
    lvObservers.Items.EndUpdate;
  end;
end;

procedure TFormLoggerMonitor.SetMenuState;
begin
  { TODO -oRadek : Decomposition of this method }
  fSelectedComponent := nil;

  if tvComponents.Selected <> nil then
  begin
    if tvComponents.Selected.Data <> nil then
      fSelectedComponent := TComponent ( tvComponents.Selected.Data );
  end;
  mnuItemDleteComponent.Enabled := fSelectedComponent <> nil;

  fSelectedHandler := nil;
  if (lvHandlers.Selected <> nil) then
  begin
    if lvHandlers.Selected.Data <> nil then
    begin
      fSelectedHandler := IORCAEventHandler( lvHandlers.Selected.Data );
    end;
  end;

  mnuItemDeleteHandler.Enabled := Assigned( fSelectedHandler );
  mnuItemDisableHandler.Enabled := Assigned( fSelectedHandler );
  mnuItemRunHandler.Enabled := Assigned( fSelectedHandler );
  mnuItemRungHandlerSource.Enabled := Assigned( fSelectedHandler );
  mnuItemHandlerInstall.Enabled := Assigned( fSelectedHandler );

  if (mnuItemHandlerInstall.Enabled) then
  begin
    if fSelectedHandler.Installed then
    begin
      mnuItemHandlerInstall.Caption := 'Odepnij';
      mnuItemHandlerInstall.Tag := 0;
    end
    else
    begin
      mnuItemHandlerInstall.Caption := 'Podepnij';
      mnuItemHandlerInstall.Tag     := 1;
    end;
  end;

  if (mnuItemDisableHandler.Enabled)  then
  begin
    if fSelectedHandler.Active then
    begin
      mnuItemDisableHandler.Caption := 'Wy��cz';
      mnuItemDisableHandler.Tag     := 0;
    end
    else
    begin
      mnuItemDisableHandler.Caption := 'W��cz';
      mnuItemDisableHandler.Tag     := 1;
    end;
  end;
end;

procedure TFormLoggerMonitor.RunSelectedHandler(Sender: TObject);
begin
  if Assigned( fSelectedHandler ) then
    fSelectedHandler.Run();
end;

procedure TFormLoggerMonitor.RemoveSelectedHandler(Sender: TObject);
begin
  lvHandlers.Items.BeginUpdate;
  try
    if (lvHandlers.Selected <> nil) and (Assigned(fSelectedHandler)) then
    begin
      fManager.Handlers.Remove( fSelectedHandler );
      lvHandlers.items.Delete( lvHandlers.Selected.Index );
    end;
  finally
    lvHandlers.Items.EndUpdate;
  end;
end;

procedure TFormLoggerMonitor.DisableSelectedHandler(Sender: TObject);
begin
  if Assigned( fSelectedHandler ) then
  begin
    if lvHandlers.Selected <> nil then
    begin
      fSelectedHandler.Active := Boolean( TMenuItem(Sender).Tag );
      lvHandlers.Selected.Checked := fSelectedHandler.Active;
      SetMenuState;
    end;
  end;
end;

procedure TFormLoggerMonitor.RunSelectedHandlerSourceMethod(Sender: TObject);
begin
  if Assigned( fSelectedHandler ) then
  begin
    fSelectedHandler.RunSourceMethod();
  end;
end;

procedure TFormLoggerMonitor.InstallOrUnistallHandler(Sender: TObject);
begin
  if (Assigned(fSelectedHandler)) then
  begin
    case TMenuItem( Sender).Tag of
       0: fSelectedHandler.Uninstall;
       1: fSelectedHandler.Install;
    end;
    SetMenuState;
  end;
end;

end.

