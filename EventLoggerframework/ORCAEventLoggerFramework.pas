unit ORCAEventLoggerFramework;

interface

uses
  Classes, ORCAEventLoggerInterafces, ORCADesignPatterns;

type

  TORCAObjectable = class(TInterfacedObject, IORCAObjectable)
  public
    function AsObject: TObject;
  end;

  TORCACustomEventHandler = class(TORCAObjectable, IORCAEventHandler, IObservable)
  private
    fActive: Boolean;
    fEventName: String;
    fEventSource: TComponent;
    fExecuteCount: Integer;
    FExecuting: Boolean;
    FManager: IORCAEventHandlerManager;
    FMyMethod: TMethod;
    FObservers: IInterfaceList;
    FOriginalMethod: TMethod;
    FPatched: Boolean;
    function GetActive: boolean;
    function GetEventManager: IORCAEventHandlerManager;
    function GetExecuteCount: Integer;
    function getObServers   : IInterfaceList;
    function isInstalled	  : Boolean;
    procedure SetActive( Value: Boolean );
    procedure SetEventmanager( Value: IORCAEventHandlerManager );
  protected
    procedure ActiveChanged(); virtual;
    function getDisplayName: string; virtual;
    function getEventSource: TComponent;
    function GetPatchedMethodName(): String; virtual; abstract;
    procedure RunSourceMethod(); virtual; abstract;
    property OriginalMethod: TMethod Read FOriginalMethod;
  public
    constructor Create( aEventSource: TComponent; aEventName: String ); reintroduce; virtual;
    { Obsever Design Pattern -> IObservable implementation }
    procedure AddObserver( aObserver: IObserver );
    //for easy observer bindings
    procedure AddObservers( aList: IInterfaceList );
    function EventHasCode( aComponent: TComponent; aEventName: String ): Boolean;
    procedure FreeInstance; override;
    function getClassName		: string;
    function getEventName   : String;
    function getEventType   : String; virtual; abstract;
    procedure Install;
    function OriginalMethodAddress: Pointer;
    function OriginalMethodName: String;
    procedure RemoveObserver( aObserver: IObserver );
    procedure Run;
    procedure Uninstall;
    procedure UpdateObservers( Data: TObject );
    property Actvie: Boolean read GetActive write SetActive;
    property EventManager: IORCAEventHandlerManager read GetEventManager write SetEventmanager;
    property ExecuteCount: Integer read GetExecuteCount;
    property Installed: Boolean read isInstalled;
  end;

  TEventHandlerClass = class of TORCACustomEventHandler;

  TORCAEventHandlerManager = class(TORCAObjectable, IOBserver,
      IORCAEventHandlerManager, IORCAObserver)
  private
    fActive: Boolean;
    FHandlers: IInterfaceList;
    fObservers: IInterfaceList;
    fRegisteredHandlers: TStringList;
    fStatus: String;
    function GetActive: Boolean;
    function GetHandledClass( index: Integer ): TEventHandlerClass;
    function GetHandlerByEventType( aEventType: String ): TEventHandlerClass;
    function getHandlers: IInterfaceList;
    procedure SetActive(value: Boolean);
  protected
    procedure AddObserver( aObserver: IObserver ); virtual;
    procedure AddObserverToHandlers( aObserver: IObserver );
    function GetStatus: String; virtual;
  public
    constructor Create( aObservers: array of IObserver); reintroduce;
    destructor Destroy; override;
    procedure AddComponent( aComponent: TComponent );
    procedure AddObservers ( aObservers: array of IObserver); dynamic;
    function AsObject: TObject;
    function ClassName: ShortString;
    function ClassType: TClass;
    function FindComponent(aName: String): TComponent;
    function GetDisplayName: String;
    class function GetManager(aoBservers: array of IObserver): IORCAEventHandlerManager;
    procedure RegisterHandlerClass ( aEventType: String; aHandlerClass: TClass );
    procedure RemoveComponent( aName: String ); overload;
    procedure RemoveComponent( aPrentclass: TClass; aComponent: TComponent ); overload;
    procedure RemoveComponent( aPrentclass: TClass; aClass: TComponentClass ); overload;
    procedure RemoveComponent( aComponent: TComponent ); overload;
    procedure RemoveComponent( aParent: TComponent;  aComponent: TComponent ); overload;
    procedure RemoveComponent( aPrent: TComponent; aClass: TComponentClass ); overload;
    procedure RemoveComponent( aClass: TComponentClass ); overload;
    procedure RemoveEventHandler ( Data: TObject );
    procedure RemoveObserver ( aObserver: IObserver ); dynamic;
    procedure UnregisterHandlerClass ( aEventType: String ); overload;
    procedure UnregisterHandlerClass ( aHandlerClass: TClass ); overload;
    procedure UpdateObserver( Data: TObject );
    property Active: Boolean read GetActive write SetActive;
    property HandledClasses[index: Integer]: TEventHandlerClass read GetHandledClass;
    property HandlerByEventType[ aEventType: String ]: TEventHandlerClass read GetHandlerByEventType;
    property Handlers: IInterfaceList Read getHandlers;
    property Status: String read GetStatus ;
  end;

var DefaultHandlerManager: IORCAEventHandlerManager;

implementation

uses TypInfo, Math, SysUtils, Dialogs;

function TORCAObjectable.AsObject: TObject;
begin
  Result := Self;
end;

constructor TORCACustomEventHandler.Create( aEventSource: TComponent; aEventName: String );
begin
  inherited Create;
  fEventSource  := aEventSource;
  FObservers    := TInterfaceList.Create;
  fEventName    := aEventName;
  fActive       := False;
  FOriginalMethod.Code := nil;
  FOriginalMethod.Data := nil;
  Fpatched      := False;
  FExecuting    := False;
  fExecuteCount := 0;
end;

procedure TORCACustomEventHandler.ActiveChanged();
begin
  if fActive then
     Install()
  else
     Uninstall();
end;

procedure TORCACustomEventHandler.AddObserver( aObserver: IObserver );
begin
  if ( FObservers.IndexOf(aObserver) = -1) then
    FObservers.Add( aObserver );
end;

procedure TORCACustomEventHandler.AddObservers( aList: IInterfaceList );
var i: Integer;
    observer: IObserver;

begin
  for i:=0 to aList.Count -1 do
  begin
    if Supports(aList.Items[i] , IObserver, observer) then
    	self.AddObserver( observer as IObserver );
  end;
end;

function TORCACustomEventHandler.EventHasCode( aComponent: TComponent; aEventName: String ): Boolean;
var EventData: TMethod;
begin
  EventData := GetMethodProp( aComponent, aEventName );
  result := Assigned( EventData.Code );
end;

procedure TORCACustomEventHandler.FreeInstance;
begin
  Uninstall;
  FObservers := nil;
  inherited FreeInstance;
end;

function TORCACustomEventHandler.GetActive: boolean;
begin
  Result := fActive;
end;

function TORCACustomEventHandler.getClassName: string;
begin
  result := self.ClassName;
end;

function TORCACustomEventHandler.getDisplayName: string;
begin
  result := Self.ClassName;
end;

function TORCACustomEventHandler.GetEventManager: IORCAEventHandlerManager;
begin
  Result := FManager;
end;

function TORCACustomEventHandler.getEventName: String;
begin
  result := fEventName;
end;

function TORCACustomEventHandler.getEventSource: TComponent;
begin
  Result := fEventSource;
end;

function TORCACustomEventHandler.GetExecuteCount: Integer;
begin
  Result := fExecuteCount;
end;

function TORCACustomEventHandler.getObServers   : IInterfaceList;
begin
  result := FObservers;
end;

procedure TORCACustomEventHandler.Install;
begin
  if FPatched or (csDestroying in fEventSource.ComponentState) then
    exit;

  if ( Assigned( fEventSource ) ) then
  begin
    ForiginalMethod := GetMethodProp( fEventSource, fEventName );
    if Assigned( ForiginalMethod.code ) then
    begin
      FMyMethod.Code := MethodAddress( GetPatchedMethodName() );
      FMyMethod.Data := Pointer( Self );
      SetMethodProp( fEventSource , fEventName , FMyMethod );
      fPatched := True;
    end;
  end;
end;

function TORCACustomEventHandler.isInstalled	  : Boolean;
begin
  result := FPatched;
end;

function TORCACustomEventHandler.OriginalMethodAddress: Pointer;
begin
  result := FOriginalMethod.Code;
end;

function TORCACustomEventHandler.OriginalMethodName: String;
begin
  result := Self.GetPatchedMethodName();
end;

procedure TORCACustomEventHandler.RemoveObserver( aObserver: IObserver );
begin
  FObservers.Remove( aObserver );
end;

procedure TORCACustomEventHandler.Run;
begin
  if FExecuting then Exit;
  FExecuting := True;
  try
    RunSourceMethod();
    Inc(fExecuteCount);
    if fActive and (not(csdestroying in fEventSource.ComponentState)) then
     UpdateObservers( self );
  finally
    FExecuting := False;
  end;
end;

procedure TORCACustomEventHandler.SetActive( Value: Boolean );
begin
  if fActive <> value then
  begin
    fActive := Value;
    ActiveChanged();
  end;
end;

procedure TORCACustomEventHandler.SetEventmanager( Value: IORCAEventHandlerManager );
begin
  FManager := Value;
end;

procedure TORCACustomEventHandler.Uninstall;
begin
   if not FPatched then exit;
   if Assigned(fEventSource) then
   begin
     if (not (csDestroying in fEventSource.ComponentState)) then
     begin
       SetMethodProp( fEventSource , fEventName , FOriginalMethod);
       FPatched := False;
     end;
   end;
end;

procedure TORCACustomEventHandler.UpdateObservers( Data: TObject );
var i:Integer;
    observer: IObserver;
begin

  for i :=0 to FObservers.count -1 do
  begin
    if Supports(FObservers.Items[i] , IObserver, observer) then
    	IObserver(observer).UpdateObserver( Data );
  end;

end;


constructor TORCAEventHandlerManager.Create( aObservers: array of IObserver);
begin
  inherited Create;
  fActive := True;
  fStatus := 'Working';
  fRegisteredHandlers := TStringList.Create;
  FHandlers := TInterfaceList.Create;
  fObservers := TInterfaceList.Create;
  AddObservers( aObservers );
end;

destructor TORCAEventHandlerManager.Destroy;
begin
  fObservers := nil;
  FHandlers  := nil;
  fRegisteredHandlers.Free;
  Inherited;
end;

procedure TORCAEventHandlerManager.AddComponent(aComponent: TComponent);
var
  Count, Size, I: Integer;
  List: PPropList;
  PropInfo: PPropInfo;
  PropTypeName: String;
  handlerClass: TEventHandlerClass;
  handler: IORCAEventHandler;
  HandlerObject: TORCACustomEventHandler;
begin { TODO -oRadek : Dekompozycja tej metody }
  Count := GetPropList(aComponent.ClassInfo, tkMethods, nil);
  Size  := Count * SizeOf(Pointer);
  GetMem(List, Size);
  try
    Count := GetPropList(aComponent.ClassInfo, tkMethods, List);
    for I := 0 to Count - 1 do
    begin
      PropInfo := List^[I];
      if PropInfo^.PropType^.Kind in tkMethods then
      begin
        PropTypeName := PropInfo^.PropType^.Name;
        handlerClass := GetHandlerByEventType( PropTypeName );
        if (handlerClass <> nil) then
        begin
          HandlerObject:= handlerClass.Create( aComponent, PropInfo^.Name );
          if (HandlerObject.EventHasCode( aComponent, PropInfo^.Name ) )then
          begin
            handler := HandlerObject as IORCAEventHandler;
            Handler.Active := True;
            handler.EventManager := Self;
            FHandlers.Add( (handler as IORCAEventHandler) );
            handler.AddObservers( fObservers );
          end else
            HandlerObject.Free;
        end;
      end;
    end;
  finally
    FreeMem(List);
  end;
end;

procedure TORCAEventHandlerManager.AddObserver( aObserver: IObserver );
var SourceObj: IORCAObjectable;
begin

  if Supports(aObserver, IORCAObjectable, SourceObj) then
  begin
    if ( SourceObj.AsObject() <> self ) then
    begin
      if ( fObservers.IndexOf( aObserver ) = -1 ) then
      begin
        fObservers.Add(aObserver);
        AddObserverToHandlers( aObserver );
      end;
    end else
      raise EInvalidArgument.CreateFmt('%s.AddObserver:: Can''t observe self!',[self.ClassName] );
  end else
  begin
      if ( fObservers.IndexOf( aObserver ) = -1 ) then
      begin
        fObservers.Add(aObserver);
        AddObserverToHandlers( aObserver );
      end;
  end;

end;

procedure TORCAEventHandlerManager.AddObservers ( aObservers: array of IObserver);
var I:Integer;
begin
  for i:= Low( aObservers ) to High( aObservers ) do
  begin
    AddObserver( aObservers[i] );
  end;
end;

procedure TORCAEventHandlerManager.AddObserverToHandlers( aObserver: IObserver );
var i: Integer;
    observed: IObservable;
begin
  for i:=0 to FHandlers.Count -1 do begin
    if (Supports(FHandlers.Items[i], IObservable, observed)) then
      observed.AddObserver( aObserver );
  end;
end;

function TORCAEventHandlerManager.AsObject: TObject;
begin
  result := Self;
end;

function TORCAEventHandlerManager.ClassName: ShortString;
begin
  result := Self.ClassName;
end;

function TORCAEventHandlerManager.ClassType: TClass;
begin
  result := Self.ClassType;
end;

function TORCAEventHandlerManager.FindComponent(aName: String): TComponent;
begin
  result := nil;
end;

function TORCAEventHandlerManager.GetActive: Boolean;
begin
 Result := fActive;
end;

function TORCAEventHandlerManager.GetDisplayName: String;
begin
  result := 'Default event manager';
end;

function TORCAEventHandlerManager.GetHandledClass(index: Integer): TEventHandlerClass;
begin
  result := TEventHandlerClass( fRegisteredHandlers.Objects[index] );
end;

function TORCAEventHandlerManager.GetHandlerByEventType( aEventType: String ): TEventHandlerClass;
var index: Integer;
begin
  result := nil;
  index := fRegisteredHandlers.IndexOf( aEventType );
  if index <> -1 then
    result := TEventHandlerClass( fRegisteredHandlers.Objects[ index ]);
end;

function TORCAEventHandlerManager.getHandlers: IInterfaceList;
begin
  result := FHandlers;
end;

class function TORCAEventHandlerManager.GetManager(aoBservers: array of IObserver): IORCAEventHandlerManager;
begin
  result := TORCAEventHandlerManager.Create(aoBservers);
end;

function TORCAEventHandlerManager.GetStatus: String;
begin
  Result := Fstatus;
end;

procedure TORCAEventHandlerManager.RegisterHandlerClass ( aEventType: String; aHandlerClass: TClass );
begin
  if ( fRegisteredHandlers.IndexOf( aEventType ) = -1 ) then
  begin
    if ( fRegisteredHandlers.IndexOfObject( Pointer( aHandlerClass ) ) = -1 ) then
    begin
       fRegisteredHandlers.AddObject( aEventType, Pointer( aHandlerClass ));
    end else
      raise EInvalidArgument.CreateFmt('%s.RegisterHandlerClass:: A handler of type %s is already registered',[self.ClassName, aHandlerClass.ClassName]);
  end else
     raise EInvalidArgument.CreateFmt('%s.RegisterHandlerClass:: A event of type %s is already registered',[self.ClassName, aEventType]);
end;

procedure TORCAEventHandlerManager.RemoveComponent( aName: String );
begin

end;

procedure TORCAEventHandlerManager.RemoveComponent( aPrentclass: TClass; aComponent: TComponent );
var i:integer;
    h: IORCAEventHandler;
begin

  for i:= FHandlers.Count -1 downto 0 do
  begin
    if Supports( FHandlers.Items[i], IORCAEventHandler, h) then
    begin
      if h.GetEventSource.GetParentComponent <> nil then
      begin
        if (h.GetEventSource.GetParentComponent.ClassType = aPrentclass) and (h.GetEventSource = aComponent) then
           FHandlers.Remove( h );
      end;
    end;
  end;
end;

procedure TORCAEventHandlerManager.RemoveComponent( aPrentclass: TClass; aClass: TComponentClass );
begin

end;

procedure TORCAEventHandlerManager.RemoveComponent( aComponent: TComponent );
var i:integer;
    h: IORCAEventHandler;
begin

  for i:= FHandlers.Count -1 downto 0 do
  begin
    if Supports( FHandlers.Items[i], IORCAEventHandler, h) then
    begin
      if h.GetEventSource = aComponent then
        FHandlers.Remove( h );
    end;
  end;

end;

procedure TORCAEventHandlerManager.RemoveComponent( aParent: TComponent;  aComponent: TComponent );
var i:integer;
    h: IORCAEventHandler;
begin

  for i:= FHandlers.Count -1 downto 0 do
  begin
    if Supports( FHandlers.Items[i], IORCAEventHandler, h ) then
    begin
      if h.GetEventSource.GetParentComponent <> nil then
      begin
        if (h.GetEventSource.GetParentComponent = aParent) and (h.GetEventSource = aComponent) then
           FHandlers.Remove( h );
      end;
    end;
  end;
  
end;

procedure TORCAEventHandlerManager.RemoveComponent( aPrent: TComponent; aClass: TComponentClass );
begin

end;

procedure TORCAEventHandlerManager.RemoveComponent( aClass: TComponentClass );
begin

end;

procedure TORCAEventHandlerManager.RemoveEventHandler ( Data: TObject );
var EvHandler: IORCAEventHandler;
begin
   if ( Supports(Data, IORCAEventHandler, EvHandler) )then
     FHandlers.Remove( EvHandler );
end;

procedure TORCAEventHandlerManager.RemoveObserver ( aObserver: IObserver );
var i: Integer;
    observed: IObservable;
begin

  observed := nil;
  try
    for i:=0 to FHandlers.Count -1 do begin
      if (Supports(FHandlers.Items[i], IObservable, observed)) then
        observed.RemoveObserver( aObserver );
    end;
  finally
    fObservers.Remove( aObserver );
  end;

end;

procedure TORCAEventHandlerManager.SetActive(value: Boolean);
begin
  if fActive <> value then
  begin
    fActive := value;
    if not fActive then fStatus := 'Inactive'
    else Fstatus := 'Working'; 
  end;
end;

procedure TORCAEventHandlerManager.UnregisterHandlerClass ( aEventType: String );
var index: Integer;
begin
  index := fRegisteredHandlers.IndexOf( aEventType );
  if index <> -1 then
  begin
    fRegisteredHandlers.Delete( index );
  end;
end;

procedure TORCAEventHandlerManager.UnregisterHandlerClass ( aHandlerClass: TClass );
var index: Integer;
begin
  index := fRegisteredHandlers.IndexOfObject( Pointer(aHandlerClass) );
  if index <> -1 then
  begin
    fRegisteredHandlers.Delete( index );
  end;
end;

procedure TORCAEventHandlerManager.UpdateObserver( Data: TObject );
begin

end;

end.
