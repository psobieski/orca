unit ORCAEventLoggerInterafces;

interface

uses
  Classes, ORCADesignPatterns;

type

  IORCAObjectable = interface
  ['{7E69A40A-7987-4FBB-A2E3-E678E6B27C31}']
    function AsObject: TObject;
  end;


  IORCAObserver = interface(IObserver)
  ['{74A88155-A9C6-49DC-9258-733F807A5E74}']
    function ClassName: ShortString;
    function ClassType: TClass;
    function GetDisplayName: String;
    function AsObject: TObject;

    function GetStatus: String;

    function GetActive: Boolean;
    procedure SetActive(value: Boolean);
    property Active: Boolean read GetActive write SetActive;
    property Status: String read GetStatus;
  end;

  IORCAEventHandlerManager = interface
  ['{53635117-929C-4006-9613-3CA93A57271A}']
    procedure RemoveEventHandler ( Data: TObject );
    procedure RemoveComponent( aComponent: TComponent ); overload;
    procedure RemoveComponent( aParent: TComponent;  aComponent: TComponent ); overload;
    procedure RemoveComponent( aClass: TComponentClass ); overload;
    procedure RemoveComponent( aName: String ); overload;
    procedure RemoveComponent( aPrentclass: TClass; aClass: TComponentClass ); overload;
    procedure RemoveComponent( aPrentclass: TClass; aComponent: TComponent ); overload;
    procedure RemoveComponent( aPrent: TComponent; aClass: TComponentClass ); overload;
    procedure AddObserverToHandlers( aObserver: IObserver );
		procedure RegisterHandlerClass ( aEventType: String; aHandlerClass: TClass );
    procedure UnregisterHandlerClass ( aEventType: String ); overload;
    procedure UnregisterHandlerClass ( aHandlerClass: TClass ); overload;
    procedure AddComponent( aComponent: TComponent );
    function FindComponent( aName: String ): TComponent;
    function getHandlers: IInterfaceList;
    property Handlers: IInterfaceList Read GetHandlers;
  end;

  IORCAEventHandler = interface
  ['{ECBB15D4-8F9C-4CE7-84A4-04373C5994E1}']
    procedure AddObservers( aList: IInterfaceList );
    function EventHasCode( aComponent: TComponent; aEventName: String ): Boolean;
    function GetActive: boolean;
    function getClassName		: string;
    function getDisplayName : string;
    function getEventName   : String;
    function GetEventManager: IORCAEventHandlerManager;
    function getEventType   : string;
    function getObServers   : IInterfaceList;
    function GetEventSource : TComponent;
    function GetExecuteCount: Integer;
    function OriginalMethodAddress: Pointer;
    function OriginalMethodName: String;
    function isInstalled	  : Boolean;
    procedure Install;
    procedure Run;
    procedure RunSourceMethod();
    procedure SetActive( Value: Boolean);
    procedure SetEventmanager( Value: IORCAEventHandlerManager );
    procedure Uninstall;

    property Active: Boolean read GetActive write SetActive;
    property Installed: Boolean read isInstalled;
    property EventManager: IORCAEventHandlerManager read GetEventManager write SetEventmanager;
    property Observers: IInterfaceList read GetObservers;
    property ExecuteCount: Integer read GetExecuteCount;
  end;

implementation

end.
