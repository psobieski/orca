unit ORCAEeventLogger;

interface

uses
  Classes, KMInterfaces;

type

  TORCAObjectable = class(TInterfacedObject, IObjectable)
  public
    function AsObject: TObject;
  end;

  TORCACustomEventHandler = class(TORCAObjectable, IEventHandler, IObservable)
  private
    FObservers: IInterfaceList;
    fActive: Boolean;
    fEventSource: TComponent;
    FMyMethod: TMethod;
    FOriginalMethod: TMethod;
    fEventName: String;
    FPatched: Boolean;
    FExecuting: Boolean;
    FManager: IEventHandlerManager;
    fExecuteCount: Integer;
    function GetActive: boolean;
    procedure SetActive( Value: Boolean );
    function GetEventManager: IEventHandlerManager;
    procedure SetEventmanager( Value: IEventHandlerManager );
    function getObServers   : IInterfaceList;
    function isInstalled	  : Boolean;
    function GetExecuteCount: Integer;
  protected
    function getDisplayName: string; virtual;
    function getEventSource: TComponent;
    procedure ActiveChanged(); virtual;
    procedure RunSourceMethod(); virtual; abstract;
    function GetPatchedMethodName(): String; virtual; abstract;
  public
    constructor Create( aEventSource: TComponent; aEventName: String ); reintroduce; virtual;

    procedure FreeInstance; override;

    function OriginalMethodAddress: Pointer;
    function OriginalMethodName: String;

    function getEventName   : String;
    function getClassName		: string;

    { Obsever Design Pattern -> IObservable implementation }
    procedure AddObserver( aObserver: IObserver );
    procedure RemoveObserver( aObserver: IObserver );
    procedure UpdateObservers( Data: TObject );
    //for easy observer bindings
    procedure AddObservers( aList: IInterfaceList );
    function getEventType   : String; virtual; abstract;

    procedure Install;
    procedure Uninstall;
    procedure Run;

    function EventHasCode( aComponent: TComponent; aEventName: String ): Boolean;
    property Actvie: Boolean read GetActive write SetActive;
    property EventManager: IEventHandlerManager read GetEventManager write SetEventmanager;
    property Installed: Boolean read isInstalled;
    property ExecuteCount: Integer read GetExecuteCount;
  published
  end;

  TEventHandlerClass = class of TORCACustomEventHandler;

  PNotifyMethod = ^TNotifyMethod;
  TNotifyMethod = TNotifyEvent;

  TORCANotifyEventHandler = class(TORCACustomEventHandler)
  protected
    procedure RunSourceMethod(); override;
    function GetPatchedMethodName(): String; override;
  public
    function getEventType: String; override;
  published
    procedure PatchedNotifyEvent( Sender: TObject );
  end;

  TEventHandlerManager = class (TORCAObjectable, IKMObserver, IEventHandlerManager)
  private
    FHandlers: IInterfaceList;
    fObservers: IInterfaceList;
    fRegisteredHandlers: TStringList;
    fActive: Boolean;
    fStatus: String;
    function GetHandledClass( index: Integer ): TEventHandlerClass;
    function GetHandlerByEventType( aEventType: String ): TEventHandlerClass;
    function getHandlers: IInterfaceList;
    function GetActive: Boolean;
    procedure SetActive(value: Boolean);
  protected
    procedure AddObserver( aObserver: IObserver ); virtual;
    procedure AddObserverToHandlers( aObserver: IObserver );
    function GetStatus: String; virtual;
  public
    constructor Create( aObservers: array of IObserver); reintroduce;
    destructor Destroy; override;

    procedure UpdateObserver( Data: TObject );
    procedure AddObservers ( aObservers: array of IObserver); dynamic;
    procedure RemoveObserver ( aObserver: IObserver ); dynamic;

    procedure RegisterHandlerClass ( aEventType: String; aHandlerClass: TClass );
    procedure UnregisterHandlerClass ( aEventType: String ); overload;
    procedure UnregisterHandlerClass ( aHandlerClass: TClass ); overload;
    procedure RemoveEventHandler ( Data: TObject );
    procedure RemoveComponent( aComponent: TComponent ); overload;
    procedure RemoveComponent( aParent: TComponent;  aComponent: TComponent ); overload;
    procedure RemoveComponent( aClass: TComponentClass ); overload;
    procedure RemoveComponent( aName: String ); overload;
    procedure RemoveComponent( aPrentclass: TClass; aClass: TComponentClass ); overload;
    procedure RemoveComponent( aPrent: TComponent; aClass: TComponentClass ); overload;
    procedure RemoveComponent( aPrentclass: TClass; aComponent: TComponent ); overload;

    procedure AddComponent( aComponent: TComponent );
    function FindComponent(aName: String): TComponent;

    property HandledClasses[index: Integer]: TEventHandlerClass read GetHandledClass;
    property Handlers: IInterfaceList Read getHandlers;
    property HandlerByEventType[ aEventType: String ]: TEventHandlerClass read GetHandlerByEventType;
    { IKMOBSERVER }
		function ClassName: ShortString;
    function ClassType: TClass;
    function GetDisplayName: String;
    function AsObject: TObject;

    property Active: Boolean read GetActive write SetActive;
    property Status: String read GetStatus ;
    { IKMOBSERVER }
    class function GetManager(aoBservers: array of IObserver): IEventHandlerManager;
  end;

var DefaultHandlerManager: IEventHandlerManager;

implementation

uses TypInfo, Math, SysUtils, Dialogs;

function TORCAObjectable.AsObject: TObject;
begin
  Result := Self;
end;

constructor TORCACustomEventHandler.Create( aEventSource: TComponent; aEventName: String );
begin
  inherited Create;
  fEventSource := aEventSource;
  FObservers   :=  TInterfaceList.Create;
  fEventName   := aEventName;
  fActive      := False;
  FOriginalMethod.Code := nil;
  FOriginalMethod.Data := nil;
  Fpatched := False;
  FExecuting := False;
  fExecuteCount := 0;
end;

procedure TORCACustomEventHandler.FreeInstance;
begin
  Uninstall;
  FObservers := nil;
  inherited FreeInstance;
end;

function TORCACustomEventHandler.GetExecuteCount: Integer;
begin
  Result := fExecuteCount;
end;

function TORCACustomEventHandler.OriginalMethodAddress: Pointer;
begin
  result := FOriginalMethod.Code;
end;

function TORCACustomEventHandler.OriginalMethodName: String;
begin
  result := fEventSource.MethodName( FOriginalMethod.Code );
end;

function TORCACustomEventHandler.isInstalled	  : Boolean;
begin
  result := FPatched;
end;

function TORCACustomEventHandler.GetEventManager: IEventHandlerManager;
begin
  Result := FManager;
end;

procedure TORCACustomEventHandler.SetEventmanager( Value: IEventHandlerManager );
begin
  FManager := Value;
end;

function TORCACustomEventHandler.getEventName: String;
begin
  result := fEventName;
end;

function TORCACustomEventHandler.getClassName: string;
begin
  result := self.ClassName;
end;

procedure TORCACustomEventHandler.AddObservers( aList: IInterfaceList );
var i: Integer;
    observer: IObserver;
begin
	for i:=0 to aList.Count -1 do
  begin
    if (Supports( aList.Items[i], IObserver, observer )) then
       self.AddObserver( observer );
  end;
end;

procedure TORCACustomEventHandler.AddObserver( aObserver: IObserver );
begin
  if ( FObservers.IndexOf(aObserver) = -1) then
    FObservers.Add( aObserver );
end;

procedure TORCACustomEventHandler.RemoveObserver( aObserver: IObserver );
begin
  FObservers.Remove( aObserver );
end;

procedure TORCACustomEventHandler.UpdateObservers( Data: TObject );
var i:Integer;
    observer: IObserver;
begin

  for i :=0 to FObservers.count -1 do
  begin
    observer := FObservers.Items[i] as IObserver;
    observer.UpdateObserver( data );
  end;

end;

function TORCACustomEventHandler.GetActive: boolean;
begin
  Result := fActive;
end;

procedure TORCACustomEventHandler.SetActive( Value: Boolean );
begin
  if fActive <> value then
  begin
    fActive := Value;
    ActiveChanged();
  end;
end;

procedure TORCACustomEventHandler.ActiveChanged();
begin
  if fActive then
     Install()
  else
     Uninstall();
end;

function TORCACustomEventHandler.getDisplayName: string;
begin
  result := Self.ClassName;
end;

function TORCACustomEventHandler.getEventSource: TComponent;
begin
  Result := fEventSource;
end;

procedure TORCACustomEventHandler.Install;
begin
  if FPatched or (csDestroying in fEventSource.ComponentState) then
  	exit;

  if ( Assigned( fEventSource ) ) then
  begin
    ForiginalMethod := GetMethodProp( fEventSource, fEventName );
    if Assigned( ForiginalMethod.code ) then
    begin
			FMyMethod.Code := MethodAddress( GetPatchedMethodName() );
		 	FMyMethod.Data := Pointer( Self );
      SetMethodProp( fEventSource , fEventName , FMyMethod );
      fPatched := True;
    end;
  end;
end;

function TORCACustomEventHandler.EventHasCode( aComponent: TComponent; aEventName: String ): Boolean;
var EventData: TMethod;
begin
  EventData := GetMethodProp( aComponent, aEventName );
  result := Assigned( EventData.Code );
end;

function TORCACustomEventHandler.getObServers   : IInterfaceList;
begin
  result := FObservers;
end;

procedure TORCACustomEventHandler.Uninstall;
begin
   if not FPatched then exit;
   if Assigned(fEventSource) then
   begin
     if (not (csDestroying in fEventSource.ComponentState)) then
     begin
       SetMethodProp( fEventSource , fEventName , FOriginalMethod);
       FPatched := False;
     end;
   end;
end;

procedure TORCACustomEventHandler.Run;
begin
  if FExecuting then Exit;
  FExecuting := True;
  try
	  RunSourceMethod();
    Inc(fExecuteCount);
    if fActive and (not(csdestroying in fEventSource.ComponentState)) then
     UpdateObservers( self );
  finally
    FExecuting := False;
  end;
end;

procedure TORCANotifyEventHandler.RunSourceMethod();
begin
  if (assigned(FOriginalMethod.Code)) then
  begin
     TNotifyMethod( fOriginalMethod )( fEventSource );
  end;
end;

function TORCANotifyEventHandler.GetPatchedMethodName(): String;
begin
  Result := 'PatchedNotifyEvent';
end;

function TORCANotifyEventHandler.getEventType: String;
begin
  Result := 'TNotifyEvent';
end;

procedure TORCANotifyEventHandler.PatchedNotifyEvent( Sender: TObject );
begin
  Run();
end;

constructor TEventHandlerManager.Create( aObservers: array of IObserver);
begin
  inherited Create;
  fActive := True;
  fStatus := 'Working';
  fRegisteredHandlers := TStringList.Create;
  FHandlers := TInterfaceList.Create;
  fObservers := TInterfaceList.Create;
  AddObservers( aObservers );
end;

destructor TEventHandlerManager.Destroy;
begin
  fObservers := nil;
  FHandlers  := nil;
  fRegisteredHandlers.Free;
  Inherited;
end;

procedure TEventHandlerManager.UpdateObserver( Data: TObject );
begin

end;

procedure TEventHandlerManager.AddObservers ( aObservers: array of IObserver);
var I:Integer;
begin
	for i:= Low( aObservers ) to High( aObservers ) do
  begin
    AddObserver( aObservers[i] );
  end;
end;

procedure TEventHandlerManager.AddObserver( aObserver: IObserver );
var SourceObj: IObjectable;
begin

  if Supports(aObserver, IObjectable, SourceObj) then
  begin
    if ( SourceObj.AsObject() <> self ) then
    begin
      if ( fObservers.IndexOf( aObserver ) = -1 ) then
      begin
        fObservers.Add(aObserver);
        AddObserverToHandlers( aObserver );
      end;
    end else
      raise EInvalidArgument.CreateFmt('%s.AddObserver:: Can''t observe self!',[self.ClassName] );
  end else
  begin
      if ( fObservers.IndexOf( aObserver ) = -1 ) then
      begin
        fObservers.Add(aObserver);
        AddObserverToHandlers( aObserver );
      end;
  end;

end;

procedure TEventHandlerManager.AddObserverToHandlers( aObserver: IObserver );
var i: Integer;
    observed: IObservable;
begin
  for i:=0 to FHandlers.Count -1 do begin
    if (Supports(FHandlers.Items[i], IObservable, observed)) then
      observed.AddObserver( aObserver );
  end;
end;

procedure TEventHandlerManager.RemoveObserver ( aObserver: IObserver );
var i: Integer;
    observed: IObservable;
begin

  observed := nil;
  try
    for i:=0 to FHandlers.Count -1 do begin
      if (Supports(FHandlers.Items[i], IObservable, observed)) then
        observed.RemoveObserver( aObserver );
    end;
  finally
    fObservers.Remove( aObserver );
  end;

end;

procedure TEventHandlerManager.RegisterHandlerClass ( aEventType: String; aHandlerClass: TClass );
begin
  if ( fRegisteredHandlers.IndexOf( aEventType ) = -1 ) then
  begin
    if ( fRegisteredHandlers.IndexOfObject( Pointer( aHandlerClass ) ) = -1 ) then
    begin
       fRegisteredHandlers.AddObject( aEventType, Pointer( aHandlerClass ));
    end else
      raise EInvalidArgument.CreateFmt('%s.RegisterHandlerClass:: A handler of type %s is already registered',[self.ClassName, aHandlerClass.ClassName]);
  end else
     raise EInvalidArgument.CreateFmt('%s.RegisterHandlerClass:: A event of type %s is already registered',[self.ClassName, aEventType]);
end;

procedure TEventHandlerManager.UnregisterHandlerClass ( aEventType: String );
var index: Integer;
begin
  index := fRegisteredHandlers.IndexOf( aEventType );
  if index <> -1 then
  begin
    fRegisteredHandlers.Delete( index );
  end;
end;

procedure TEventHandlerManager.UnregisterHandlerClass ( aHandlerClass: TClass );
var index: Integer;
begin
  index := fRegisteredHandlers.IndexOfObject( Pointer(aHandlerClass) );
  if index <> -1 then
  begin
    fRegisteredHandlers.Delete( index );
  end;
end;

function TEventHandlerManager.GetHandledClass(index: Integer): TEventHandlerClass;
begin
  result := TEventHandlerClass( fRegisteredHandlers.Objects[index] );
end;

procedure TEventHandlerManager.AddComponent(aComponent: TComponent);
var
	Count, Size, I: Integer;
	List: PPropList;
	PropInfo: PPropInfo;
  PropTypeName: String;
  handlerClass: TEventHandlerClass;
  handler: IEventHandler;
  HandlerObject: TORCACustomEventHandler;
begin { TODO -oRadek : Dekompozycja tej metody }
	Count := GetPropList(aComponent.ClassInfo, tkMethods, nil);
	Size  := Count * SizeOf(Pointer);
	GetMem(List, Size);
	try
		Count := GetPropList(aComponent.ClassInfo, tkMethods, List);
		for I := 0 to Count - 1 do
		begin
			PropInfo := List^[I];
			if PropInfo^.PropType^.Kind in tkMethods then
			begin
        PropTypeName := PropInfo^.PropType^.Name;
        handlerClass := GetHandlerByEventType( PropTypeName );
        if (handlerClass <> nil) then
        begin
          HandlerObject:= handlerClass.Create( aComponent, PropInfo^.Name );
          if (HandlerObject.EventHasCode( aComponent, PropInfo^.Name ) )then
          begin
            handler := HandlerObject as IEventHandler;
            Handler.Active := True;
            handler.EventManager := Self;
            FHandlers.Add( (handler as IEventHandler) );
            handler.AddObservers( fObservers );
          end else
            HandlerObject.Free;
        end;
      end;
		end;
	finally
		FreeMem(List);
	end;
end;


function TEventHandlerManager.GetHandlerByEventType( aEventType: String ): TEventHandlerClass;
var index: Integer;
begin
  result := nil;
  index := fRegisteredHandlers.IndexOf( aEventType );
  if index <> -1 then
    result := TEventHandlerClass( fRegisteredHandlers.Objects[ index ]);
end;

function TEventHandlerManager.FindComponent(aName: String): TComponent;
begin
  result := nil;
end;

procedure TEventHandlerManager.RemoveEventHandler ( Data: TObject );
var EvHandler: IEventHandler;
begin
   if ( Supports(Data, IEventHandler, EvHandler) )then
     FHandlers.Remove( EvHandler );
end;

procedure TEventHandlerManager.RemoveComponent( aComponent: TComponent );
var i:integer;
    h: IEventHandler;
begin

	for i:= FHandlers.Count -1 downto 0 do
  begin
    if Supports( FHandlers.Items[i], IEventHandler, h) then
    begin
      if h.GetEventSource = aComponent then
        FHandlers.Remove( h );
    end;
  end;

end;

procedure TEventHandlerManager.RemoveComponent( aParent: TComponent;  aComponent: TComponent );
var i:integer;
    h: IEventHandler;
begin

	for i:= FHandlers.Count -1 downto 0 do
  begin
    if Supports( FHandlers.Items[i], IEventHandler, h) then
    begin
      if h.GetEventSource.GetParentComponent <> nil then
      begin
        if (h.GetEventSource.GetParentComponent = aParent) and (h.GetEventSource = aComponent) then
           FHandlers.Remove( h );
      end;
    end;
  end;

end;

procedure TEventHandlerManager.RemoveComponent( aPrentclass: TClass; aComponent: TComponent );
var i:integer;
    h: IEventHandler;
begin

	for i:= FHandlers.Count -1 downto 0 do
  begin
    if Supports( FHandlers.Items[i], IEventHandler, h) then
    begin
      if h.GetEventSource.GetParentComponent <> nil then
      begin
        if (h.GetEventSource.GetParentComponent.ClassType = aPrentclass) and (h.GetEventSource = aComponent) then
           FHandlers.Remove( h );
      end;
    end;
  end;
end;

procedure TEventHandlerManager.RemoveComponent( aClass: TComponentClass );
begin

end;

procedure TEventHandlerManager.RemoveComponent( aName: String );
begin

end;

procedure TEventHandlerManager.RemoveComponent( aPrentclass: TClass; aClass: TComponentClass );
begin

end;

procedure TEventHandlerManager.RemoveComponent( aPrent: TComponent; aClass: TComponentClass );
begin

end;

function TEventHandlerManager.getHandlers: IInterfaceList;
begin
  result := FHandlers;
end;

class function TEventHandlerManager.GetManager(aoBservers: array of IObserver): IEventHandlerManager;
begin
  result := TEventHandlerManager.Create(aoBservers);
end;

function TEventHandlerManager.GetActive: Boolean;
begin
 Result := fActive;
end;

procedure TEventHandlerManager.SetActive(value: Boolean);
begin
  if fActive <> value then
  begin
    fActive := value;
    if not fActive then fStatus := 'Inactive'
    else Fstatus := 'Working'; 
  end;
end;

function TEventHandlerManager.GetStatus: String;
begin
  Result := Fstatus;
end;

function TEventHandlerManager.ClassName: ShortString;
begin
  result := Self.ClassName;
end;

function TEventHandlerManager.ClassType: TClass;
begin
  result := Self.ClassType;
end;

function TEventHandlerManager.GetDisplayName: String;
begin
  result := 'Default event manager';
end;

function TEventHandlerManager.AsObject: TObject;
begin
  result := Self;
end;

end.
