unit ORCAEventHandlers;

interface

uses
	Classes, ORCAEventLoggerFramework;

type

  TORCANotifyEventHandler = class(TORCACustomEventHandler)
  protected
    function GetPatchedMethodName(): String; override;
    procedure RunSourceMethod(); override;
  public
    function getEventType: String; override;
  published
    procedure PatchedNotifyEvent( Sender: TObject );
  end;

implementation

{
}

type
 PNotifyMethod = ^TNotifyMethod;
 TNotifyMethod = TNotifyEvent;

function TORCANotifyEventHandler.getEventType: String;
begin
  Result := 'TNotifyEvent';
end;

function TORCANotifyEventHandler.GetPatchedMethodName(): String;
begin
  Result := 'PatchedNotifyEvent';
end;

procedure TORCANotifyEventHandler.PatchedNotifyEvent( Sender: TObject );
begin
  Run();
end;

procedure TORCANotifyEventHandler.RunSourceMethod();
begin
  if (assigned(OriginalMethod.Code)) then
  begin
     TNotifyMethod( OriginalMethod )( GetEventSource() );
  end;
end;

{
}

end.
