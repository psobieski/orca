unit uDemoMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdActns, ExtActns, ToolWin, ComCtrls, Menus, ImgList,
  StdCtrls, ORCAEventLoggerFramework, ORCAEventLoggerInterafces, ORCADesignPatterns,
  ORCAEventHandlers;

type
  TMainForm = class(TForm, IObserver )
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Edycja1: TMenuItem;
    Szukaj1: TMenuItem;
    View1: TMenuItem;
    Pomoc1: TMenuItem;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    Nowy1: TMenuItem;
    Otwrz1: TMenuItem;
    N1: TMenuItem;
    Zapisz1: TMenuItem;
    Zapiszjako1: TMenuItem;
    N2: TMenuItem;
    Zamknij1: TMenuItem;
    ActionList: TActionList;
    FileOpen: TFileOpen;
    FileSaveAs: TFileSaveAs;
    FileExit: TFileExit;
    FileNew: TAction;
    FileSave: TAction;
    EditCut: TEditCut;
    EditCopy: TEditCopy;
    EditPaste: TEditPaste;
    EditSelectAll: TEditSelectAll;
    EditDelete: TEditDelete;
    RichEditBold: TRichEditBold;
    RichEditItalic: TRichEditItalic;
    RichEditUnderline: TRichEditUnderline;
    RichEditStrikeOut: TRichEditStrikeOut;
    RichEditBullets: TRichEditBullets;
    RichEditAlignLeft: TRichEditAlignLeft;
    RichEditAlignRight: TRichEditAlignRight;
    RichEditAlignCenter: TRichEditAlignCenter;
    SearchFind: TSearchFind;
    SearchFindNext: TSearchFindNext;
    SearchReplace: TSearchReplace;
    SearchFindFirst: TSearchFindFirst;
    Copy1: TMenuItem;
    Cut1: TMenuItem;
    Delete1: TMenuItem;
    N3: TMenuItem;
    Paste1: TMenuItem;
    SelectAll1: TMenuItem;
    N4: TMenuItem;
    Find1: TMenuItem;
    Replace1: TMenuItem;
    N5: TMenuItem;
    FindFirst1: TMenuItem;
    FindNext1: TMenuItem;
    ImageList1: TImageList;
    reEditor: TRichEdit;
    SaveDialog: TSaveDialog;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    cbxFonts: TComboBox;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    Manadzereventw1: TMenuItem;
    Informacjeoprogramie1: TMenuItem;
    Format1: TMenuItem;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    Center1: TMenuItem;
    AlignLeft1: TMenuItem;
    AlignRight1: TMenuItem;
    Bullets1: TMenuItem;
    N6: TMenuItem;
    Bold1: TMenuItem;
    Italic1: TMenuItem;
    Underline1: TMenuItem;
    Strikeout1: TMenuItem;
    FontDialog1: TFontDialog;
    FormatSelectFont: TAction;
    N7: TMenuItem;
    Czcionka1: TMenuItem;
    procedure cbxFontsChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FileNewExecute(Sender: TObject);
    procedure FileOpenAccept(Sender: TObject);
    procedure FileSaveExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Manadzereventw1Click(Sender: TObject);
    procedure reEditorChange(Sender: TObject);
  private
    FObserverMessages: TStrings;
    FCurrentFileName: String;
    FModified: Boolean;
    FManager: IORCAEventHandlerManager;
    eventLogger: IObserver;
    Procedure UpdateControlsState();
    Procedure ListAvailableFonts();
    Procedure AddComponentsToLog();
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure UpdateObserver( Data: TObject );
  end;

var
  MainForm: TMainForm;

implementation

uses ORCALoggerMonitor, ORCAEventObserverXML;

{$R *.dfm}

constructor TMainForm.Create(aOwner: TComponent);
begin
  eventLogger := TORCAEventObserverXML.Create(ExtractFilePath(Application.exename)+'events.xml');
  if (Supports(eventLogger, IFileReader)) then
  inherited Create(aOwner);
end;

destructor TMainForm.Destroy;

begin
  if (Supports(eventLogger, IFileWriter)) then
    IFileWriter(eventLogger).SaveToFile( ExtractFilePath(Application.exename)+'events.xml' );
  eventLogger := nil;
  inherited;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
var
  obs: IObservable;
begin
  if (Supports( Fmanager, iObservable, obs)) then
     obs.RemoveObserver( Self );
  FManager := nil;
  FObserverMessages.SaveToFile( ExtractFilePath(Application.ExeName)+'log.txt');
  FObserverMessages.Free;
end;

procedure TMainForm.FileNewExecute(Sender: TObject);
begin
  if FModified then
  begin
    if MessageDlg('Current file is modified! Would you like to save it now ?', mtConfirmation, [mbYes, mbNo],0) = mrYes then
    begin
      FileSave.Execute;
      if FCurrentFileName = '' then
        Exit;
    end;
  end;

  reEditor.Lines.Clear;
  FModified := False;
  UpdateControlsState();
end;

procedure TMainForm.FileOpenAccept(Sender: TObject);
begin
  FCurrentFileName := TFileOpen(sender).Dialog.FileName;
  reEditor.Lines.LoadFromFile( FCurrentFileName );
  FModified := False;
  UpdateControlsState();
end;

procedure TMainForm.FileSaveExecute(Sender: TObject);
begin
  if FCurrentFileName = '' then
  begin
    if SaveDialog.Execute then
    begin
       FCurrentFileName := SaveDialog.FileName;
    end else
      Exit;
  end;

  reEditor.Lines.SaveToFile( FCurrentFileName );
  FModified := False;
  UpdateControlsState;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FObserverMessages := TStringList.Create();
  FManager := TORCAEventHandlerManager.Create( [self, eventLogger] );
  FManager.RegisterHandlerClass('TNotifyEvent', TORCANotifyEventHandler );

  AddComponentsToLog();

  self.Caption := Application.Title;
  FCurrentFileName := '';
  FModified := False;
  UpdateControlsState;
  ListAvailableFonts();
  cbxFonts.ItemIndex := cbxFonts.Items.IndexOf( reEditor.Font.Name );
end;

procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word; Shift:
    TShiftState);
begin
   if ( ssCtrl in Shift ) then
   begin
     case Chr(Key) of
       'Q': Self.Close();
     end;
   end;
end;

procedure TMainForm.reEditorChange(Sender: TObject);
begin
  FModified := True;
  UpdateControlsState();
end;

Procedure TMainForm.UpdateControlsState();
begin
  Self.Caption := Application.Title;

  if FCurrentFileName <> '' then
     self.Caption := self.Caption + ' ['+FCurrentFileName+']';

  FileSave.Enabled := FModified;
  FileSaveAs.Enabled := (FCurrentFileName <> '');

  cbxFonts.ItemIndex := cbxFonts.Items.IndexOf( reEditor.SelAttributes.Name );
end;

Procedure TMainForm.ListAvailableFonts();
begin
  cbxFonts.Items.Clear;
  cbxFonts.Items.AddStrings( Screen.Fonts );
end;

Procedure TMainForm.AddComponentsToLog();
var i:Integer;
begin
  for i:=0 to ComponentCount-1 do begin
    FManager.AddComponent( Components[i] );
  end;
end;

procedure TMainForm.cbxFontsChange(Sender: TObject);
begin
  reEditor.SelAttributes.Name := cbxFonts.Items[ cbxFonts.Itemindex ];
end;

procedure TMainForm.Manadzereventw1Click(Sender: TObject);
begin
    TFormLoggerMonitor.Execute( FManager );
end;

procedure TMainForm.UpdateObserver( Data: TObject );
var evHandler: IORCAEventHandler;
    aComponent: TComponent;
begin

  if Supports( Data, IORCAEventHandler, evHandler ) then
  begin
     aComponent := evHandler.GetEventSource();
     FObserverMessages.Add( Format('[%s]:: Observer notified from %s::%s.%s()',
          [DateTimeToStr(Now), data.ClassName, aComponent.Name,evHandler.getEventName]));
  end;

end;

end.
