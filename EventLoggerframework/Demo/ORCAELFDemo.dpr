program ORCAELFDemo;

uses
  FastMM4,
  FastMMMemLeakMonitor,
  Forms,
  uDemoMainForm in 'uDemoMainForm.pas' {MainForm},
  ORCAEventLoggerFramework in '..\ORCAEventLoggerFramework.pas',
  ORCAEventLoggerInterafces in '..\ORCAEventLoggerInterafces.pas',
  ORCALoggerMonitor in '..\ORCALoggerMonitor.pas' {FormLoggerMonitor},
  ORCADesignPatterns in '..\ORCADesignPatterns.pas',
  ORCAEventHandlers in '..\ORCAEventHandlers.pas',
  ORCAEventObservers in '..\ORCAEventObservers.pas',
  ORCAEventObserverXML in '..\ORCAEventObserverXML.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'ORCA-EventLoggerFramework - DEMO Application';
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
