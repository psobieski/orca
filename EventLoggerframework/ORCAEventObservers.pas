unit ORCAEventObservers;

interface

uses
  ORCADesignPatterns, ORCAEventLoggerInterafces;

type
  TORCAEventObserverAbstract = class ( TInterfacedObject, IObserver, IORCAObserver )
  private
    FActive: Boolean;
    function GetActive: Boolean;
    procedure SetActive(value: Boolean);
  protected
    function GetStatus: String; virtual; abstract;
    function GetDisplayName: String; virtual; abstract;
    procedure ActiveChanged(); virtual; abstract;
  public
		function ClassName: ShortString;
    function ClassType: TClass;
    function AsObject: TObject; virtual;
    procedure UpdateObserver( Data: TObject ); virtual; abstract;
    property Active: Boolean read GetActive write SetActive;
    property Status: String read GetStatus;
  end;
  
implementation

function TORCAEventObserverAbstract.GetActive: Boolean;
begin
  result := FActive;
end;

procedure TORCAEventObserverAbstract.SetActive(value: Boolean);
begin
  if FActive <> Value then
  begin
    FActive := Value;
    ActiveChanged();
  end;
end;

function TORCAEventObserverAbstract.ClassName: ShortString;
begin
  result := Self.ClassName;
end;

function TORCAEventObserverAbstract.ClassType: TClass;
begin
  result := Self.ClassType;
end;

function TORCAEventObserverAbstract.AsObject: TObject;
begin
  result := Self;
end;

end.
